#include <scif.h>

#include <cstring>
#include <stdlib.h>

#include <stdio.h>
#include <omp.h>

#include <fstream>
#include <iostream>
#include <cmath>



//#define PHI

#include "../common/cell.h"
#include "../common/unit.h"
//#include "../common/common.h"  //because cell.h include common.h
#include "../common/printInfo.h"
#include "../common/compute_cell.h"

#define PORTNUMBER0 3000
#define PORTNUMBER1 3020

#define Ntotdyads 10000

#define ALIGN 0x1000
#define START_OFFSET 0x800000

//set numthreads in common.h file
//#define NUMTHREADS 118
#define PHINUMMAX 4



unsigned int PORTNUMBER[PHINUMMAX] = {3000,3020,3040,3060};

//#define REG
//#define SCIFT

//#define OPENMPONCELL

//printf("%s\n", string); 
#ifdef hh
#define BARRIER(epd, string) { \
        if ((err = scif_recv(epd, &control_msg, sizeof(control_msg), 1)) <= 0) { \
		printf("scif_recv failed with err %d\n", errno); \
		fflush(stdout); \
	} \
	if ((err = scif_send(epd, &control_msg, sizeof(control_msg), 1)) <= 0) { \
		printf("scif_send failed with err %d\n", errno); \
		fflush(stdout); \
	} \	
}


#else
#define BARRIER(epd, string) { \
	if ((err = scif_send(epd, &control_msg, sizeof(control_msg), 1)) <= 0) { \
		printf("scif_send failed with err %d\n", errno); \
		fflush(stdout); \
	} \
	if ((err = scif_recv(epd, &control_msg, sizeof(control_msg), 1)) <= 0) { \
		printf("scif_recv failed with err %d\n", errno); \
		fflush(stdout); \
	} \
}
#endif

	//printf("==============================================================\n"); 

#define FENCE(newepd, self) { \
        if ((err = scif_fence_mark(newepd, \
                        self? SCIF_FENCE_INIT_SELF : SCIF_FENCE_INIT_PEER, \
                        &mark))) { \
                printf("scif_fence_mark failed with err %d\n", errno); \
        } \
        if ((err = scif_fence_wait(newepd, mark))) { \
                printf("scif_fence_wait failed with err %d\n", errno); \
        } \
}

//printf("Value of mark 0x%x\n", mark); \

#define SEND(newepd,sbuffer,size){\
	if (scif_send (newepd, &sbuffer, size, SCIF_SEND_BLOCK) < 0){\
		switch (errno){\
			case ECONNRESET:printf ("[SEND]A connection was forcibly closed by a peer.\n");break;\
			case EFAULT:printf ("[SEND]An invalid address was specified for a parameter.\n");break;\
			case EINTR:printf("[SEND]epd was closed by scif_close().\n");break;\
			case EINVAL:printf("[SEND]epd is not a valid endpoint descriptor, or flags is invalid or len is negative\n");break;\
			case ENODEV:printf("[SEND]The remote node is lost.\n");break;\
			case ENOMEM:printf("[SEND]Not enough space\n");break;\
			case ENOTCONN:printf("[SEND]The endpoint is not connected\n");break;\
			case ENOTTY:printf("[SEND]ENOTTY\n");break; \ 
			default:printf("SEND]Unknown exit reason %d\n", errno);\
		}\
  }\
}

#define RECV(newepd,sbuffer,size){\
 if (scif_recv (newepd,&sbuffer,size, SCIF_RECV_BLOCK) < 0){\
      printf ("[MIC1] Error in scif_send %d\n", errno);\
 }\
}


double *** PHIallocate_3D_double(int Nx, int Ny,int Nz)
{
  double ***ptr;
  int size = Nx*Ny*Nz*sizeof(double);
  if((size%ALIGN)!=0)
       size = (size/ALIGN+1)*ALIGN;
  double *base_ptr = (double*)_mm_malloc(size,ALIGN);
  ptr = (double***)_mm_malloc(Nx*sizeof(double**),ALIGN);
  for (int ix=0; ix<Nx; ix++)
  {
	ptr[ix] = (double**)_mm_malloc(Ny*sizeof(double*),ALIGN);
	for (int iy = 0; iy<Ny; iy++)
       {
		ptr[ix][iy] = &(base_ptr[ix*Ny*Nz + iy*Nz]);
       }
   }
   return ptr;
}


struct cell ***PHIAllocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z)
{
    struct cell ***SubCells;
    struct cell *cells = (struct cell*)malloc(sub_N_x*sub_N_y*sub_N_z*sizeof(struct cell));
    SubCells = (struct cell***)malloc(sub_N_x*sizeof(struct cell**));
    for(int ix=0; ix<sub_N_x; ix++)
    {
        SubCells[ix] = (struct cell**)malloc(sub_N_y*sizeof(struct cell*));
        for(int iy=0;iy<sub_N_y;iy++)
	     SubCells[ix][iy] = &(cells[ix*sub_N_y*sub_N_z+iy*sub_N_z]);
    }
    return SubCells;

}

void PHIdeallocate3DSubCells(struct cell ***SubCells,int Nx)
{
    free(SubCells[0][0]);
    for(int i=0;i<Nx;i++)
    	free(SubCells[i]);
    free(SubCells);
    return;
}


void PHIdeallocate_3D_int(int ***ptr,int Nx)
{
	_mm_free(ptr[0][0]);
	for(int i=0;i<Nx;i++)
	    _mm_free(ptr[i]);
	_mm_free(ptr);
}
void PHIdeallocate_3D_double(double ***ptr,int Nx)
{
	_mm_free(ptr[0][0]);
	for(int i=0;i<Nx;i++)
	    _mm_free(ptr[i]);
	_mm_free(ptr);
}




int main(int argc,char **argv)
{

	//#define PORTNUMBER0 3000
        int phiid=-2;
        if(argc>1)
           phiid = atoi(argv[1]);
	scif_epd_t source;
	
        int portNum = PORTNUMBER[phiid];
	struct scif_portID dstID;
	dstID.node = 0;
	dstID.port = portNum;
	
	int mark;

	source = scif_open();
	if(source == ((scif_epd_t)-1))
	{
		printf("scif open failed!\n");
	}
	else 
		printf("mic0 scif open success!\n");
	
__retry:
	if(scif_connect(source, &dstID)<0)
	{
		if((errno == ECONNREFUSED))
		{
			printf("Scif_connect connect to host failed! \n");
			goto __retry;
		}

	}
	else 
		printf("mic0 connect to host success!\n");
		
	int err;   	
	if ((err = scif_send(source, &phiid, sizeof(int), 1)) <= 0) { 
		printf("scif_send failed with err %d on phi %d\n", errno,phiid); 
		fflush(stdout); 
	} 
		

        PHItype *P = (PHItype *)_mm_malloc(sizeof(PHItype),ALIGN);
        if(scif_recv(source,P,sizeof(PHItype),SCIF_RECV_BLOCK)<0)
		printf("mic 0: error in scif_recv for array a %d\n",errno);
	else
		printf("mic 0 recv for array a success!\n");

        double Dx,d_x;
	double dt;
        double tstim,stimtime,ist;
	double t=0;
        int ibeat=0;
	int  Nryr = 100;
	int Ndhpr = 15;
	int bcl=1000;
        int beats; 
        int steps;

	int offset_x;
        int offset_y;
	int offset_z;

        int total_x;
        int total_y;
        int total_z;
    

	int N_x,N_y,N_z;
	int Ndyads;
	int micsignal=0;
	
	int pstart,pend;

        int Unit_X,Unit_Y,Unit_Z;
        int my_rank;
        N_x = P->N_x;
        N_y = P->N_y;
        N_z = P->N_z;
        Ndyads = P->Ndyads;
        bcl = P->bcl;
        dt = P->dt;
        offset_x = P->offset_x;
        offset_y = P->offset_y;
        offset_z = P->offset_z;
        total_x = P->total_x;
        total_y = P->total_y;
        total_z = P->total_z;
        Dx = P->Dx;
        d_x = P->d_x;
        beats = P->beats;
        Unit_X = P->Unit_X;
        Unit_Y = P->Unit_Y;
        Unit_Z = P->Unit_Z;

        my_rank = P->rank;
        steps = P->steps;
        printf("Ndyads = %d\n",Ndyads);
        fflush(stdout);

        pstart = P->start;
        pend = P->end;
        

        int NN = 101;
        double **T;
        double *basePtr = (double *)_mm_malloc(NN*NN*sizeof(double),ALIGN);
        T = (double **)_mm_malloc(NN*sizeof(double *),ALIGN);
        for(int i=0;i<NN;i++)
        T[i] = basePtr+i*NN;
    
        for(int L=0;L<NN;L++)
       	    for(int C=0;C<=L;C++)
            {
           	 if((C==0)||(L==C))
                      T[L][C] = 1;
           	 else
              	      T[L][C] = T[L-1][C]+T[L-1][C-1];
            }

   	 double p=0.5*dt;
   	 double **B;
   	 double *basePtr1 = (double *)_mm_malloc(NN*NN*sizeof(double),ALIGN);
  	 B = (double **)_mm_malloc(NN*sizeof(double *),ALIGN);
   	 for(int i=0;i<NN;i++)
      	      B[i] = basePtr1+i*NN;
   	 
   	 for(int L=0;L<NN;L++)
   	 {
    	      double p_knk=pow((1-p),L);
     	      double p1p=p/(1-p);
     	      B[L][0]=T[L][0]*p_knk;
      	      for(int C=1;C<=L;C++)
      	      {
		    p_knk = p_knk*p1p;
       	            B[L][C]=B[L][C-1]+T[L][C]*p_knk;            
      	      }
    	 }

    	 double *ZT = (double *)_mm_malloc(NN*sizeof(double),ALIGN);
    	 for(int L=0;L<NN;L++)
              ZT[L]=pow((1-0.99999999),L);

    	tstim = 50;
    	stimtime = 100;

	scif_send(source,&N_x,sizeof(int),SCIF_SEND_BLOCK);


        int c_size = pend-pstart;
        scif_send(source,&c_size,sizeof(int),SCIF_SEND_BLOCK);
        
	double *v = (double *)_mm_malloc(c_size*sizeof(double),ALIGN);

	
	struct cell *subcells;
        subcells = (struct cell*)malloc(c_size*sizeof(struct cell));
    


	
	omp_set_num_threads(NUMTHREADS);
	
	
        double dtstep=0;
	
	for(int I=pstart;I<pend;I++)
	{
            InitCellValue(subcells[I-pstart],Unit_X,Unit_Y,Unit_Z,Nryr,Ndhpr,beats);
        }
	

        for(int I=pstart;I<pend;I++)
        {
            int cx = I/((N_y-2)*(N_z-2))+1;
     	    int cy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
     	    int cz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
	
            compute_cell_id(subcells[I-pstart], cx,cy,cz, total_x,total_y,total_z,offset_x,offset_y,offset_z);//also initialize the data random seed
	    v[I-pstart] = -87.6;
    
	}
	
	printf("cell id : %d \n",subcells[0].cellID);
        
	
	int goon=1;
	int semaphor=-1;
	
	double *v_base = v;
	int v_size = c_size*sizeof(double);
	if((v_size%ALIGN)!=0)
	       v_size = (v_size/ALIGN+1)*ALIGN;
	off_t self_offset;
    	off_t peer_offset;
    	int  control_msg;
        
        
        
        
        #ifdef REG
        BARRIER(source,"Initial done on MIC");
        printf("initial done on MIC\n");
        
	if ((self_offset = scif_register (source,
					(void *)v_base,
					v_size,
					START_OFFSET*(phiid+1),
					SCIF_PROT_READ | SCIF_PROT_WRITE,
					SCIF_MAP_FIXED )) < 0) {
		printf("scif_register on MIC failed with error");
		
	}//return START_OFFSET
	BARRIER(source,"Register done on MIC");
	#endif

        printf("registered buffer on MIC at offset 0x%lx\n", (unsigned long)self_offset);
        
        
        
	printf ("steps on MIC; %d\n",steps);
	
        
        
    	
    	int total = (N_x-2)*(N_y-2)*(N_z-2);	
    	int self = 0;
    	int rwmsg=0;
    	double test1 = 0;
    	
    	int start_signal = 888;
    	scif_send(source,&start_signal,sizeof(int),SCIF_SEND_BLOCK);
    	
    	          
    	#ifdef OPENMPONCELL
	#pragma omp parallel 
	#endif
	{
  
                for(int icounter=0;icounter<steps;icounter++)
		{
		#ifdef OPENMPONCELL
                #pragma omp for
                #endif
                for(int I=pstart;I<pend;I++)
                {
		        	int ix = I/((N_y-2)*(N_z-2))+1;
			        int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
			        int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
                //for (int ix = 1; ix<N_x-1; ix++)
                //    for (int iy = 1; iy<N_y-1; iy++)
                //        for(int iz = 1; iz<N_z-1;iz++)               
                            {
                               
                                comp_cell(subcells[I-pstart],I-pstart,ix,iy,iz, offset_x, offset_z, t, ist, bcl, v, dtstep, Ndhpr,Ntotdyads,Nryr,T,B,ZT,icounter,steps);
                                dtstep = dt;
                            }
                 }

                //////////////////////////////////????????????????????????????????????????//////////////////////////
                //#ifdef OPENMPONCELL	
                //#pragma omp for
                //#endif
                
                #ifdef diffusion
                for(int I=0;I<total;I++)
                {
		        	int ix = I/((N_y-2)*(N_z-2))+1;
			        int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
			        int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
                //for (int ix = 1; ix<N_x-1; ix++)
                //    for (int iy = 1; iy<N_y-1; iy++)
                //        for(int iz = 1; iz<N_z-1;iz++)
                            v[ix][iy][iz]+= dt*Dx*(vtemp[ix+1][iy][iz]+vtemp[ix-1][iy][iz]+vtemp[ix][iy-1][iz]+\
                                            vtemp[ix][iy+1][iz]+vtemp[ix][iy][iz-1]+vtemp[ix][iy][iz+1]-6*vtemp[ix][iy][iz])/(d_x*d_x);
                }
                #endif
                
                
                #ifdef OPENMPONCELL
                #pragma omp single
                #endif
                {
                      BARRIER(source,"update v on MIC done");
                      
                      
                      RECV(source,rwmsg,sizeof(int));
                      
                      #ifdef SCIFT
                      //SEND(source,v,sizeof(double)*c_size);
                      if (scif_send(source, &v[0], c_size*sizeof(double), SCIF_SEND_BLOCK) < 0){
                           printf("send votalge error on phi!\n");
                      }
                      #endif
                      
                      
                      /* scif_fence_mark : Marks the current set of uncompleted RMAs initiated
		      * through endpt or corresponding peer endpt which uses flags
		      * SCIF_FENCE_INIT_SELF and SCIF_FENCE_INIT_PEER respectively.
		      * The RMAs are marked with a value in mark.
		      * scif_fence_wait : Waits for the completion of RMAs marked with value
		      * in mark. As we dont have any RMAs intiatedfrom newepd, self = 0.
		      * We are fencing against scif_writeto operations initiatedy peer end point
		      */
		      
		      FENCE(source, self);                 
                                           
                      //BARRIER(source,"wait host update v ");
                      
                      RECV(source,rwmsg,sizeof(int));
                      FENCE(source, self);
                      #ifdef SCIFT
                      //RECV(source,v,sizeof(double)*c_size);
                      if (scif_recv(source,&v[0],c_size*sizeof(double), SCIF_RECV_BLOCK) < 0){
                          printf("recieve votlage error on phi!\n");
                       }
                      #endif
                }

		//#ifdef OPENMPONCELL
                //#pragma omp for   
                //#endif      
                
                #ifdef diffusion  
                for (int ix = 0; ix<N_x; ix++)
                    for (int iy = 0; iy<N_y; iy++)
                        for(int iz = 0;iz<N_z;iz++)
                        {                                                                     
                            vtemp[ix][iy][iz] = v[ix][iy][iz];
                        }
                 #endif
               
                #ifdef OPENMPONCELL   
                #pragma omp single
                #endif
                {
                    if (t>=tstim && t<(tstim+dt))
                    {
                        stimtime = 0;
                        ibeat = ibeat+1;
                        tstim = tstim+bcl;
                        if (my_rank==0)
                            cout<<"ibeat is :"<<ibeat<<endl;

                    }
                    if (stimtime>=0 && stimtime<=0.5)
                        ist = -80;
                    else
                        ist = 0;                
                    t+=dt;
                    stimtime += dt;

                }

	    }
	}
	
        printf("out of time loop on MIC\n");
    	

	scif_recv(source,&semaphor,sizeof(int),SCIF_RECV_BLOCK);
	if(semaphor==3){
	    semaphor = 0;
	    scif_send(source,&semaphor,sizeof(int),SCIF_SEND_BLOCK);
    	}


    #ifdef PRINT
        printf("Number of random data used in cell 0=%lld\n",subcells[0].RandUsedN);
        printf("time info!\n");
        printf("|--comp_cell=%g\n",subcells[0].T_cell);
        printf("   |--comp_ina=%g  percentage=%g \n",subcells[0].T_ina,(subcells[0].T_ina)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_ito=%g  percentage=%g \n",subcells[0].T_ito,(subcells[0].T_ito)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_ikr=%g  percentage=%g \n",subcells[0].T_ikr,(subcells[0].T_ikr)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_iks=%g  percentage=%g \n",subcells[0].T_iks,(subcells[0].T_iks)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_ik1=%g  percentage=%g \n",subcells[0].T_ik1,(subcells[0].T_ik1)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_inak=%g  percentage=%g \n",subcells[0].T_inak,(subcells[0].T_inak)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_inab=%g  percentage=%g \n",subcells[0].T_inab,(subcells[0].T_inab)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_calc_dyad=%g  percentage=%g \n",subcells[0].T_calc_dyad,(subcells[0].T_calc_dyad)/(subcells[0].T_cell));fflush(stdout);
        
        printf("      |--comp_randomG=%g  percentage=%g \n",subcells[0].T_randomG,(subcells[0].T_randomG)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_prepare=%g  percentage=%g \n",subcells[0].T_prepare,(subcells[0].T_prepare)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_ical_dyad=%g  percentage=%g \n",subcells[0].T_ical_dyad,(subcells[0].T_ical_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_L_Channel=%g  percentage=%g \n",subcells[0].T_L_Channel,(subcells[0].T_L_Channel)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_ical_dyad1=%g  percentage=%g \n",subcells[0].T_ical_dyad1,(subcells[0].T_ical_dyad1)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_irel_dyad=%g  percentage=%g \n",subcells[0].T_irel_dyad,(subcells[0].T_irel_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_irel1=%g  percentage=%g \n",subcells[0].T_irel1,(subcells[0].T_irel1)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_irel2=%g  percentage=%g \n",subcells[0].T_irel2,(subcells[0].T_irel2)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_RyrOpen=%g  percentage=%g \n",subcells[0].T_RyrOpen,(subcells[0].T_RyrOpen)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_irel3=%g  percentage=%g \n",subcells[0].T_irel3,(subcells[0].T_irel3)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_conc_dyad=%g  percentage=%g \n",subcells[0].T_conc_dyad,(subcells[0].T_conc_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_ica_dyad=%g  percentage=%g \n",subcells[0].T_ica_dyad,(subcells[0].T_ica_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_conc_diff=%g  percentage=%g \n",subcells[0].T_conc_diff,(subcells[0].T_conc_diff)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_acculate=%g  percentage=%g \n",subcells[0].T_acculate,(subcells[0].T_acculate)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("   |--comp_ical=%g  percentage=%g \n",subcells[0].T_ical,(subcells[0].T_ical)/(subcells[0].T_cell));
        printf("   |--comp_rest=%g  percentage=%g \n",subcells[0].T_rest,(subcells[0].T_rest)/(subcells[0].T_cell));
        #endif
    

   	_mm_free(T[0]);
	_mm_free(T);
	_mm_free(B[0]);
	_mm_free(B);
	_mm_free(ZT);
	
	for(int I=pstart;I<pend;I++)
	{
        //for(int cx = 1; cx < N_x-1; cx++)
	//    for(int cy =1; cy < N_y-1; cy++)
	//	for(int cz = 1;cz < N_z-1;cz++)
               releaseCell(subcells[I-pstart]);
         }

        //PHIdeallocate_3D_double (v,N_x);
        //PHIdeallocate_3D_double (vtemp,N_x);
        //PHIdeallocate3DSubCells(subcells,N_x);
        _mm_free(v);
        free(subcells);

	return 0;

}
