#include "RectPart2D.h"
#include <cmath>
#include <malloc.h>



Partitioner_t * Partitioner_construct(MPI_Comm comm, int N1, int N2,int N3, int num_procs1, int num_procs2,int num_procs3)
{
	Partitioner_t* partitioner = (Partitioner_t *)malloc(sizeof(Partitioner_t));
	partitioner->mpi_info.comm = comm;
        MPIInfo *mpi_Info;
	mpi_Info = &(partitioner->mpi_info);

	MPI_Comm_size (comm, &(mpi_Info->total_MPI_procs));
	MPI_Comm_rank (comm, &(mpi_Info->my_rank));

	int i,j,k;
	if (mpi_Info->total_MPI_procs != (num_procs1*num_procs2*num_procs3)) {
	  i = cbrt(mpi_Info->total_MPI_procs);
	  for (j=i; j>=1; j--)
		if ((mpi_Info->total_MPI_procs%j) == 0) {
	  num_procs3 = j;
	  k=mpi_Info->total_MPI_procs/j;
		  i=sqrt(k);
	  for(j=i;j>=1;j--)
		  if((k%j)==0) {
		  num_procs2 = j;
		  j = 0;
		  }
	  j = 0;
		}
	 num_procs1 = mpi_Info->total_MPI_procs/(num_procs2*num_procs3);
	}

	mpi_Info->dims[0] = num_procs1;
	mpi_Info->dims[1] = num_procs2;
	mpi_Info->dims[2] = num_procs3;

	partitioner->ndims = NDIMS;


	int periods[NDIMS] = {0,0,0};
	int reorganisation =0;

	MPI_Cart_create(comm,partitioner->ndims,mpi_Info->dims,
					periods,reorganisation,&(mpi_Info->comm3d));
	MPI_Cart_get(mpi_Info->comm3d,NDIMS,mpi_Info->dims,
				 periods,mpi_Info->coord);

	for(int i=0;i<NDIMS*2;i++)
	  mpi_Info->neighbors[i] = MPI_PROC_NULL;
	MPI_Cart_shift(mpi_Info->comm3d,0,1,&(mpi_Info->neighbors[0]),&(mpi_Info->neighbors[1]));
	MPI_Cart_shift(mpi_Info->comm3d,1,1,&(mpi_Info->neighbors[2]),&(mpi_Info->neighbors[3]));
	MPI_Cart_shift(mpi_Info->comm3d,2,1,&(mpi_Info->neighbors[4]),&(mpi_Info->neighbors[5]));

   	partitioner->N[0] = N1;
	partitioner->N[1] = N2;
	partitioner->N[2] = N3;

	for(int di=0;di<partitioner->ndims;di++)
	{
	   i = mpi_Info->coord[di]*partitioner->N[di]/mpi_Info->dims[di];
	   j = (mpi_Info->coord[di]+1)*partitioner->N[di]/mpi_Info->dims[di];
	   partitioner->my_offset[di] = i;
	   partitioner->sub_N[di] = j-i+2;
	}

	printf("<%d> coord(%d,%d,%d) sub_N[%d,%d,%d] offset[%d,%d,%d] neighbors[%d,%d,%d,%d,%d,%d]\n",mpi_Info->my_rank,
		  mpi_Info->coord[0],mpi_Info->coord[1],mpi_Info->coord[2],
		  partitioner->sub_N[0],partitioner->sub_N[1],partitioner->sub_N[2],
		  partitioner->my_offset[0],partitioner->my_offset[1],partitioner->my_offset[2],
		  mpi_Info->neighbors[0],mpi_Info->neighbors[1],mpi_Info->neighbors[2],
		  mpi_Info->neighbors[3],mpi_Info->neighbors[4],mpi_Info->neighbors[5]);
	
  
	for(i=0;i<2*NDIMS;i++)
	{
		int loc_dim = i/2;
		unsigned int left_right = i%2;
		//YZ sheet (every thing is contigous)
		if(loc_dim == 0)
		{
			if(left_right == 0)
			{
			  partitioner->v_offsets[i]=partitioner->sub_N[2]+1;
			  partitioner->v_inner_offsets[i]=partitioner->sub_N[1]*partitioner->sub_N[2]+partitioner->sub_N[2]+1;
			}
			else
			{
			  partitioner->v_offsets[i]=(partitioner->sub_N[0]-1)*partitioner->sub_N[1]*partitioner->sub_N[2]+partitioner->sub_N[2]+1;
			  partitioner->v_inner_offsets[i]=partitioner->v_offsets[i]-partitioner->sub_N[1]*partitioner->sub_N[2];
			}
			partitioner->v_offsets1[i]=partitioner->sub_N[2];
			partitioner->v_offsets2[i]=1;
			partitioner->plane[i][0]=partitioner->sub_N[1]-2;
			partitioner->plane[i][1]=partitioner->sub_N[2]-2;

			partitioner->size_ghost_values[i]=(partitioner->sub_N[1]-2)*(partitioner->sub_N[2]-2);
		}
		else if(loc_dim==1)
		{
			if(left_right==0)
			{
			  partitioner->v_offsets[i]=partitioner->sub_N[1]*partitioner->sub_N[2]+1;
			  partitioner->v_inner_offsets[i]=partitioner->v_offsets[i]+partitioner->sub_N[2];
			}
			else
			{
			  partitioner->v_offsets[i]=partitioner->sub_N[1]*partitioner->sub_N[2]+(partitioner->sub_N[1]-1)*partitioner->sub_N[2]+1;
			  partitioner->v_inner_offsets[i]=partitioner->v_offsets[i]-partitioner->sub_N[2];
			}
			partitioner->v_offsets1[i]=partitioner->sub_N[1]*partitioner->sub_N[2];
			partitioner->v_offsets2[i]=1;
			partitioner->plane[i][0]=partitioner->sub_N[0]-2;
			partitioner->plane[i][1]=partitioner->sub_N[2]-2;
	   
			partitioner->size_ghost_values[i]=(partitioner->sub_N[0]-2)*(partitioner->sub_N[2]-2);
		}
		else
		{
			if(left_right==0)
			{
			  partitioner->v_offsets[i]=partitioner->sub_N[1]*partitioner->sub_N[2]+partitioner->sub_N[2];
			  partitioner->v_inner_offsets[i]=partitioner->v_offsets[i]+1;
			}
			else
			{
			  partitioner->v_offsets[i]=partitioner->sub_N[1]*partitioner->sub_N[2]+2*partitioner->sub_N[2]-1;
			  partitioner->v_inner_offsets[i]=partitioner->v_offsets[i]-1;
			}
			partitioner->v_offsets1[i]=partitioner->sub_N[1]*partitioner->sub_N[2];
			partitioner->v_offsets2[i]=partitioner->sub_N[2];
			partitioner->plane[i][0]=partitioner->sub_N[0]-2;
			partitioner->plane[i][1]=partitioner->sub_N[1]-2;

			partitioner->size_ghost_values[i]=(partitioner->sub_N[0]-2)*(partitioner->sub_N[1]-2);
		}

		partitioner->ghost_value_send[i] = (double *)malloc(partitioner->size_ghost_values[i]*sizeof(double));
		partitioner->ghost_value_recieve[i] = (double *)malloc(partitioner->size_ghost_values[i]*sizeof(double));	
	}
	return partitioner;
}


void Partitioner_destruct(Partitioner_t *partitioner)
{
  	int i;
	for(i=0;i<2*(partitioner->ndims);i++)
	{
	  	free(partitioner->ghost_value_send[i]);
		free(partitioner->ghost_value_recieve[i]);
	}
	free(partitioner);
	return;
}




