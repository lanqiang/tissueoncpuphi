#include "PackMsg.h"

void PackVOnHost(double ***v,int N_x,int N_y,int N_z,double *v_out_msg,int start,int end)
{
     for(int I = start;I<end;I++)
     {
     	int ix = I/((N_y-2)*(N_z-2))+1;
     	int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
     	int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
     	v_out_msg[I-start] = v[ix][iy][iz];
     }
     return;
}

void UnPackVOnHost(double ***v,int N_x,int N_y,int N_z,double *v_in_msg,int start,int end)
{
     for(int I = start;I<end;I++)
     {
     	int ix = I/((N_y-2)*(N_z-2))+1;
     	int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
     	int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
     	v[ix][iy][iz] = v_in_msg[I-start];
     }
     return;
}
