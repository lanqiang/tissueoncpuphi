#include <stdlib.h>
#include <stdio.h> 
#include <assert.h> 
#include <string.h>
#include <scif.h>
#include <source/COIProcess_source.h>
#include <source/COIEngine_source.h>

#include "../common/common.h"

#define PORTNUMBER0 3000
#define PORTNUMBER1 3020

#define PHINUMMAX 4

unsigned int PORTNUMBER[PHINUMMAX] = {3000,3020,3040,3060};

#ifndef ONLYCPU

void PHIdevicesetupSCIF(PHItype *P,int phiid,int N_x, int N_y, int N_z,int offset_x, int offset_y,int offset_z, int Ndyads, int bcl, double dt,double Dx,double d_x,int Unit_X,int Unit_Y,int Unit_Z,int Ndhpr,int beats,int global_N_x,int global_N_y,int global_N_z,int rank,int steps,int start,int end)
{

	COIRESULT result0 = COI_ERROR;
	COIPROCESS proc0;
	COIENGINE engine0;
	unsigned int num_engines = 0;
	
	const char *SINK_NAME0 = "connect";



	result0 = COIEngineGetCount(COI_ISA_KNC, &num_engines);
	if(result0!=COI_SUCCESS)
	{
		printf("COIEngineGetCount result %s\n",	COIResultGetName(result0));
		return;
	}
	if(num_engines<1)
	{
		printf("Error: Need at least 1 engine!\n");
		return;
	}
	printf("the total engines num is %d\n",num_engines);

	result0 = COIEngineGetHandle(COI_ISA_KNC,phiid,&engine0);
	if(result0!=COI_SUCCESS)
	{
		printf("COIEngineGetHandle result0 %s\n",COIResultGetName(result0));
		return;
	}
	else
		printf("COIEngineGetHandle0 success!\n");

	scif_epd_t source0;
	unsigned int portNum0;
	struct scif_portID portID0;
//	scif_epd_t dst0;
	
	//first call scif_open()
	source0 = scif_open();
	if(source0 == ((scif_epd_t)-1))
	{
		printf("scif open failed!\n");
	}

	else 
		printf("scif open success !\n");
	portNum0 = PORTNUMBER[phiid];
	
	//then bind the port 
	if(scif_bind(source0,portNum0)<0)
	{
		printf("scif bind failed!\n");
		return;
	}
	else 
		printf("scif bind success!\n");


	//listen some endpoint to connect
	if(scif_listen(source0,5)<0)
	{
		printf("scif listen failed!\n");
		return;
	}

	else 
		printf("host listen success!");

        int sink_argv_MaxLenth=128;
        int sink_argc=2;
        const char *sink_argv[1];
        //sink_argv[0] = (char *)malloc(sink_argv_MaxLenth*sizeof(char));
        if(phiid==1)
            sink_argv[0] = {"1"};
        else 
            sink_argv[0] = {"0"};
        
	result0 = COIProcessCreateFromFile(engine0,
                                        SINK_NAME0,
                                        sink_argc,sink_argv,
                                        0,NULL,
                                        1,NULL,
                                        0,
                                        NULL,
                                        &proc0
        );
        if(result0!=COI_SUCCESS)
        {
                printf ("COIProcessCreateFromFile result %s\n",COIResultGetName (result0));

                return;
        }
        else
                printf("COIProcessCreateFromFile success!\n");





	//accept if some remote endpoint connect 
	if(scif_accept(source0,&portID0,&P->dst0,SCIF_ACCEPT_SYNC)<0)
	{
		printf("scif accept mic0 failed!\n");
		return;
	}

	else
		printf("host accept mic0 success!\n");

			//	int start = L->GPUstart[0];
			//int size  = L->GPUstart[L->GPUs]-L->GPUstart[0];
	int phiID = -3;
        scif_recv(P->dst0,&phiID,sizeof(int),SCIF_RECV_BLOCK);
        printf("recieve phi id %d\n",phiID);

    P->N_x = N_x;
    P->N_y = N_y;
    P->N_z = N_z;
    P->Ndyads = Ndyads;
    P->bcl = bcl;
    P->dt = dt;
    P->offset_x = offset_x;
    P->offset_y = offset_y;
    P->offset_z = offset_z;
    P->total_x = global_N_x;
    P->total_y = global_N_y;
    P->total_z = global_N_z;
    P->Dx = Dx;
    P->d_x = d_x;
    P->beats = beats;
    P->Unit_X = Unit_X;
    P->Unit_Y = Unit_Y;
    P->Unit_Z = Unit_Z;
    P->rank = rank;
    P->steps = steps;
    P->start = start;
    P->end = end;

	scif_send(P->dst0,P,sizeof(PHItype),SCIF_SEND_BLOCK);

    int temp=-1;
	scif_recv(P->dst0,&temp,sizeof(int),SCIF_RECV_BLOCK);
	printf("N_X recieved is = %d\n",temp);
	
	int cell_size = 0;
	scif_recv(P->dst0,&cell_size,sizeof(int),SCIF_RECV_BLOCK);
	printf("cell size on phi %d is = %d\n",phiID,cell_size);

/*	scif_send(P->dst0,&Nx,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&Ny,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&Nz,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&ndyads,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&bcl,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&dt,sizeof(double),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&offset_x,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&offset_z,sizeof(int),SCIF_SEND_BLOCK);
*/

	int threads= -1;

	//scif_recv(P->dst0,&threads,sizeof(int),SCIF_RECV_BLOCK);

	printf("Running %d  threads\n",threads);
	

	return;

}

void PHIcomputeSCIF(PHItype *P,int phinum, int icounter)
{
	int semaphor = 1;
	scif_send(P->dst0,&semaphor,sizeof(int),SCIF_SEND_BLOCK);
	scif_send(P->dst0,&icounter,sizeof(int),SCIF_SEND_BLOCK);
	if(scif_recv(P->dst0,&semaphor,sizeof(int),SCIF_RECV_BLOCK)<0)
    {
        printf("recieve phi compute signal failed!\n");
        return;
    }
}
/*
void PHIswapSCIF(int phinum, localPHItype* P)
{
	int semaphor = 2;
	scif_send(P->dst0,&semaphor,sizeof(int),SCIF_SEND_BLOCK);
	scif_recv(P->dst0,&semaphor,sizeof(int),SCIF_RECV_BLOCK);
}
*/

void PHIfinishSCIF(PHItype *P,int phinum)
{
	int semaphor = 3;
	scif_send(P->dst0,&semaphor,sizeof(int),SCIF_SEND_BLOCK);
	if(scif_recv(P->dst0,&semaphor,sizeof(int),SCIF_RECV_BLOCK)<0)
    {
        printf("recieve finish signal failed!\n");
        return;
    }
    else if(semaphor==0)
        printf("recieve phi finished signal success!\n");
    else
            printf("wrong finish signal!\n");
      
}

#endif
