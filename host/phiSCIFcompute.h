#include "../common/common.h"
void PHIdevicesetupSCIF(PHItype *P,int phiid,int N_x, int N_y, int N_z,int offset_x, int offset_y,int offset_z, int Ndyads, int bcl, double dt,double Dx,double d_x,int Unit_X,int Unit_Y,int Unit_Z,int Ndhpr,int beats,int global_N_x,int global_N_y,int global_N_z,int rank,int steps,int start,int end);
void PHIcomputeSCIF(PHItype *P,int phinum, int icounter);
void PHIfinishSCIF(PHItype *P,int phinum);
