#include "RectPart2D.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
#include <cstring>

#include <omp.h>
#include <malloc.h>
#include <stdlib.h>

#include <unistd.h>

//#include "../common/common.h"
#include "phiSCIFcompute.h"
#include "../common/compute_cell.h"
#include "allocArray.h"

#include "UpdateGhost.h"
#include "postprocess.h"


#include "../common/printInfo.h"
#include <time.h>

#include "PackMsg.h"


#define ALIGN 0x1000

//#define debug


#define Ntotdyads 10000

#define PAGE_SIZE 0x1000
#define START_OFFSET 0x800000

//printf("%s\n", string); 
#define BARRIER(epd, string) { \
	if ((err = scif_send(epd, &control_msg, sizeof(control_msg), 1)) <= 0) { \
		printf("scif_send failed with err %d\n", errno); \
		fflush(stdout); \
	} \
	if ((err = scif_recv(epd, &control_msg, sizeof(control_msg), 1)) <= 0) { \
		printf("scif_recv failed with err %d\n", errno); \
		fflush(stdout); \
	} \
}
	//printf("==============================================================\n"); 

#define FENCE(newepd, self) { \
        if ((err = scif_fence_mark(newepd, \
                        self? SCIF_FENCE_INIT_SELF : SCIF_FENCE_INIT_PEER, \
                        &mark))) { \
                printf("scif_fence_mark failed with err %d\n", errno); \
        } \
        if ((err = scif_fence_wait(newepd, mark))) { \
                printf("scif_fence_wait failed with err %d\n", errno); \
        } \
}

//printf("Value of mark 0x%x\n", mark); \

#define SEND(newepd,sbuffer,size){\
	if (scif_send (newepd, &sbuffer, size, SCIF_SEND_BLOCK) < 0){\
		switch (errno){\
			case ECONNRESET:printf ("[SEND]A connection was forcibly closed by a peer.\n");break;\
			case EFAULT:printf ("[SEND]An invalid address was specified for a parameter.\n");break;\
			case EINTR:printf("[SEND]epd was closed by scif_close().\n");break;\
			case EINVAL:printf("[SEND]epd is not a valid endpoint descriptor, or flags is invalid or len is negative\n");break;\
			case ENODEV:printf("[SEND]The remote node is lost.\n");break;\
			case ENOMEM:printf("[SEND]Not enough space\n");break;\
			case ENOTCONN:printf("[SEND]The endpoint is not connected\n");break;\
			case ENOTTY:printf("[SEND]ENOTTY\n");break; \ 
			default:printf("SEND]Unknown exit reason %d\n", errno);\
		}\
  }\
}

#define RECV(newepd,sbuffer,size){\
 if (scif_recv (newepd,&sbuffer,size, SCIF_RECV_BLOCK) < 0){\
      printf ("[MIC1] Error in scif_send %d\n", errno);\
 }\
}




using namespace std;
//

double ***v;
double ***vtemp;
double ***flagArray;
double ***dtstep;
double t,dt,d_x,Dx;
double dx;
double tstim,stimtime,ist;
int steps,ibeat;

#ifdef TEST
int bcl=200;
#else
//int bcl=500;
int bcl=2000;
#endif

ofstream fout;
ofstream f0out;
ostringstream file;
ostringstream file0;


int main(int nargs, char** args)
{
        //dt=0.01;
	dt = 0.05;
	int ut = (int)(0.1/dt);
	d_x = 0.5; //dx for voltage
	dx = 0.2;
	//Dx = 0.1;
        Dx = 0.2;

	
	//tstim = 100;
	tstim = 50;
	stimtime = 100;

        int err; 
        int control_msg;
        int mark;
	//***************************************************
	Partitioner_t* partitioner;
	int num_procs1=0, num_procs2=0,num_procs3=0;
	int global_N_x,global_N_y,global_N_z;
	int  Nryr = 100;
	int Ndhpr = 15;
	int Ndyads = 100;
	int beats = 1;
	int Unit_X=100;
	int Unit_Y=1;
	int Unit_Z=1;	


	steps = beats*bcl/dt;
        int N_x, N_y,N_z;
        char *output_dir=NULL; 
	int storeStrideX=1,storeStrideY=1,storeStrideZ=1;
	int StoreS=50;
	
	MPI_Init (&nargs, &args);
	if (nargs>1)
            global_N_x = atoi(args[1]);
	if (nargs>2)
            global_N_y = atoi(args[2]);
	if(nargs>3)
	    global_N_z = atoi(args[3]);
	if (nargs>4)
            num_procs1 = atoi(args[4]);
	if (nargs>5)
            num_procs2 = atoi(args[5]);
	if(nargs>6)
	    num_procs3 = atoi(args[6]);
	
	if(nargs>7)
	    Ndyads = atoi(args[7]);
	if(nargs>8)
	    Unit_X = atoi(args[8]);
	if(nargs>9)
	    Unit_Y = atoi(args[9]);
	if(nargs>10)
	    Unit_Z = atoi(args[10]);
     
        if(nargs>11)
	    storeStrideX = atoi(args[11]);
	if(nargs>12)
	    storeStrideY = atoi(args[12]);
	if(nargs>13)
	    storeStrideZ = atoi(args[13]);
	if(nargs>14)
	    output_dir = args[14];

        int offsetX,offsetY,offsetZ;
	offsetX = partitioner->my_offset[0];
	offsetY = partitioner->my_offset[1];
	offsetZ = partitioner->my_offset[2];
        int s_ix,s_iy,s_iz;
	int remain;
	int ds;
	remain = offsetX%storeStrideX;
	if(remain==0)
		s_ix=1;
	else
	{
		ds = storeStrideX - remain;
		s_ix = ds+1;
	}	
	remain = offsetY%storeStrideY;
	if(remain==0)
		s_iy=1;
	else
	{
		ds = storeStrideY - remain;
		s_iy = ds+1;
	}	
	remain = offsetZ%storeStrideZ;
	if(remain==0)
		s_iz=1;
	else
	{
		ds = storeStrideZ - remain;
		s_iz = ds+1;
	}	
       

	if(Ndyads!=Unit_X*Unit_Y*Unit_Z)
	{
		Ndyads = 100;
		Unit_X = 100;
		Unit_Y = 1;
		Unit_Z = 1;
	}

        printf("hello 0\n");
        MPI_Comm comm=MPI_COMM_WORLD;
	partitioner = Partitioner_construct(comm,global_N_x,global_N_y,global_N_z,
				                        num_procs1,num_procs2,num_procs3);
	N_x = partitioner->sub_N[0];
	N_y = partitioner->sub_N[1];
	N_z = partitioner->sub_N[2];
	
	printf("hello 1\n");
	
        file << "file_" << partitioner->mpi_info.my_rank;	
	fout.open(file.str().c_str());	
	
	
	
	int NN = 101;
    double **T;
    double *basePtr = (double *)_mm_malloc(NN*NN*sizeof(double),ALIGN);
    T = (double **)_mm_malloc(NN*sizeof(double *),32);
    for(int i=0;i<NN;i++)
        T[i] = basePtr+i*NN;
    //double T[101][101];
    for(int L=0;L<NN;L++)
        for(int C=0;C<=L;C++)
        {
            if((C==0)||(L==C))
                T[L][C] = 1;
            else
                T[L][C] = T[L-1][C]+T[L-1][C-1];
        }

    double p=0.5*dt;
    double **B;
    double *basePtr1 = (double *)_mm_malloc(NN*NN*sizeof(double),ALIGN);
    B = (double **)_mm_malloc(NN*sizeof(double *),32);
    for(int i=0;i<NN;i++)
        B[i] = basePtr1+i*NN;
    //double T[101][101];
    for(int L=0;L<NN;L++)
    {
        double p_knk=pow((1-p),L);
        double p1p=p/(1-p);
        B[L][0]=T[L][0]*p_knk;
        for(int C=1;C<=L;C++)
        {
	    p_knk = p_knk*p1p;
            B[L][C]=B[L][C-1]+T[L][C]*p_knk;            
        }
    }

    double *ZT = (double *)_mm_malloc(NN*sizeof(double),ALIGN);
    for(int L=0;L<NN;L++)
        ZT[L]=pow((1-0.99999999),L);
	
	
	

	v = allocate_3D_double(N_x,N_y,N_z);
	memset(v[0][0],-87.6,N_x*N_y*N_z*sizeof(double));
	vtemp = allocate_3D_double(N_x,N_y,N_z);
	memset(vtemp[0][0],0,N_x*N_y*N_z*sizeof(double));
	
        double dtstep = 0;
              
	file0 << "cell";

        if (partitioner->mpi_info.my_rank == 0)
		f0out.open(file0.str().c_str());

	int offset_x = partitioner->my_offset[0];
	int offset_y = partitioner->my_offset[1];
	int offset_z = partitioner->my_offset[2];
	int rank = partitioner->mpi_info.my_rank;

	int total = (N_x-2)*(N_y-2)*(N_z-2);	
	int blocks = total/OptiPhiThreads;
	int pstart[PhiNUM+1];
	int pend[PhiNUM+1];
	
	#ifdef ONLYPHI
	pstart[0] = 0;                 //cpu start &end
	pend[0] = 0;
	int phisize = total/PhiNUM;
	for(int i=1;i<PhiNUM+1;i++)
	{
	    pstart[i] = pend[i-1];
	    pend[i] = pstart[i] + phisize;
	}
	#else 
	#ifdef ONLYCPU
	pstart[0] = 0;                 //cpu start &end
	pend[0] = total;
	int phisize = 0;
	for(int i=1;i<PhiNUM+1;i++)
	{
	    pstart[i] = pend[i-1];
	    pend[i] = pstart[i] + phisize;
	}
	
	#else	
	double divisor=1+PhiNUM*PHISpeedFactor;
	int phisize = OptiPhiThreads*(int)(blocks*PHISpeedFactor/divisor);
	printf("phisize computed on host = %d\n",phisize);
	pstart[0] = 0;                 //cpu start &end
	pend[0] = total-phisize*PhiNUM;
	for(int i=1;i<PhiNUM+1;i++)
	{
	    pstart[i] = pend[i-1];
	    pend[i] = pstart[i] + phisize;
	    
	    printf("pstart[%d] = %d pend[%d]=%d \n",i,pstart[i],i,pend[i]);
	}
	#endif //end of def ONLYCPU
	#endif  //end of def ONLYPHI
	
	printf("cell size for PHI = %d \n",phisize);
	
	
	int c_size = pend[0]-pstart[0];
	struct cell *subcells;
        subcells = (struct cell*)malloc(c_size*sizeof(struct cell));
        printf("cell size for CPU : %d\n",c_size);
        printf("pstart[0] = %d pend[0]=%d \n",pstart[0],pend[0]);
	
	for(int I=pstart[0];I<pend[0];I++)
	{
            InitCellValue(subcells[I-pstart[0]],Unit_X,Unit_Y,Unit_Z,Nryr,Ndhpr,beats);
        }
        
        int total_x = global_N_x;
        int total_y = global_N_y;
        int total_z = global_N_z;
        
        omp_set_num_threads(NUMTHREADS);
        
        #pragma omp parallel
        {
            int threadNum = omp_get_num_threads();
            #pragma omp single
            {
                printf("num of threads on cpu is %d \n",threadNum);
            }
        }
        
        for(int I=pstart[0];I<pend[0];I++)
        {
            int cx = I/((N_y-2)*(N_z-2))+1;
     	    int cy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
     	    int cz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;	
            compute_cell_id(subcells[I-pstart[0]], cx,cy,cz, total_x,total_y,total_z,offset_x,offset_y,offset_z);//also initialize the data random seed
	    v[cx][cy][cz] = -87.6;    
	}
	
	printf("cell id for cell 0 is %d\n",subcells[0].cellID);
	
	double comp_cell_start,comp_cell_end,comp_cell_max;
    	double diffusion_start,diffusion_end,diffusion_max;
    	double comp_cell_time=0,diffusion_time=0;
    	double start;
    	MPI_Barrier(MPI_COMM_WORLD);
    	start = MPI_Wtime();
	
	#ifndef ONLYCPU
	double **v_msg_inoutPtr;
	v_msg_inoutPtr = (double **)malloc(PhiNUM*sizeof(double *));	
	off_t *v_inout;	
	v_inout = (off_t *)malloc(PhiNUM*sizeof(off_t));
	
	PHItype **P = (PHItype **) malloc(sizeof(PHItype*));
	for(int i=0;i<PhiNUM;i++)
	{
	     P[i] = (PHItype *) malloc(sizeof(PHItype));
             PHIdevicesetupSCIF(P[i],i, N_x,  N_y,  N_z, offset_x,  offset_y, offset_z,  Ndyads,  bcl,  dt, Dx, d_x, Unit_X, Unit_Y, Unit_Z, Ndhpr, beats, global_N_x, global_N_y, global_N_z, rank, steps,pstart[i+1],pend[i+1]);
        }
	int v_size;
	for(int i=0;i<PhiNUM;i++)
	{
	    v_size = phisize*sizeof(double);
	    if((v_size%ALIGN)!=0)
	    	v_size = (v_size/ALIGN+1)*ALIGN;
	    v_msg_inoutPtr[i] = (double *)_mm_malloc(phisize*sizeof(double),ALIGN);
	    
	    #ifdef REG
	    BARRIER(P[i]->dst0,"waiting phi initialize");
	    //be careful of the P[i]->dst0 parameter
	    
	    if ((v_inout[i] = scif_register (P[i]->dst0,               
					(void *)v_msg_inoutPtr[i],
					v_size,
					START_OFFSET*(i+1),
					SCIF_PROT_READ | SCIF_PROT_WRITE,
					SCIF_MAP_FIXED )) < 0) {
		printf("scif_register on host failed with error");
		
	     }
	     BARRIER(P[i]->dst0,"Register done on host");
	     #endif
	     
	     printf("regsiter offset on phi %d is 0x%lx\n",i,(unsigned long)v_inout[i]);	     
	}
	
	double *v_base = v[0][0];
	
	
	
	int rwmsg=1;
	int self=1;
	double test1 = 0;
	
	int start_signal=0;
	for(int i=0;i<PhiNUM;i++)
        {
            start_signal = 0;
	    if (scif_recv(P[i]->dst0,&(start_signal),sizeof(int), SCIF_RECV_BLOCK) < 0){
                          printf("recieve votlage error on host!\n");
                       }
	    if(start_signal==888)
	        printf("timestep start !\n");
	    else
	        printf("wrong signal\n");
        }
	
	#endif //end of ifndef ONLYCPU
	
	printf("hello 2\n");
	int pos_x = partitioner->mpi_info.coord[0];
	int pos_y = partitioner->mpi_info.coord[1];
	int pos_z = partitioner->mpi_info.coord[2];
	double TT = bcl*beats*20;
        int pos_xe=(num_procs1-1)/2;
        int pos_ye=(num_procs2-1)/2;
        int pos_ze=(num_procs3-1)/2;
        printf("rank pos: <%d,%d,%d> range:<%d,%d,%d>\n",pos_x,pos_y,pos_z,pos_xe,pos_ye,pos_ze);
    
        if((pos_x<=pos_xe)&&(pos_y<=pos_ye)&&(pos_z<=pos_ze))
            TT = tstim+global_N_x/2+285;//350===>-70;330===>-20;285===>0(this works for 512);
        int flagg=0;
	printf("hello 3\n");
	
	//steps = 100;
	#ifdef OPENMPONCELL
	#pragma omp parallel 
	#endif
	{
        for (int icounter = 0; icounter<steps; icounter++)
        {
        
           //compute cell on cpu
           #ifdef OPENMPONCELL
           #pragma omp for
           #endif
           for(int I=pstart[0];I<pend[0];I++)
           {
		int ix = I/((N_y-2)*(N_z-2))+1;
	        int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
	        int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
                comp_cell(subcells[I],ix,iy,iz, offset_x, offset_z, t, ist, bcl, v, dtstep, Ndhpr,Ntotdyads,Nryr,T,B,ZT,icounter,steps,TT);
                dtstep = dt;
           }
           
        
           #ifndef ONLYCPU
           
           #ifdef OPENMPONCELL
           #pragma omp single
           #endif
           {
           for(int i=0;i<PhiNUM;i++)
           {
                //be careful with P[i]->dst0
                BARRIER(P[i]->dst0,"waiting MIC update v ");
                
                #ifdef REG
	        if ((err = scif_readfrom(P[i]->dst0,
				v_inout[i], /* local RAS offset */
				phisize*sizeof(double),
				v_inout[i], /* remote RAS offset */////////here is the same as local,but sometimes it is not the same. be careful 
				SCIF_RMA_USECACHE))<0) {//(use_cpu ? SCIF_RMA_USECPU : 0) | SCIF_RMA_SYNC)
		        printf("scif_readfrom failed with error : %d\n", errno);
	        }
	        #endif
	        SEND(P[i]->dst0,rwmsg,sizeof(int));//send mesg to device to tell it the read operation start
	        
	        #ifdef SCIFT	      
	        if (scif_recv(P[i]->dst0,&(v_msg_inoutPtr[i][0]),phisize*sizeof(double), SCIF_RECV_BLOCK) < 0){
                          printf("recieve votlage error on host!\n");
                       }
	        #endif
	        
           }
           
           for(int i=0;i<PhiNUM;i++)
                FENCE(P[i]->dst0, self);           
           //need put wait code here, wait read done
           for(int i=0;i<PhiNUM;i++)
           {
                UnPackVOnHost(v, N_x, N_y, N_z,v_msg_inoutPtr[i], pstart[i+1],pend[i+1]);
           }
           
           
	   
	   }//end of omp single
	   
	   #endif  //end of ifndef ONLYCPU
	   
	   #ifdef OPENMPONCELL	
           #pragma omp for
           #endif
	        for(int I=0;I<total;I++)
                {
		        	int ix = I/((N_y-2)*(N_z-2))+1;
			        int iy = (I%((N_y-2)*(N_z-2)))/(N_z-2)+1;
			        int iz = (I%((N_y-2)*(N_z-2)))%(N_z-2)+1;
                //for (int ix = 1; ix<N_x-1; ix++)
                //    for (int iy = 1; iy<N_y-1; iy++)
                //        for(int iz = 1; iz<N_z-1;iz++)
                            v[ix][iy][iz]+= dt*Dx*(vtemp[ix+1][iy][iz]+vtemp[ix-1][iy][iz]+vtemp[ix][iy-1][iz]+\
                                            vtemp[ix][iy+1][iz]+vtemp[ix][iy][iz-1]+vtemp[ix][iy][iz+1]-6*vtemp[ix][iy][iz])/(d_x*d_x);
                }
                
           #ifdef OPENMPONCELL
           #pragma omp single
           #endif
           {     
                //MPI comunication
        	UpdateGhostValue(v,partitioner); 
           
        	
          #ifndef ONLYCPU      
          
            	
        	for(int i=0;i<PhiNUM;i++)
            {
        	    PackVOnHost(v, N_x, N_y, N_z,v_msg_inoutPtr[i], pstart[i+1],pend[i+1]);
        	}
        	
        	
          	
        	for(int i=0;i<PhiNUM;i++)
        	{
        	    //be careful with P[i]->dst0
        	    #ifdef REG
        	    if ((err = scif_writeto(P[i]->dst0,
				v_inout[i], /* local RAS offset */
				phisize*sizeof(double),
				v_inout[i], /* remote RAS offset */
				SCIF_RMA_USECACHE))<0) {
			perror("scif_writeto failed with err");
		    }
		    #endif
		    SEND(P[i]->dst0,rwmsg,sizeof(int));//send mesg to device to tell it the write operation start
		    #ifdef SCIFT
		    //SEND(P[i]->dst0,v_msg_inoutPtr[i],sizeof(double)*phisize);
		    if (scif_send(P[i]->dst0, &(v_msg_inoutPtr[i][0]), phisize*sizeof(double), SCIF_SEND_BLOCK) < 0){
                           printf("send votalge error on host!\n");
                      }
		    #endif
		    
		    //BARRIER(P[i]->dst0,"Host update v done"); 
        	}
       	        for(int i=0;i<PhiNUM;i++)
                    FENCE(P[i]->dst0, self);///////This can be put after the copy from v to vtemp loop
          #endif //end of ifndef ONLYCPU
          } //end of omp single
          
          
           #ifdef OPENMPONCELL	
           #pragma omp for
           #endif	
        	for (int ix = 0; ix<N_x; ix++)
                    for (int iy = 0; iy<N_y; iy++)
                        for(int iz = 0;iz<N_z;iz++)
                        {                                                                     
                            vtemp[ix][iy][iz] = v[ix][iy][iz];
                        }
        
           
           
           #ifdef OPENMPONCELL
           #pragma omp single
           #endif
           {
        	
                
                if ((t>=tstim && t<(tstim+dt))||((t>=TT)&&(t<TT+dt)))
                    {
                        stimtime = 0;
                        ibeat = ibeat+1;
                        tstim = tstim+bcl;
                        //flagg = 0;
                        
                        if (partitioner->mpi_info.my_rank==0&&(t>=tstim && t<(tstim+dt)))
                            cout<<"ibeat is :"<<ibeat<<endl;
                    }


                if (stimtime>=0 && stimtime<=0.5)
                        ist = -80;
                else
                        ist = 0;

                //if (ibeat>beats-7) {ist = 0;}
                    
                //if (t>(beats-12)*bcl && t<=((beats-12)*bcl+6000))
                {          
                        if (icounter%StoreS == 0) //  && partitioner->is_master_proc())
                        {
                            for(int ix=s_ix;ix<N_x-1;ix+=storeStrideX)
                                for(int iy=s_iy;iy<N_y-1;iy+=storeStrideY)
                                {
                                    for(int iz=s_iz;iz<N_z-1;iz+=storeStrideZ)
                                        fout << v[ix][iy][iz] << " ";
                                    fout << endl;
                                }		
                        }
                          
                        if(icounter%ut==0)
                        {                            
                            if (partitioner->mpi_info.my_rank == 0)                          
                                f0out<<t<<"  "<< v[N_x/2][N_y/2][N_z/2]<<endl;
                            
                        }
                }
                t+=dt;
                stimtime += dt;
            }//end of omp single
                    
               
        }//end time loop
        
        }//end of omp parallel
        
        
            
	printf("out of time loop on host\n");

    	double end = MPI_Wtime()-start;
    	double mpitime;
    	MPI_Allreduce(&end,&mpitime,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
    	printf(" on %d total time is %lf \n  comp cell time %lf \n  diffusion time %lf",partitioner->mpi_info.my_rank,mpitime,comp_cell_time,diffusion_time);
    	
    	#ifdef PRINT
        printf("Number of random data used in cell 0=%lld\n",subcells[0].RandUsedN);
        printf("time info!\n");
        printf("|--comp_cell=%g\n",subcells[0].T_cell);
        printf("   |--comp_ina=%g  percentage=%g \n",subcells[0].T_ina,(subcells[0].T_ina)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_ito=%g  percentage=%g \n",subcells[0].T_ito,(subcells[0].T_ito)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_ikr=%g  percentage=%g \n",subcells[0].T_ikr,(subcells[0].T_ikr)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_iks=%g  percentage=%g \n",subcells[0].T_iks,(subcells[0].T_iks)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_ik1=%g  percentage=%g \n",subcells[0].T_ik1,(subcells[0].T_ik1)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_inak=%g  percentage=%g \n",subcells[0].T_inak,(subcells[0].T_inak)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_inab=%g  percentage=%g \n",subcells[0].T_inab,(subcells[0].T_inab)/(subcells[0].T_cell));fflush(stdout);
        printf("   |--comp_calc_dyad=%g  percentage=%g \n",subcells[0].T_calc_dyad,(subcells[0].T_calc_dyad)/(subcells[0].T_cell));fflush(stdout);
        
        printf("      |--comp_randomG=%g  percentage=%g \n",subcells[0].T_randomG,(subcells[0].T_randomG)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_prepare=%g  percentage=%g \n",subcells[0].T_prepare,(subcells[0].T_prepare)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_ical_dyad=%g  percentage=%g \n",subcells[0].T_ical_dyad,(subcells[0].T_ical_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_L_Channel=%g  percentage=%g \n",subcells[0].T_L_Channel,(subcells[0].T_L_Channel)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_ical_dyad1=%g  percentage=%g \n",subcells[0].T_ical_dyad1,(subcells[0].T_ical_dyad1)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_irel_dyad=%g  percentage=%g \n",subcells[0].T_irel_dyad,(subcells[0].T_irel_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_irel1=%g  percentage=%g \n",subcells[0].T_irel1,(subcells[0].T_irel1)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_irel2=%g  percentage=%g \n",subcells[0].T_irel2,(subcells[0].T_irel2)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_RyrOpen=%g  percentage=%g \n",subcells[0].T_RyrOpen,(subcells[0].T_RyrOpen)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_irel3=%g  percentage=%g \n",subcells[0].T_irel3,(subcells[0].T_irel3)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_conc_dyad=%g  percentage=%g \n",subcells[0].T_conc_dyad,(subcells[0].T_conc_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("         |--comp_ica_dyad=%g  percentage=%g \n",subcells[0].T_ica_dyad,(subcells[0].T_ica_dyad)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_conc_diff=%g  percentage=%g \n",subcells[0].T_conc_diff,(subcells[0].T_conc_diff)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("      |--comp_acculate=%g  percentage=%g \n",subcells[0].T_acculate,(subcells[0].T_acculate)/(subcells[0].T_calc_dyad));fflush(stdout);
        printf("   |--comp_ical=%g  percentage=%g \n",subcells[0].T_ical,(subcells[0].T_ical)/(subcells[0].T_cell));
        printf("   |--comp_rest=%g  percentage=%g \n",subcells[0].T_rest,(subcells[0].T_rest)/(subcells[0].T_cell));
        #endif
    	
    	
    	if (partitioner->mpi_info.my_rank == 0)
		VTKOutPut(num_procs1,num_procs2,num_procs3,\
			  global_N_x/storeStrideX,global_N_y/storeStrideY,global_N_z/storeStrideZ,steps/StoreS);
    	
    	
    	#ifndef ONLYCPU
    	for(int i=0;i<PhiNUM;i++)
	    PHIfinishSCIF(P[i],i);
	    
	for(int i=0;i<PhiNUM;i++)
	{
	    if ((err = scif_unregister(P[i]->dst0, v_inout[i], v_size)) < 0) {
		perror("scif_unregister failed with error");
	    }
	    
	}  
	    
	
	free(v_inout);
	
	
	for(int i=0;i<PhiNUM;i++)
	    free(P[i]);
	free(P);
	
	for(int i=0;i<PhiNUM;i++)
	{
	    
	    _mm_free(v_msg_inoutPtr[i]);
	    
	}
	free(v_msg_inoutPtr);
	#endif  //end of ifndef ONLYCPU
	
  	if (partitioner->mpi_info.my_rank == 0)
		f0out.close();

        deallocate_3D_double (v,N_x);
        deallocate_3D_double (vtemp,N_x);
        
        _mm_free(T[0]);
	_mm_free(T);
	_mm_free(B[0]);
	_mm_free(B);
	_mm_free(ZT);
        
        
        Partitioner_destruct(partitioner);
	MPI_Finalize ();
	printf("finished!\n");
	return 0;
}




