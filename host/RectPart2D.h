                                                                 // -*- C++ -*-

#ifndef RectPart2D_h__
#define RectPart2D_h__
#include <mpi.h>
#define NDIMS 3

typedef struct {
	MPI_Comm comm;
	MPI_Comm comm3d;
	int total_MPI_procs;
	int my_rank;
	
  	int dims[NDIMS];//the number of procs in each dimension
  	int coord[NDIMS];//the position of procs in 3D dimension space
	int neighbors[2*NDIMS];//the rank of the neighbor MPI proccess
	MPI_Request sendRequest[6];
	MPI_Request recieveRequest[6];
	MPI_Status  status[6];
} MPIInfo;

struct Partitioner
{    		
							  
	int my_rank_x, my_rank_y,my_rank_z;//the sanme as coord[]
	int ndims;

    int plane[2*NDIMS][2];//the size of each plane 
    int v_offsets[2*NDIMS];//the offset of the start point in each ghost plane
    int v_inner_offsets[2*NDIMS];
	int v_offsets1[2*NDIMS];
	int v_offsets2[2*NDIMS];


	int num_procs_x, num_procs_y,num_procs_z;//the same as dims[]
	int N_x, N_y,N_z;//the same as N[]
	int sub_Nx, sub_Ny, sub_Nz;//the same as sub_N[]
	int my_offset_x, my_offset_y, my_offset_z;//the same as my_offset[]

	//int num_procs[NDIMS];//the same with dims[]
	int N[NDIMS];//the total number of grids in each dimension
	int sub_N[NDIMS];//the number of grids in each dimension Each MPI process owned 
	int my_offset[NDIMS];//the global position of the start grid 


	double *ghost_value_send[6];
	double *ghost_value_recieve[6];
	int size_ghost_values[6];

	MPIInfo mpi_info;
};

typedef struct Partitioner Partitioner_t;

Partitioner_t* Partitioner_construct(MPI_Comm comm, int N1,int N2,int N3,
									int num_procs1, int num_procs2, int num_procs3);

void Partitioner_destruct(Partitioner_t *partitioner);

#endif
