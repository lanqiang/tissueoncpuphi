#include "UpdateGhost.h"
//#include "utils.h"


void UpdateGhostValue(double ***v,Partitioner_t *partitioner)
{
    int di;
    int ndims=3;
    //int sendi;
    //int sends;
    int i1,i2;
    int n1,n2;
    int buffer_offset;
    int v_offset,v_inner_offset,v_offset1,v_offset2;
    double *vPtr;
    vPtr = &(v[0][0][0]);
    for(di=0;di<2*ndims;di++)
    {
	if(partitioner->mpi_info.neighbors[di]!=MPI_PROC_NULL)
	{
             v_inner_offset = partitioner->v_inner_offsets[di];
	     v_offset1 = partitioner->v_offsets1[di];
	     v_offset2 = partitioner->v_offsets2[di];

             n1 = partitioner->plane[di][0];
   	     n2 = partitioner->plane[di][1];
	     for(i1=0;i1<n1;i1++)
             {
		buffer_offset = i1*n2;
     		for(i2=0;i2<n2;i2++)
		{
		      partitioner->ghost_value_send[di][buffer_offset+i2]=vPtr[v_inner_offset+i1*v_offset1+i2*v_offset2];
		}
  	     }
	}
	MPI_Isend(partitioner->ghost_value_send[di],
		  partitioner->size_ghost_values[di],
		  MPI_DOUBLE,partitioner->mpi_info.neighbors[di],
		  123456,
		  partitioner->mpi_info.comm,&(partitioner->mpi_info.sendRequest[di]));
	MPI_Irecv(partitioner->ghost_value_recieve[di],
		  partitioner->size_ghost_values[di],
		  MPI_DOUBLE,partitioner->mpi_info.neighbors[di],
		  123456,
		  partitioner->mpi_info.comm,&(partitioner->mpi_info.recieveRequest[di]));
    }

    //Update cells which have no neighbors
    for(di=0;di<2*ndims;di++)
    {
        if(partitioner->mpi_info.neighbors[di]==MPI_PROC_NULL)
        {
            v_offset = partitioner->v_offsets[di];
            v_inner_offset = partitioner->v_inner_offsets[di];
            v_offset1 = partitioner->v_offsets1[di];
            v_offset2 = partitioner->v_offsets2[di];
            n1 = partitioner->plane[di][0];
            n2 = partitioner->plane[di][1];
            for(i1=0;i1<n1;i1++)
                for(i2=0;i2<n2;i2++)
		    vPtr[v_offset+i1*v_offset1+i2*v_offset2] = vPtr[v_inner_offset+i1*v_offset1+i2*v_offset2];
        }
    }
 
    MPI_Waitall(2*ndims,partitioner->mpi_info.sendRequest,MPI_STATUS_IGNORE);
    MPI_Waitall(2*ndims,partitioner->mpi_info.recieveRequest,MPI_STATUS_IGNORE);
    //deal with recieved data 
    for(di=0;di<2*ndims;di++)
    {
	if(partitioner->mpi_info.neighbors[di]!=MPI_PROC_NULL)
	{
	     v_offset = partitioner->v_offsets[di];
	     v_offset1 = partitioner->v_offsets1[di];
	     v_offset2 = partitioner->v_offsets2[di];
	     n1 = partitioner->plane[di][0];
	     n2 = partitioner->plane[di][1];
	     for(i1=0;i1<n1;i1++)
	     {
		buffer_offset = i1*n2;
		for(i2=0;i2<n2;i2++)
		{
		    vPtr[v_offset+i1*v_offset1+i2*v_offset2] = partitioner->ghost_value_recieve[di][buffer_offset+i2];
		}
	     }

	}
    }
}
