#include "allocArray.h"

#include <stdlib.h>
#define ALIGN 0x1000

double ** allocate_2D_double ( int Nx, int Ny)
{
  double **ptr;
  int size = Nx*Ny*sizeof(double);
  if((size%ALIGN)!=0)
       size = (size/ALIGN+1)*ALIGN;
  double *base_ptr = (double*)_mm_malloc(size,ALIGN);
  ptr = (double**)malloc(Nx*sizeof(double*));
  for (int i=0; i<Nx; i++)
    ptr[i] = &(base_ptr[i*Ny]);
  return ptr;
}


double *** allocate_3D_double(int Nx, int Ny,int Nz)
{
  double ***ptr;
  int size = Nx*Ny*Nz*sizeof(double);
  if((size%ALIGN)!=0)
       size = (size/ALIGN+1)*ALIGN;
  double *base_ptr = (double*)_mm_malloc(size,ALIGN);
  ptr = (double***)malloc(Nx*sizeof(double**));
  for (int ix=0; ix<Nx; ix++)
  {
	ptr[ix] = (double**)malloc(Ny*sizeof(double*));
	for (int iy = 0; iy<Ny; iy++)
       {
		ptr[ix][iy] = &(base_ptr[ix*Ny*Nz + iy*Nz]);
       }
   }
   return ptr;
}

int *** allocate_3D_int( int Nx, int Ny, int Nz)
{
	int ***ptr;
	int size = Nx*Ny*Nz*sizeof(int);
        if((size%ALIGN)!=0)
               size = (size/ALIGN+1)*ALIGN;
	int *base_ptr = (int *)_mm_malloc(size,ALIGN);
	ptr =(int ***)malloc(Nx*sizeof(int**));
	for(int ix=0; ix<Nx;ix++)
		{
			ptr[ix] = (int **)malloc(Ny*sizeof(int *));
			for( int iy = 0; iy<Ny; iy++)
				{
					ptr[ix][iy] = &(base_ptr[ix*Ny*Nz + iy*Nz]);
				//	(*ptr)[ix][iy] = (int *)malloc(Ndyads*sizeof(int));
				}
		}
       return ptr;
}


double **** allocate_4D_double( int Nx, int Ny, int Nz, int Nt)
{
	double ****ptr;
	int size = Nx*Ny*Nz*Nt*sizeof(double);
        if((size%ALIGN)!=0)
               size = (size/ALIGN+1)*ALIGN;
	double *base_ptr = (double*)_mm_malloc(size,ALIGN);
	ptr =  (double****)malloc(Nx*sizeof(double***));

	for (int ix = 0; ix<Nx; ix++)
	{
		ptr[ix] = (double***)malloc(Ny*sizeof(double**));
		for (int iy = 0; iy<Ny; iy++)
		{
			ptr[ix][iy] = (double**)malloc(Nz*sizeof(double*));
			for (int idyads = 0; idyads<Nz; idyads++)
			{
				ptr[ix][iy][idyads] = &(base_ptr[ix*Ny*Nz*Nt + iy*Nz*Nt + idyads*Nt]);
			//	(*ptr)[ix][iy][idyads] = (double *)malloc(Nryr*sizeof(double));
			}
		}
	}
	return ptr;
}

int **** allocate_4D_int( int Nx, int Ny, int Nz, int Nt)
{
	int ****ptr;
	int size = Nx*Ny*Nz*Nt*sizeof(int);
        if((size%ALIGN)!=0)
               size = (size/ALIGN+1)*ALIGN;
	int *base_ptr = (int*)_mm_malloc(size,ALIGN);
	ptr =  (int****)malloc(Nx*sizeof(int***));

	for (int ix = 0; ix<Nx; ix++)
	{
		ptr[ix] = (int***)malloc(Ny*sizeof(int**));
		for (int iy = 0; iy<Ny; iy++)
		{
			ptr[ix][iy] = (int**)malloc(Nz*sizeof(int*));
			for (int idyads = 0; idyads<Nz; idyads++)
			{
				ptr[ix][iy][idyads] = &(base_ptr[ix*Ny*Nz*Nt + iy*Nz*Nt + idyads*Nt]);
			//	(*ptr)[ix][iy][idyads] = (int *)malloc(Nryr*sizeof(int));
			}
		}
	}
	return ptr;
}

void deallocate_2D_double (double **ptr)
{
	_mm_free(ptr[0]);
	free(ptr);
}


void deallocate_3D_double(double ***ptr,int Nx)
{
	_mm_free(ptr[0][0]);
	for(int i=0;i<Nx;i++)
	    free(ptr[i]);
	free(ptr);
}

void deallocate_3D_int(int ***ptr,int Nx)
{
	_mm_free(ptr[0][0]);
	for(int i=0;i<Nx;i++)
	    free(ptr[i]);
	free(ptr);
}


void deallocate_4D_int(int ****ptr,int Nx,int Ny)
{
	_mm_free(ptr[0][0][0]);
	for(int i=0;i<Nx;i++)
	{
	     for(int j=0;j<Ny;j++)
		free(ptr[i][j]);
	     free(ptr[i]);
	}
	free(ptr);
}
