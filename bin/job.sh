#!/bin/bash

#SBATCH --job-name=3d-tissue
#SBATCH --time=01:10:00
#SBATCH --account=nn2849k
#SBATCH --mem-per-cpu=2G
#SBATCH --output=./Output_File.out
#SBATCH --partition=accel --gres=phi:1



#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive
#SBATCH --cpus-per-task=1

#module load vtune_amplifier/2015.0

source /cluster/bin/jobsetup
#cp $SUBMITDIR/cell $SCRATCH

module load gcc/4.8.2
module load intel/2015.1
module load intelmpi.intel

source ~/envset
#export MIC_ENV_PREFIX=MIC
#export MIC_OMP_NUM_THREADS=4
#export PHI_KMP_AFFINITY=scatter
#export PHI_KMP_PLACE_THREADS=59c,2t


#export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#time mpirun -np 4  amplxe-cl -c hotspots -r amp_result -source-search-dir=/usit/abel/u1/qianglan/HPC-APP/tissue-stable/code -- ./tissue 2 2 
time mpirun -np 1 ./tissue  1 1 1 1 1 1 10000 100 10 10 1 1 1
