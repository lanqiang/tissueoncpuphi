#include "printInfo.h"
#include <fstream>

#include "cell.h"
void printInfo(double t,cell& Cell, double v,ofstream &f1,ofstream &f2,ofstream &f3,ofstream &f4,ofstream &f5,ofstream &f6)
{
    int Ndyads = Cell.Ndyads;
    if(f1.is_open())
        //f1<< t <<" "<< v <<" "<< Cell.unitsInCell[Ndyads/2].cads<<" "<<Cell.cassdiff[Ndyads/2]<<" "<<Cell.cacytdiff[Ndyads/2]<< " "<<Cell.cansrdiff[Ndyads/2]<< " " << Cell.unitsInCell[Ndyads/2].cajsr<<endl;
        f1<< t <<"  "<< v <<"  "<< Cell.cadyad<<"  "<<Cell.casub<<"  "<<Cell.cai<< "  "<<Cell.nsr<< "  " << Cell.jsr<<endl;
    if(f2.is_open())
        //f2out<<t<<" "<<cads[Ndyads/2]<<"    "<<cass[Ndyads/2]<<"    "<<cacyt[Ndyads/2]<<"   "<<cansr[Ndyads/2]<<"   "<<cajsr[Ndyads/2]<<endl;
//        f2<< t <<"  "<< Cell.unitsInCell[Ndyads/2].cads<<"  "<<Cell.unitsInCell[Ndyads/2].cass<<"  "<<Cell.unitsInCell[Ndyads/2].cacyt<< "  "<<Cell.unitsInCell[Ndyads/2].cansr<< "  " << Cell.unitsInCell[Ndyads/2].cajsr<<endl;
        f3<< t <<"  "<<Cell.ina<<"   "<<Cell.ICaL_print<<"    "<<Cell.Ito<<"   "<<Cell.IKs<<"   "<<Cell.IKr<<"   "<<Cell.IK1<<"   "<<Cell.INaK<<"  "<<Cell.inaca+Cell.inacass<<"  "<<Cell.jrel<<endl;
    
//    for(int i=0;i<Ndyads;i++)
//    {
//        f4<<Cell.unitsInCell[i].cacyt<<" ";
//        f5<<Cell.unitsInCell[i].cajsr<<" ";
//    }
//    f6<< t << " " << Cell.nryrc1 << " " << Cell.nryrc2 << " " << Cell.nryro1<< " "<<Cell.nryro2 <<" " <<Cell.kC1O1<<" " <<Cell.kC1C2<<" " <<Cell.kO1C1<<" " <<Cell.kO1O2<<" " <<Cell.kC2C1<<" " <<Cell.kC2O2<<" " <<Cell.kO2C2<<" " <<Cell.kO2O1<< endl; 
}

void printFinalStateInfo(cell &Cell)
{
    printf("Cell.nai = %lf\n",Cell.nai);
    printf("Cell.nass = %lf\n",Cell.nass);
    printf("Cell.ki = %lf\n",Cell.ki);
    printf("Cell.kss = %lf\n",Cell.kss);
    printf("Cell.m = %lf\n",Cell.m);
    printf("Cell.hf = %lf\n",Cell.hf);
    printf("Cell.hs = %lf\n",Cell.hs);
    printf("Cell.j = %lf\n",Cell.j);
    printf("Cell.hsp = %lf\n",Cell.hsp);
    printf("Cell.jp = %lf\n",Cell.jp);
    printf("Cell.ml = %lf\n",Cell.ml);
    printf("Cell.hl = %lf\n",Cell.hl);
    printf("Cell.hlp = %lf\n",Cell.hlp);
    printf("Cell.a = %lf\n",Cell.a);
    printf("Cell.iF = %lf\n",Cell.iF);
    printf("Cell.iS = %lf\n",Cell.iS);
    printf("Cell.ap = %lf\n",Cell.ap);
    printf("Cell.iFp = %lf\n",Cell.iFp);
    printf("Cell.iSp = %lf\n",Cell.iSp);
    printf("Cell.d = %lf\n",Cell.d);
    printf("Cell.ff = %lf\n",Cell.ff);
    printf("Cell.fs = %lf\n",Cell.fs);
    printf("Cell.fcaf = %lf\n",Cell.fcaf);
    printf("Cell.fcas = %lf\n",Cell.fcas);
    printf("Cell.jca = %lf\n",Cell.jca);
    printf("Cell.ffp = %lf\n",Cell.ffp);
    printf("Cell.fcafp = %lf\n",Cell.fcafp);
    printf("Cell.xrf = %lf\n",Cell.xrf);
    printf("Cell.xrs = %lf\n",Cell.xrs);
    printf("Cell.xs1 = %lf\n",Cell.xs1);
    printf("Cell.xs2 = %lf\n",Cell.xs2);
    printf("Cell.xk1 = %lf\n",Cell.xk1);
    printf("Cell.nca_print = %lf\n",Cell.nca_print);
}
