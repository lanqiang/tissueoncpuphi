#include "cell.h"
#include "unit.h"
#include <omp.h>
#include <cstring>
#include <stdlib.h>




struct cell ***Allocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z)
{
    struct cell ***SubCells;
    struct cell *cells = (struct cell*)malloc(sub_N_x*sub_N_y*sub_N_z*sizeof(struct cell));
    SubCells = (struct cell***)malloc(sub_N_x*sizeof(struct cell**));
    for(int ix=0; ix<sub_N_x; ix++)
    {
        SubCells[ix] = (struct cell**)malloc(sub_N_y*sizeof(struct cell*));
        for(int iy=0;iy<sub_N_y;iy++)
	     SubCells[ix][iy] = &(cells[ix*sub_N_y*sub_N_z+iy*sub_N_z]);
    }
    return SubCells;

}

struct cell * allocateSubCells(int c_size)
{
    struct cell *SubCells;
    SubCells = (struct cell*)malloc(c_size*sizeof(struct cell));
    return SubCells;
}


void deallocate3DSubCells(struct cell ***SubCells,int Nx,int Ny)
{
    free(SubCells[0][0]);
    for(int i=0;i<Nx;i++)
    	free(SubCells[i]);
    free(SubCells);
    return;
}


void InitCellValue(struct cell & Cell,int unitNum_x,int unitNum_y,int unitNum_z,int nryr,int ndhpr,int beats)
{
    
    Cell.nai=5.812871;//?
    Cell.nass = 5.7;
    Cell.ki=141.347081;
    Cell.kss = 143.000;

    Cell.m=0.012217;
    Cell.hf=0.501200;
    Cell.hs=0.500771;
    Cell.j=0.495238;
    Cell.hsp=0.265035;
    Cell.jp=0.483247;

    Cell.ml=0.000191;
    Cell.hl=0.314456;
    Cell.hlp=0.142849;

    Cell.a=0.001006;
    Cell.iF=0.999547;
    Cell.iS=.209676;
    Cell.ap=.000513;
    Cell.iFp=0.999547;
    Cell.iSp=.232485;

    Cell.d=0.00;
    Cell.ff=1.00;
    Cell.fs=.694365;
    Cell.fcaf=1.00;
    Cell.fcas=0.937321;
    Cell.jca=0.959827;
    Cell.ffp=0.999985;
    Cell.fcafp=0.999998;

    Cell.xrf=0.015655;
    Cell.xrs=0.812052;
    Cell.xs1=0.518277;
    Cell.xs2=0.000615;
    Cell.xk1=0.996787;

    Cell.nca_print = 0.998965;//added by qianglan
   
    Cell.T_cell=0;
    Cell.T_ina=0;
    Cell.T_ito=0;
    Cell.T_ikr=0;
    Cell.T_iks=0;
    Cell.T_ik1=0;
    Cell.T_inak=0;
    Cell.T_inab=0;
    Cell.T_calc_dyad=0;
    Cell.T_ical=0;
    Cell.T_rest=0;

    Cell.T_randomG=0;
    Cell.T_prepare=0;
    Cell.T_ical_dyad=0;
        Cell.T_L_Channel=0;
        Cell.T_ical_dyad1=0;
    Cell.T_irel_dyad=0;
        Cell.T_irel1=0;
        Cell.T_irel2=0;
        Cell.T_RyrOpen=0;
        Cell.T_irel3=0;
    Cell.T_conc_dyad=0;
        Cell.T_ica_dyad=0;
    Cell.T_conc_diff=0;
    Cell.T_acculate=0;
    
    
    Cell.unitNum_x = unitNum_x;
    Cell.unitNum_y = unitNum_y;
    Cell.unitNum_z = unitNum_z;

    Cell.Ndhpr = ndhpr;
    Cell.Nryr = nryr;

    int ndyads = unitNum_x*unitNum_y*unitNum_z;
    Cell.Ndyads = ndyads;

    Cell.beats = beats;

    Cell.nca = (double *)_mm_malloc(ndyads*sizeof(double),64); //memset(Cell.nca,0,ndyads*sizeof(double));
    Cell.CaMKt = (double *)_mm_malloc(ndyads*sizeof(double),64);//memset(Cell.CaMKt,0.1,ndyads*sizeof(double));
    Cell.CaMKa = (double *)_mm_malloc(ndyads*sizeof(double),64);//memset(Cell.CaMKa,0.1,ndyads*sizeof(double));
    Cell.CaMKb = (double *)_mm_malloc(ndyads*sizeof(double),64);//memset(Cell.CaMKb,0.1,ndyads*sizeof(double));
   
    Cell.trel = (double *)_mm_malloc(ndyads*sizeof(double),64);memset(Cell.trel,-2000,ndyads*sizeof(double));
    Cell.Poryr = (double *)_mm_malloc(ndyads*sizeof(double),64);//memset(Cell.Poryr,0,ndyads*sizeof(double));
    Cell.nactive = (int *)_mm_malloc(ndyads*sizeof(int),64);//memset(Cell.nactive,0,ndyads*sizeof(int));

    //malloc unit ptr within cell
    Cell.unitsInCell = (unit *)_mm_malloc(ndyads*sizeof(unit),64);
    
    
    Cell.dyadInnerVPtr = (DyadInnerVars *)_mm_malloc(sizeof(DyadInnerVars),64);
    Cell.dyadOuterVPtr = (DyadOuterVars *)_mm_malloc(sizeof(DyadOuterVars),64);
    

    Cell.cacyt = (double *)_mm_malloc(ndyads*sizeof(double),64);
    memset(Cell.cacyt,0,ndyads*sizeof(double));
    Cell.cass = (double *)_mm_malloc(ndyads*sizeof(double),64);
    memset(Cell.cass,0,ndyads*sizeof(double));
    Cell.cansr = (double *)_mm_malloc(ndyads*sizeof(double),64);
    memset(Cell.cansr,0,ndyads*sizeof(double));
    Cell.cajsr = (double *)_mm_malloc(ndyads*sizeof(double),64);
    memset(Cell.cajsr,0,ndyads*sizeof(double));
    Cell.cads = (double *)_mm_malloc(ndyads*sizeof(double),64);
    memset(Cell.cads,0,ndyads*sizeof(double));


    Cell.NryrO1 = (double *)_mm_malloc(ndyads*sizeof(double),64);
    Cell.NryrO2 = (double *)_mm_malloc(ndyads*sizeof(double),64);
    Cell.NryrC1 = (double *)_mm_malloc(ndyads*sizeof(double),64);
    Cell.NryrC2 = (double *)_mm_malloc(ndyads*sizeof(double),64);

    Cell.casstemp = (double *)_mm_malloc(ndyads*sizeof(double),64);
    Cell.cansrtemp = (double *)_mm_malloc(ndyads*sizeof(double),64);
    Cell.cacyttemp = (double *)_mm_malloc(ndyads*sizeof(double),64);

    #ifdef OPENMPONCELL
    Cell.RandUsedN = 0;
    #endif
    
    int LtypeRandN = 2;
    int RyrRandN = 8;

    Cell.LtypeRandN = LtypeRandN;
    Cell.RyrRandN = RyrRandN;
    
    int randU = LtypeRandN + RyrRandN;
    Cell.randU = randU;
    Cell.rand_n = ndyads*randU;//binomial method
    
    #ifdef OPENMPONCELL
    Cell.RandUsedN = Cell.rand_n;
    #endif
    
    Cell.l_Random = (double *)_mm_malloc(Cell.rand_n*sizeof(double),64); 
    


    //std::random_device rd;
    //Cell.gen(Cell.rd());


    //init the value of 1d mem

    for(int i=0; i<ndyads;i++)
    {
        Cell.CaMKt[i] = 0.009;
        Cell.nca[i] = 0.001;
        Cell.CaMKa[i] = 0.1;
        Cell.CaMKb[i] = 0.1;
        //Cell.trel[i] = -2000;
        Cell.Poryr[i] = 0;
        Cell.nactive[i] = 0;

        Cell.cacyt[i] = 6.73e-5;//diff
        Cell.cass[i] = 6.65e-5;//diff
        Cell.cansr[i] = 2.5;//diff
        Cell.cajsr[i] = 2.5;
        Cell.cads[i] = (6.65e-5)*1e3;
        Cell.l_Random[i] = 0;

        Cell.unitsInCell[i].nryr = nryr;
        initUnitValue(Cell.unitsInCell[i]);

        Cell.NryrO1[i] = 0;
        Cell.NryrO2[i] = 0;
        Cell.NryrC1[i] = 100;
        Cell.NryrC2[i] = 0;

    }


    //malloc random generator
    Cell.pRD = (struct RandData *)malloc(sizeof(struct RandData));
    //Cell.randcallcount  = 0;
    Cell.timestep = 0;
    
    int  Ndyads = ndyads;
    Cell.ab5 = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.cd5 = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.nLCC_dyad_np = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.nLCC_dyad_p  = (double *)_mm_malloc(sizeof(double)*Ndyads,64);//memset(nLCC_dyad_p,0.0,sizeof(double)*Ndyads);
    Cell.jlca_dyad = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.tref = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.kC1O1 = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.kC1C2 = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.kC2C1 = (double *)_mm_malloc(sizeof(double)*Ndyads,64);
    Cell.kC2O2 = (double *)_mm_malloc(sizeof(double)*Ndyads,64);

    return;
}


void releaseCell(struct cell &Cell)
{
    int ndyads = Cell.unitNum_x;
    for(int i=0; i<ndyads;i++)
        releaseUnit(Cell.unitsInCell[i]);
    _mm_free(Cell.unitsInCell);

    free(Cell.pRD);
    
    _mm_free(Cell.dyadInnerVPtr);
    _mm_free(Cell.dyadOuterVPtr);

    //vslDeleteStream(&Cell.Randomstream);
    _mm_free(Cell.nca);
    _mm_free(Cell.CaMKt);
    _mm_free(Cell.CaMKa);
    _mm_free(Cell.CaMKb);
    _mm_free(Cell.trel);
    _mm_free(Cell.Poryr);
    _mm_free(Cell.nactive);


    _mm_free(Cell.NryrO1);
    _mm_free(Cell.NryrC1);
    _mm_free(Cell.NryrO2);
    _mm_free(Cell.NryrC2);


    _mm_free(Cell.cacyt);
    _mm_free(Cell.cass);
    _mm_free(Cell.cansr);
    _mm_free(Cell.cajsr);
    _mm_free(Cell.cads);

     _mm_free(Cell.casstemp);
    _mm_free(Cell.cansrtemp);
    _mm_free(Cell.cacyttemp);
    
    
    
     _mm_free(Cell.ab5);
    _mm_free(Cell.cd5);
    _mm_free(Cell.nLCC_dyad_np);
    _mm_free(Cell.nLCC_dyad_p);
    _mm_free(Cell.jlca_dyad);
    _mm_free(Cell.tref);
    _mm_free(Cell.kC1O1);
    _mm_free(Cell.kC1C2);
    _mm_free(Cell.kC2C1);
    _mm_free(Cell.kC2O2);
    

    if(Cell.l_Random!=NULL);
        _mm_free(Cell.l_Random);

   
    return;
}

void compute_cell_id(struct cell & Cell, int cx, int cy,int cz, int total_N_x,int total_N_y,int total_N_z,int my_offset_x,int my_offset_y,int my_offset_z)
{

//    int cell_id = (my_offset_y + cy - 1)*total_N_x + my_offset_x + cx -1;
    int cell_id = (my_offset_x+cx-1)*total_N_z*total_N_y+(my_offset_z+cz-1)*total_N_z+my_offset_y+cy-1;
    Cell.cellID = cell_id+1;//id from 1
    //Cell.cellID = 1;

    Cell.pRD->myrandomseed = -Cell.cellID;
    Cell.pRD->iy = 0;
    Cell.pRD->randcallcount = 0;

    Cell.totalCellSize = total_N_x*total_N_y*total_N_z;

    //vslNewStream(&Cell.Randomstream,VSL_BRNG_MCG31,Cell.cellID);

    

    #ifdef OPENMPONDYAD
       #ifdef parallerandom
        #pragma omp parallel
        {
            int tid = omp_get_thread_num();
            vslNewStream(&Cell.RandomMultistream[tid],VSL_BRNG_MCG31,(Cell.cellID*236)+tid);
        }
       #else
           vslNewStream(&Cell.RandomMultistream[0],VSL_BRNG_MCG31,Cell.cellID);
       #endif
    #else
        vslNewStream(&Cell.Randomstream,VSL_BRNG_MCG31,Cell.cellID);
    #endif

    return;

}

