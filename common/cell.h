#ifndef CELL_H_INCLUDED
#define CELL_H_INCLUDED

#include "randomGenerate.h"

#include "unit.h"
#include "mkl_vsl.h"
//#include <random>
#include "common.h"


struct DyadInnerVars
{
     double k2nP;
     double expkm2n;
     double if2;
     double if3;
     double if4;
     double if5;
     double PhiCaLP2;
     double caoM;
     double PhiCaLP1;
     double PhiCaNa;
     double PhiCaK;  
     double jlca_dyadP; 
     double kmcsqn8; 
     double Kmcsqn4;
     double jrelP;
     double vsjrM;
     double h6;
     double x1P;
     double k2;
     double k4;
     
     double x2P;
     double x3P;
     double x2P2;
     double x3P2;
     double k1;
     double k3;
     double EP1;
     double EP2;
     double X4;
     double k7;
     double k8;
     double k4pp;
     double k3pp;
     double cassP1;
     double cassP2;
     double cacytP;   
     double Km4; 
     double PCabM2; 
     double PCabM1; 
};

struct DyadOuterVars
{
     int Ndyads;
     int randU;
     int Ndhpr;
     double PCa;
     double PCaNa;
     double PCaNap;
     double PCaK;
     double PCaKp;
     double ilca;
     double ilcana;
     double ilcak; 
     int Nryr;
     double t;
     int beats;
     int bcl;
     
     int Cell_LtypeRandN;
   
     double dt;
     double vjsr_dyad;
     double vnsr_dyad;
     double kmtrpn;
     double vcyt_dyad;
     double jrel;
     double inaca;
     double inacass;
     double ipca;
     double icab;
     double voltage;
     double vmyo;
     double vss_dyad;
     double Ntotdyads;
     double acap;
     double vfrt;
     double vffrt;
     double PCap;
     int NumThreads;
};


struct cell
{
    int cellID;
    struct RandData *pRD;
    struct unit *unitsInCell;
    
    struct DyadInnerVars *dyadInnerVPtr;
    struct DyadOuterVars *dyadOuterVPtr;

    int unitNum_x;
    int unitNum_y;
    int unitNum_z;
	
    int Ndhpr;
    int Ndyads;
    int Nryr;
    unsigned long rand_n;
    int randU;

    int LtypeRandN;
    int RyrRandN;

    #ifdef OPENMPONDYAD
    unsigned int  RandUsedN[236];
    #else
    unsigned int  RandUsedN;
    #endif

    int beats;

   //added new variebles
    double f,fca,fp,fcap;

    double m,hf,hs,hsp,j,jp;
    double ml,hl,hlp;
    double nai,ki,nass,kss;

    double ap,a,iF,iS,iFp,iSp;
    double xs1,xs2,xrs,xrf,xk1;
    double d,ff,fcaf,fs,fcas,jca,ffp,fcafp;

    double CaMKa_cell;//added by qianglan
    double nca_print;//added by qianglan
    //double inacass_dyad;//added by qianglan

    int nryrc1;
    int nryro1;
    int nryrc2;
    int nryro2;

    //double kC1O1,kC1C2,kO1C1,kO1O2,kC2C1,kC2O2,kO2C2,kO2O1;

    double *nca;//related with cads[]
    double *CaMKt;
    double *CaMKa;//added by qianglan
    double *CaMKb;//added by qianglan
    double *trel;
    double *Poryr;

    int *nactive;
    //int randcallcount;

    long timestep;
    long totalCellSize;



    #ifdef OPENMPONDYAD
        VSLStreamStatePtr RandomMultistream[236];
    #else
        VSLStreamStatePtr Randomstream;
    #endif
   

    //out put info
    double cadyad;
    double casub;
    double cai;
    double nsr;
    double jsr;

    double ina;
    double ICaL_print;
    double Ito;
    double IKs;
    double IKr;
    double IK1;
    double INaK;
    double inaca;
    double inacass;
    double jrel;

    double *cacyt;//show this
    double *cass;
    double *cansr;
    double *cajsr;//show this
    double *cads;

    double *casstemp;
    double *cansrtemp;
    double *cacyttemp;

    double *NryrO1;
    double *NryrO2;
    double *NryrC1;
    double *NryrC2;
  
    double *l_Random;

    //time info
    double T_cell;
    double T_ina;
    double T_ito;
    double T_ikr;
    double T_iks;
    double T_ik1;
    double T_inak;
    double T_inab;
    double T_calc_dyad;
    double T_ical;
    double T_rest;    
    
    //time info:inside T_calc_dyad
    double T_randomG;
    double T_prepare;
    double T_ical_dyad;
        double T_L_Channel;
        double T_ical_dyad1;
    double T_irel_dyad;
        double T_irel1;
        double T_irel2;
        double T_RyrOpen;
        double T_irel3;
    double T_conc_dyad;
        double T_ica_dyad;
    double T_conc_diff;
    double T_acculate;
    
    
    double *ab5;
    double *cd5;
    double *nLCC_dyad_np;
    double *nLCC_dyad_p;//memset(nLCC_dyad_p,0.0,sizeof(double)*Ndyads);
    double *jlca_dyad;
    double *tref;
    double *kC1O1;
    double *kC1C2;
    double *kC2C1;
    double *kC2O2;
        
};




struct cell ***Allocate3DSubCells(int sub_N_x,int sub_N_y,int sub_N_z);

struct cell * allocateSubCells(int c_size);

void deallocate3DSubCells(struct cell ***SubCells,int Nx);

void InitCellValue(struct cell & Cell,int unitNum_x,int unitNum_y,int unitNum_z, int nryr, int ndhpr,int beats);
void releaseCell(struct cell &Cell);

//void generateSeed(struct cell&Cell, int thread_id,int process_id);
void compute_cell_id(struct cell & Cell, int cx, int cy, int cz, int total_N_x,int total_N_y,int total_N_z,int my_offset_x,int my_offset_y,int my_offset_z);



#endif // CELL_H_INCLUDED
