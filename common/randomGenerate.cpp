#include "randomGenerate.h"

double RandData::randomDataGenerate()
{
    int j;
    long k;
    double temp;
    randcallcount++;
    if (myrandomseed <= 0 || ! iy)
	{
        if (-myrandomseed < 1)
            myrandomseed=1;
        else
            myrandomseed = -myrandomseed;
        for (j=NTAB+7;j>=0;j--)
		{
            k=myrandomseed/IQ;
            myrandomseed=IA*(myrandomseed-k*IQ)-IR*k;
            if (myrandomseed < 0)
                myrandomseed += IM;
            if (j < NTAB)
                iv[j] = myrandomseed;
        }
        iy=iv[0];
    }

    k=myrandomseed/IQ;
    myrandomseed=IA*(myrandomseed-k*IQ)-IR*k;
    if (myrandomseed < 0)
        myrandomseed += IM;
    j=iy/NDIV;
    iy=iv[j];
    iv[j] = myrandomseed;
    if ((temp=AM*iy) > RNMX)
        return RNMX;
    else
        return temp;
}
