#ifndef COMPUTE_CELL_H_INCLUDED
#define COMPUTE_CELL_H_INCLUDED

#include "cell.h"
//#include <random>

#ifdef PHI
void comp_cell(struct cell &Cell, int I,int ix, int iy, int iz,int offset_x, int offset_y, double t, double ist, int bcl, double *v,double dtstep,int Ndhpr , int Ntotdyads, int nryr,double **T,double **B,double *ZT,int icounter, int steps);
#endif

#ifdef CPU
void comp_cell(struct cell &Cell,int ix, int iy, int iz,int offset_x, int offset_y, double t, double ist, int bcl, double ***v,double dtstep,int Ndhpr , int Ntotdyads, int nryr,double **T,double **B,double *ZT,int icounter, int steps,double);
#endif


#endif // COMPUTE_CELL_H_INCLUDED
