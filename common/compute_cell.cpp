#include "compute_cell.h"
#include "cell.h"

#include "mkl_vsl.h"
#include "mkl_lapacke.h"
#include <cmath>
#include <cstring>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
//#include <random>

#include <assert.h>

#include <omp.h>

#include "offload.h"


#include "immintrin.h"

#include <sys/time.h>

#include "constant.h"
//#include "common.h" //





#ifdef OPENMPONDYAD
double  noryr=0.0,jrel=0.0,ilca=0.0,ilcana=0.0,ilcak=0.0,inaca=0.0,icab=0.0,ipca=0.0,inacass=0.0;
#endif

int ic;

//using namespace std;

 __attribute__((always_inline)) inline double timing(){
        double time;
        struct timeval timmer;

        gettimeofday(&timmer,NULL);
        time = timmer.tv_sec + timmer.tv_usec*1e-6;        
        return time;
}


__attribute__((always_inline)) inline  double constp_binomial(double remainder,double *BR)
{
    int i = 0;
    while(remainder>BR[i])
    {
        i++;
    }    
    return (double)i;
}


__attribute__((always_inline)) inline  __m512d constp_binomialVec(__m512d N,__m512d REMAINDER,double *BR,__m512i I,__m512i CONST,__m512d CONST3)
{
    __mmask8 MASK = 255;
    __m512i index;
    __m512d indexD;
    index = _mm512_set1_epi32(0);
    __m512d INDEX = _mm512_mul_pd(N,CONST3);    
    index = _mm512_cvtfxpnt_roundpd_epi32lo(INDEX,_MM_FROUND_TO_NEAREST_INT |_MM_FROUND_NO_EXC);    
    int scale = 8;
    __m512d BV;
   
    do
    {
        BV = _mm512_i32logather_pd(index,(void *)BR,scale);
         MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,BV,_MM_CMPINT_GT);
         index = _mm512_mask_add_epi32(index,MASK,index,CONST); 
          I = _mm512_mask_add_epi32(I,MASK,I,CONST);
    }while(MASK);
    
    indexD =  _mm512_cvtepu32lo_pd(I);
    return indexD;
}


__attribute__((always_inline)) inline double binomial(double n,double p,double remainder,double *TR,double *ZT)
{

    //if((n==0)||((p<0.00000001)&&remainder<ZT[(int)n]))
    #ifdef APPROXIMATE
    if((n==0)||(p<0.00000001))
    #else
    if(n==0)
    #endif
	return 0;
    #ifdef CMPFB
    int i = -1;
    double p_knk=pow((1-p),(int)n);
    double p1p=p/(1-p);
    while(remainder>=0.0)  // &&i<n
    {
        i++;
        remainder -=TR[i]*p_knk;
        p_knk = p_knk*p1p;
    }
    #else
    
    int i = 0;
    double p_knk=pow((1-p),(int)n);
    double p1p=p/(1-p);
    double C_new = 1;
    double C_old = 1;
    remainder -=C_new*p_knk;
    p_knk = p_knk*p1p;
       
    
    while(remainder>=0.0)  // &&i<n
    {
        i++;
        C_new = ((n-i+1)/i)*C_old;
        remainder -=C_new*p_knk;
        p_knk = p_knk*p1p;
        C_old = C_new;
    }
    #endif
    
    return (double)i;
    
}



__attribute__((always_inline)) inline __m512d binomialVec(__m512d N,__m512d P,__m512d REMAINDER,double *TR,double *ZT,__m512d CONST,__m512d CONST1,__m512d CONST2,__m512d CONST3,__m512i CONST4,__m512i CONST5)
{
         //__m512d CONST,CONST1,CONST2,CONST3;
	 //__m512i CONST4,CONST5;
	 __m512d PKNK,N1,P1P,N2,NLCCDYAD,TRI,RES1,RES2,RES3,C_old,C_new,NP1,RCPI;
    
          /*CONST = _mm512_set1_pd(1.0);       
          CONST1 = _mm512_set1_pd(15.0);
          CONST2 = _mm512_set1_pd(0.0);
          CONST3 = _mm512_set1_pd(101.0);
          CONST4 = _mm512_set1_epi64(4);
          CONST5 = _mm512_set1_epi32(1);*/

        double NN2;
        int NN3;
        int scale;
        __m512d N3,INDEX;
        __m512i index;
        __m256i index0;
        __mmask8 MASK = 255;
       
        NN3 = _mm512_reduce_gmax_pd(N);
        N3 = N;
        
        RES1 = _mm512_sub_pd(CONST,P);//1-p
        PKNK = _mm512_set1_pd(1.0);
        P1P = _mm512_div_pd(P,RES1);//p/(1-p)
        
        
        for(int t=0;t<NN3;t++)
        {
              MASK = _mm512_mask_cmp_pd_mask(MASK,N3,CONST2,_MM_CMPINT_GT);
              N3 = _mm512_sub_pd(N3,CONST);
              PKNK = _mm512_mask_mul_pd(PKNK,MASK,PKNK,RES1);              
        }
        //PKNK = _mm512_pow_pd(RES1,N3);


        #ifdef CMPFB    
        INDEX = _mm512_mul_pd(N,CONST3);        
        index = _mm512_cvtfxpnt_roundpd_epi32lo(INDEX,_MM_FROUND_TO_NEAREST_INT |_MM_FROUND_NO_EXC);
        #endif
        

        scale = 8;
        MASK = 255;

        #ifdef CMPFB    
        NLCCDYAD = _mm512_set1_pd(-1.0);
        #else    
        NLCCDYAD = _mm512_set1_pd(0.0);
        C_old = CONST;
        C_new = CONST;
        
        TRI = _mm512_mul_pd(C_new,PKNK);
        REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
        PKNK = _mm512_mul_pd(P1P,PKNK);                       
        
               
        RCPI = _mm512_set1_pd(1.0);  
        NP1 = _mm512_add_pd(N,CONST);  //n+1 
        
        #endif
        
        
        #ifdef CMPFB
        do
        {        
               MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
               NLCCDYAD = _mm512_mask_add_pd(NLCCDYAD,MASK,NLCCDYAD,CONST);                        
               TRI = _mm512_i32logather_pd(index,(void *)TR,scale);
               index = _mm512_add_epi32(index,CONST5);
               TRI = _mm512_mul_pd(TRI,PKNK);
               REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
               PKNK = _mm512_mul_pd(P1P,PKNK);
         
                    
        }while(MASK);
        
        #else
        while(MASK){
               NLCCDYAD = _mm512_mask_add_pd(NLCCDYAD,MASK,NLCCDYAD,CONST); 
                                                           
               RCPI = _mm512_mask_div_pd(RCPI,MASK,NP1,NLCCDYAD);//(n+1)/i
               C_new = _mm512_fmsub_pd(RCPI,C_old,C_old);//(n+1)/i*C_old-C_old
               C_old = C_new;
               TRI = _mm512_mul_pd(C_new,PKNK);
               REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
               PKNK = _mm512_mul_pd(P1P,PKNK);               
               MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);  
         }
         #endif
      

        return NLCCDYAD;

}


#ifdef varstruct
struct DyadInnerVars
{
     double k2nP;
     double expkm2n;
     double if2;
     double if3;
     double if4;
     double if5;
     double PhiCaLP2;
     double caoM;
     double PhiCaLP1;
     double PhiCaNa;
     double PhiCaK;  
     double jlca_dyadP; 
     double kmcsqn8; 
     double Kmcsqn4;
     double jrelP;
     double vsjrM;
     double h6;
     double x1P;
     double k2;
     double k4;
     
     double x2P;
     double x3P;
     double x2P2;
     double x3P2;
     double k1;
     double k3;
     double EP1;
     double EP2;
     double X4;
     double k7;
     double k8;
     double k4pp;
     double k3pp;
     double cassP1;
     double cassP2;
     double cacytP;   
     double Km4; 
     double PCabM2; 
     double PCabM1; 
};

struct DyadOuterVars
{
     int Ndyads;
     int randU;
     int Ndhpr;
     double PCa;
     double PCaNa;
     double PCaNap;
     double PCaK;
     double PCaKp;
     double ilca;
     double ilcana;
     double ilcak; 
     int Nryr;
     double t;
     int beats;
     int bcl;
     
     int Cell_LtypeRandN;
   
     double dt;
     double vjsr_dyad;
     double vnsr_dyad;
     double kmtrpn;
     double vcyt_dyad;
     double jrel;
     double inaca;
     double inacass;
     double ipca;
     double icab;
     double voltage;
     double vmyo;
     double vss_dyad;
     double Ntotdyads;
     double acap;
     double vfrt;
     double vffrt;
     double PCap;
     int NumThreads;
};
#endif


//in:dyadInnerVars, dyadOuterVars,Unit_cads[]
//out:ab5[],cd5[](new array)
//inout:Cell_nca[]
__attribute__((always_inline)) inline void dyadloop1LtypePCOMP(DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *Cell_nca,double *Cell_CaMKa,double *ab5,double *cd5)
{
     int Ndyads = dyadOuterVars->Ndyads;
     double k2nP = dyadInnerVars->k2nP;
     double if2 = dyadInnerVars->if2;
     double if3 = dyadInnerVars->if3;
     double if4 = dyadInnerVars->if4;
     double if5 = dyadInnerVars->if5;
     double expkm2n = dyadInnerVars->expkm2n;
     int numThreads = dyadOuterVars->NumThreads;
     
#ifdef AVEC
   #ifdef OPENMPONDYAD
     #pragma omp for schedule(static,CHUNCKSIZE) nowait
   #endif
     #pragma ivdep
     #pragma vector aligned //nontemporal
     #pragma vector always
     for(int i=0;i<Ndyads;i++)     
     {
         
         double KU = 1.0 + Kmnmult/Unit_cads[i];
         double KU2 = KU*KU;
         double KU4 = KU2*KU2;
	 double anca=dyadInnerVars->k2nP/(dyadInnerVars->k2nP+KU4);
         Cell_nca[i]=anca-(anca-Cell_nca[i])*dyadInnerVars->expkm2n;       
         double CaMKaTMP = Cell_CaMKa[i]/(Cell_CaMKa[i]+KmCaMK);
         ab5[i] = (1.0-CaMKaTMP)*(dyadInnerVars->if2+Cell_nca[i]*dyadInnerVars->if3);
	 cd5[i] = CaMKaTMP*(dyadInnerVars->if4+Cell_nca[i]*dyadInnerVars->if5);	 
     }
#endif
     
#ifdef MVEC      
     __m512d K2NP = _mm512_set1_pd(dyadInnerVars->k2nP);
     __m512d IF2 = _mm512_set1_pd(dyadInnerVars->if2);
     __m512d IF3 = _mm512_set1_pd(dyadInnerVars->if3);
     __m512d IF4 = _mm512_set1_pd(dyadInnerVars->if4);
     __m512d IF5 = _mm512_set1_pd(dyadInnerVars->if5);
     __m512d EXPKM2N = _mm512_set1_pd(dyadInnerVars->expkm2n);
     __m512d KMNMULT = _mm512_set1_pd(Kmnmult);
     __m512d CONST = _mm512_set1_pd(1.0);
     __m512d KMCAMK = _mm512_set1_pd(KmCaMK);
     //__m512d UCADS,KU,KU2,KU4,ANCA,CNCA,CCAMKA,CAMKATMP,RES1,RES2,RES3,RES4,RES5,RES6,AB5,CD5;
     __m512d KU,ANCA,CNCA,CCAMKA,CAMKATMP,RES5,AB5,CD5;
   #ifdef OPENMPONDYAD
     #pragma omp for schedule(static,CHUNCKSIZE) nowait
   #endif
     #pragma vector aligned nontemporal
     //#pragma unroll_and_jam(4)
     //#pragma prefetch Cell_CaMKa:0:4
     //#pragma prefetch Cell_CaMKa:1:4
     //#pragma prefetch Unit_cads:0:4
     //#pragma prefetch Unit_cads:1:4
     //#pragma prefetch Cell_nca:0:4
     //#pragma prefetch Cell_nca:1:4
     //#pragma noprefetch 
     //#pragma vector aligned nontemporal(ab5,cd5,Cell_nca)
     
     //#pragma noprefetch
     for(int i=0;i<Ndyads;i+=8)     
     {
     
         /*_mm_prefetch((const char *)&Unit_cads[i],_MM_HINT_T0);
         _mm_prefetch((const char *)&Unit_cads[i],_MM_HINT_T1);
         _mm_prefetch((const char *)&Cell_nca[i],_MM_HINT_T0);
         _mm_prefetch((const char *)&Cell_nca[i],_MM_HINT_T1);
         _mm_prefetch((const char *)&Cell_CaMKa[i],_MM_HINT_T0);
         _mm_prefetch((const char *)&Cell_CaMKa[i],_MM_HINT_T1);*/
         //__m512d UCADS,KU,KU2,KU4,ANCA,CNCA,CCAMKA,CAMKATMP,RES1,RES2,RES3,RES4,RES5,RES6,AB5,CD5;
         /*UCADS = _mm512_load_pd(Unit_cads+i);
         RES1 = _mm512_div_pd(KMNMULT,UCADS);
         KU = _mm512_add_pd(CONST,RES1);
         KU2 = _mm512_mul_pd(KU,KU);
         KU4 = _mm512_mul_pd(KU2,KU2);
         RES2 = _mm512_add_pd(K2NP,KU4);
         ANCA = _mm512_div_pd(K2NP,RES2);
         
         CNCA = _mm512_load_pd(Cell_nca+i);
         RES3 = _mm512_sub_pd(ANCA,CNCA); 
	 CNCA = _mm512_fnmadd_pd(RES3,EXPKM2N,ANCA);
	 _mm512_store_pd(Cell_nca+i,CNCA);
	 
	 CCAMKA = _mm512_load_pd(Cell_CaMKa+i);
	 RES4 = _mm512_add_pd(CCAMKA,KMCAMK);
	 CAMKATMP = _mm512_div_pd(CCAMKA,RES4);
	 
	 RES5 = _mm512_fmadd_pd(CNCA,IF3,IF2);
	 AB5 = _mm512_fnmadd_pd(RES5,CAMKATMP,RES5);
	 RES6 = _mm512_fmadd_pd(CNCA,IF5,IF4);
	 CD5 = _mm512_mul_pd(CAMKATMP,RES6);
	 
	 _mm512_store_pd(ab5+i,AB5);
	 _mm512_store_pd(cd5+i,CD5);*/	 
	 KU = _mm512_add_pd(CONST,_mm512_div_pd(KMNMULT,_mm512_load_pd(Unit_cads+i)));
           ANCA = _mm512_div_pd(K2NP,_mm512_add_pd(K2NP,_mm512_mul_pd(_mm512_mul_pd(KU,KU),_mm512_mul_pd(KU,KU))));

           CNCA = _mm512_fnmadd_pd(_mm512_sub_pd(ANCA,_mm512_load_pd(Cell_nca+i)),EXPKM2N,ANCA);

           CCAMKA = _mm512_load_pd(Cell_CaMKa+i);
           CAMKATMP = _mm512_div_pd(CCAMKA,_mm512_add_pd(CCAMKA,KMCAMK));

           RES5 = _mm512_fmadd_pd(CNCA,IF3,IF2);
           AB5 = _mm512_fnmadd_pd(RES5,CAMKATMP,RES5);
           //CD5 = _mm512_mul_pd(CAMKATMP,_mm512_fmadd_pd(CNCA,IF5,IF4));
            CD5 = _mm512_div_pd(_mm512_mul_pd(CAMKATMP,_mm512_fmadd_pd(CNCA,IF5,IF4)),_mm512_sub_pd(CONST,AB5)); 
           _mm512_store_pd(Cell_nca+i,CNCA);
           _mm512_store_pd(ab5+i,AB5);
           _mm512_store_pd(cd5+i,CD5);
     }
#endif
     
}


//in:dyadOuterVars,l_Random[],ab5[](new),cd5[](new)
//out:nLCC_dyad_np[](new array),nLCC_dyad_p[](new array)
//inout:
__attribute__((always_inline)) inline void dyadloop2LtypeOpenCOMP(cell &Cell,DyadOuterVars* dyadOuterVars,double *l_Random,double *T,double *ZT,double *ab5,double *cd5,double *nLCC_dyad_np,double *nLCC_dyad_p)
{
    int Ndyads = dyadOuterVars->Ndyads;
    int randU = dyadOuterVars->randU;
    int Ndhpr = dyadOuterVars->Ndhpr;
    int numThreads = dyadOuterVars->NumThreads;
#ifdef AVEC
   #ifdef OPENMPONDYAD
   #pragma omp for schedule(static,CHUNCKSIZE) nowait
   #endif
    //#pragma novector
     for(int i=0;i<Ndyads;i++)     
     {
        //for (int t = 0;t<Ndhpr;t++)
        //{       
        //    double random = l_Random[i*Ndhpr+t];
        //    if ( random<= ab5[i])
        //        nLCC_dyad_np[i]++;
        //    else if ( random<=(ab5[i]+cd5[i]))//step 2
        //        nLCC_dyad_p[i]++;
        //} 
	//double random1 = l_Random[i*2+0];
        //double random2 = l_Random[i*2+1];//result as cell3
        double random1 = l_Random[(i/8)*8+i];  
        double random2 = l_Random[(i/8+1)*8+i];  //result as cell4
        nLCC_dyad_np[i] = binomial((double)dyadOuterVars->Ndhpr,ab5[i],random1,T+101*(dyadOuterVars->Ndhpr),ZT);
        nLCC_dyad_p[i] = binomial((double)(dyadOuterVars->Ndhpr)-nLCC_dyad_np[i],cd5[i]/(1-ab5[i]),random2,T+101*((dyadOuterVars->Ndhpr)-(int)nLCC_dyad_np[i]),ZT);      
    }
#endif
 
 #ifdef MVEC  
    __m512d CONST,CONST1,CONST2,CONST3;
    __m512i CONST4,CONST5;
    
    CONST = _mm512_set1_pd(1.0);
    CONST1 = _mm512_set1_pd(dyadOuterVars->Ndhpr);
    CONST2 = _mm512_set1_pd(0.0);
    CONST3 = _mm512_set1_pd(101.0);
    CONST4 = _mm512_set1_epi64(4);
  
    CONST5 = _mm512_set1_epi32(1);
    
   #ifdef OPENMPONCELL
   #ifdef SAVERAND
     unsigned int randomUsed=0;
   #endif
   #endif 
   
   
    
    
  #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
    #pragma vector aligned nontemporal
    #ifdef SPLIT
    for(int nb=0;nb<NB;nb++)
     for(int i=nb*(Ndyads/NB);i<(nb+1)*(Ndyads/NB);i+=8)
     #else
     for(int i=0;i<Ndyads;i+=8) 
     #endif    
     {
        //for(int li=0;li<8;li++)
	// {
	//      double random1 = l_Random[i*2+li];
        //      nLCC_dyad_np[i+li] = binomial((double)dyadOuterVars->Ndhpr,ab5[i+li],random1,T+101*(dyadOuterVars->Ndhpr),ZT);            
	// } 
	
	#ifndef RandOrigin
	int subi = i%(LRS/2);
	if(subi==0)
	    vdRngUniform(0, Cell.Randomstream, LRS, l_Random, 0.0, 1.0-(1.2e-12));
	#endif

	#ifdef LtypeCallTwoBinomial
	__m512d REMAINDER1,REMAINDER2,AB5,CD5,NLCCDYADNP,NLCCDYADP,N1,N2; 
            
            N1 = _mm512_set1_pd(15.0);
            AB5 = _mm512_load_pd(ab5+i);
            REMAINDER1 = _mm512_load_pd(l_Random+i*2);
            NLCCDYADNP =  binomialVec(N1,AB5,REMAINDER1,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);
            _mm512_store_pd(nLCC_dyad_np+i,NLCCDYADNP);
            
            REMAINDER2 = _mm512_load_pd(l_Random+2*i+8);
            N2 = _mm512_sub_pd(N1,NLCCDYADNP);
            CD5 = _mm512_load_pd(cd5+i);
            NLCCDYADP =  binomialVec(N2,CD5,REMAINDER2,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);
            _mm512_store_pd(nLCC_dyad_p+i,NLCCDYADP);
         #else
	
        __m512d P,REMAINDER,AB5,CD5,PKNK,N1,P1P,N2,NLCCDYADNP,NLCCDYADP,TRI,RES1,RES2,RES3,C_old,C_new,RCPI,NP1;
        __mmask8 MASK;
        
        MASK = 255;
        bool flag1 =1;
        int j;
        
        P = _mm512_load_pd(ab5+i);
        RES1 = _mm512_sub_pd(CONST,P);
        //PKNK = _mm512_pow_pd(RES1,CONST1);
        RES2 = _mm512_mul_pd(RES1,RES1);//(1-p)^2
        RES3 = _mm512_mul_pd(RES2,RES2);//(1-p)^4
        PKNK = _mm512_mul_pd(RES3,RES3);//(1-p)^8
        PKNK = _mm512_mul_pd(RES3,PKNK);//(1-p)^12
        PKNK = _mm512_mul_pd(RES2,PKNK);//(1-p)^14
        PKNK = _mm512_mul_pd(RES1,PKNK);//(1-p)^15
        P1P = _mm512_div_pd(P,RES1);
           
        #ifdef RandOrigin
        #ifdef SAVERAND
        REMAINDER = _mm512_load_pd(l_Random+randomUsed);
        randomUsed+=8;
        #else
        REMAINDER = _mm512_load_pd(l_Random+i*2);
        #endif  //endif saverand
        #else
        REMAINDER = _mm512_load_pd(l_Random+subi*2);
        #endif
                
        #ifdef CMPFB    
        NLCCDYADNP = _mm512_set1_pd(-1.0);
        #else            
        NLCCDYADNP = _mm512_set1_pd(0.0);
        C_old = CONST;//set to 1
        C_new = CONST;        
        TRI = _mm512_mul_pd(C_new,PKNK);
        REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
        PKNK = _mm512_mul_pd(P1P,PKNK);                                              
        RCPI = _mm512_set1_pd(1.0);  
        NP1 = _mm512_add_pd(CONST1,CONST);  //n+1=15+1        
        #endif
        
        
        #ifdef CMPFB   
        for( j=0;j<8;j++)
        {
                MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
                NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
        	TRI = _mm512_set1_pd((T+101*(dyadOuterVars->Ndhpr))[j]);
        	TRI = _mm512_mul_pd(TRI,PKNK);
        	REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        	PKNK = _mm512_mul_pd(P1P,PKNK);   	
        }
        
        
        
        for( ;(j<16)&&(flag1);j++)
        {
                MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
                NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
        	TRI = _mm512_set1_pd((T+101*(dyadOuterVars->Ndhpr))[j]);
        	TRI = _mm512_mul_pd(TRI,PKNK);
        	REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        	PKNK = _mm512_mul_pd(P1P,PKNK);
        	if(MASK==0)
        	    flag1=0;	
        }
        #else
        
        while(MASK) 
        {
               NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
                                                           
               RCPI = _mm512_mask_div_pd(RCPI,MASK,NP1,NLCCDYADNP);//(n+1)/i
               C_new = _mm512_fmsub_pd(RCPI,C_old,C_old);//(n+1)/i*C_old-C_old
               C_old = C_new;
               TRI = _mm512_mul_pd(C_new,PKNK);
               REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
               PKNK = _mm512_mul_pd(P1P,PKNK);               
               MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);      
               
        }
        #endif
        
        _mm512_store_pd(nLCC_dyad_np+i,NLCCDYADNP);
    
        __m512d REMAINDER2;         
        N1 = CONST1;
        N2 = _mm512_sub_pd(N1,NLCCDYADNP);
        CD5 = _mm512_load_pd(cd5+i);


        NLCCDYADP = CONST2;//set to 0
        
        
        MASK = _mm512_cmp_pd_mask(N2,CONST,_MM_CMPINT_LT);
        if(MASK!=255)
        {
        #ifdef RandOrigin
        #ifdef SAVERAND
        REMAINDER2 = _mm512_load_pd(l_Random+randomUsed);
        randomUsed+=8;
        #else
        REMAINDER2 = _mm512_load_pd(l_Random+2*i+8);
        #endif  //endif of saverand
        #else
        REMAINDER2 = _mm512_load_pd(l_Random+2*subi+8);
        #endif
        NLCCDYADP =  binomialVec(N2,CD5,REMAINDER2,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);  
        
        }
        
            
        _mm512_store_pd(nLCC_dyad_p+i,NLCCDYADP);
        
        #endif //endof Ltypecalltwo
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
 
       /* for(int li=0;li<8;li++)
	 {
	      
              double random2 = l_Random[i*2+li+8];                                 
              nLCC_dyad_p[i+li] = binomial((double)(dyadOuterVars->Ndhpr)-nLCC_dyad_np[i+li],cd5[i+li]/(1-ab5[i+li]),random2,T+101*((dyadOuterVars->Ndhpr)-(int)nLCC_dyad_np[i+li]),ZT);                        
              
	 } */
       
              
    }
     /*#pragma omp for schedule(static,CHUNCKSIZE) nowait
     for(int i=0;i<Ndyads;i++)     
     {       
        
          double random2 = l_Random[(i/8+1)*8+i];   
          //nLCC_dyad_p[i] = binomial((double)(dyadOuterVars->Ndhpr)-nLCC_dyad_np[i],cd5[i],random2,T+101*((dyadOuterVars->Ndhpr)-(int)nLCC_dyad_np[i]),ZT);
          nLCC_dyad_p[i] = binomial((double)(dyadOuterVars->Ndhpr),cd5[i],random2,T+101*(dyadOuterVars->Ndhpr),ZT);
       
    }*/
    
    #ifdef OPENMPONCELL
    #ifdef SAVERAND
        Cell.RandUsedN+=randomUsed;
    #endif
    #endif
    
    #endif  //endif of MVEC

}


__attribute__((always_inline)) inline void dyadloop12LtypeCOMP(cell &Cell,DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *Cell_nca,double *Cell_CaMKa,double *l_Random,double *T,double *ZT,double *nLCC_dyad_np,double *nLCC_dyad_p)
{
    int Ndyads = dyadOuterVars->Ndyads;
     double k2nP = dyadInnerVars->k2nP;
     double if2 = dyadInnerVars->if2;
     double if3 = dyadInnerVars->if3;
     double if4 = dyadInnerVars->if4;
     double if5 = dyadInnerVars->if5;
     double expkm2n = dyadInnerVars->expkm2n;
     int numThreads = dyadOuterVars->NumThreads;
     
     __m512d K2NP = _mm512_set1_pd(dyadInnerVars->k2nP);
     __m512d IF2 = _mm512_set1_pd(dyadInnerVars->if2);
     __m512d IF3 = _mm512_set1_pd(dyadInnerVars->if3);
     __m512d IF4 = _mm512_set1_pd(dyadInnerVars->if4);
     __m512d IF5 = _mm512_set1_pd(dyadInnerVars->if5);
     __m512d EXPKM2N = _mm512_set1_pd(dyadInnerVars->expkm2n);
     __m512d KMNMULT = _mm512_set1_pd(Kmnmult);
     __m512d CONST = _mm512_set1_pd(1.0);
     __m512d KMCAMK = _mm512_set1_pd(KmCaMK);
    
     
     int randU = dyadOuterVars->randU;
     int Ndhpr = dyadOuterVars->Ndhpr;
     
     __m512d CONST1,CONST2,CONST3;
    __m512i CONST4,CONST5;
    
    //CONST = _mm512_set1_pd(1.0);
    CONST1 = _mm512_set1_pd(15.0);
    CONST2 = _mm512_set1_pd(0.0);
    CONST3 = _mm512_set1_pd(101.0);
    CONST4 = _mm512_set1_epi64(4);
    CONST5 = _mm512_set1_epi64(1);
   #ifdef OPENMPONDYAD
     #pragma omp for schedule(static,CHUNCKSIZE) nowait
   #endif
     #pragma vector aligned nontemporal
     #pragma unroll_and_jam(4)
     #ifdef SPLIT
    for(int nb=0;nb<NB;nb++)
     for(int i=nb*(Ndyads/NB);i<(nb+1)*(Ndyads/NB);i+=8)
     #else
     for(int i=0;i<Ndyads;i+=8) 
     #endif     
     {
         __m512d UCADS,KU,KU2,KU4,ANCA,CNCA,CCAMKA,CAMKATMP,RES1,RES2,RES3,RES4,RES5,RES6,AB5,CD5;
         UCADS = _mm512_load_pd(Unit_cads+i);
         RES1 = _mm512_div_pd(KMNMULT,UCADS);
         KU = _mm512_add_pd(CONST,RES1);
         KU2 = _mm512_mul_pd(KU,KU);
         KU4 = _mm512_mul_pd(KU2,KU2);
         RES2 = _mm512_add_pd(K2NP,KU4);
         ANCA = _mm512_div_pd(K2NP,RES2);
         
         CNCA = _mm512_load_pd(Cell_nca+i);
         RES3 = _mm512_sub_pd(ANCA,CNCA);
	 CNCA = _mm512_fnmadd_pd(RES3,EXPKM2N,ANCA);
	 _mm512_store_pd(Cell_nca+i,CNCA);
	 
	 CCAMKA = _mm512_load_pd(Cell_CaMKa+i);
	 RES4 = _mm512_add_pd(CCAMKA,KMCAMK);
	 CAMKATMP = _mm512_div_pd(CCAMKA,RES4);
	 
	 RES5 = _mm512_fmadd_pd(CNCA,IF3,IF2);
	 AB5 = _mm512_fnmadd_pd(RES5,CAMKATMP,RES5);
	 RES6 = _mm512_fmadd_pd(CNCA,IF5,IF4);
	 CD5 = _mm512_mul_pd(CAMKATMP,RES6);
	 CD5 = _mm512_div_pd(CD5,_mm512_sub_pd(CONST,AB5));//cd5[i]/(1-ab5[i])
	 
	 //_mm512_store_pd(ab5,AB5);
	 //_mm512_store_pd(cd5,CD5);
	 //_mm512_store_pd(cd5+i,CD5);
	 
	 
	 #ifndef RandOrigin
	int subi = i%(LRS/2);
	if(subi==0)
	    vdRngUniform(0, Cell.Randomstream, LRS, l_Random, 0.0, 1.0-(1.2e-12));
	#endif

        __m512d P,REMAINDER,PKNK,N1,P1P,N2,NLCCDYADNP,NLCCDYADP,TRI,C_old,C_new,RCPI,NP1;//,RES1,RES2,RES3;
        __mmask8 MASK;
        
        MASK = 255;
        bool flag1 =1;
        int j;
        
        //P = _mm512_load_pd(ab5+i);
        P = AB5;
        RES1 = _mm512_sub_pd(CONST,P);
        //PKNK = _mm512_pow_pd(RES1,CONST1);
        RES2 = _mm512_mul_pd(RES1,RES1);//(1-p)^2
        RES3 = _mm512_mul_pd(RES2,RES2);//(1-p)^4
        PKNK = _mm512_mul_pd(RES3,RES3);//(1-p)^8
        PKNK = _mm512_mul_pd(RES3,PKNK);//(1-p)^12
        PKNK = _mm512_mul_pd(RES2,PKNK);//(1-p)^14
        PKNK = _mm512_mul_pd(RES1,PKNK);//(1-p)^15
        P1P = _mm512_div_pd(P,RES1);
           
        #ifdef RandOrigin
        REMAINDER = _mm512_load_pd(l_Random+i*2);
        #else
        REMAINDER = _mm512_load_pd(l_Random+subi*2);
        #endif
                
        #ifdef CMPFB    
        NLCCDYADNP = _mm512_set1_pd(-1.0);
        #else            
        NLCCDYADNP = _mm512_set1_pd(0.0);
        C_old = CONST;//set to 1
        C_new = CONST;        
        TRI = _mm512_mul_pd(C_new,PKNK);
        REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
        PKNK = _mm512_mul_pd(P1P,PKNK);                                              
        RCPI = _mm512_set1_pd(1.0);  
        NP1 = _mm512_add_pd(CONST1,CONST);  //n+1=15+1        
        #endif
        
        
        #ifdef CMPFB   
        for( j=0;j<8;j++)
        {
                MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
                NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
        	TRI = _mm512_set1_pd((T+101*(dyadOuterVars->Ndhpr))[j]);
        	TRI = _mm512_mul_pd(TRI,PKNK);
        	REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        	PKNK = _mm512_mul_pd(P1P,PKNK);   	
        }
        
        
        
        for( ;(j<16)&&(flag1);j++)
        {
                MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
                NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
        	TRI = _mm512_set1_pd((T+101*(dyadOuterVars->Ndhpr))[j]);
        	TRI = _mm512_mul_pd(TRI,PKNK);
        	REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        	PKNK = _mm512_mul_pd(P1P,PKNK);
        	if(MASK==0)
        	    flag1=0;	
        }
        #else
        
        while(MASK) 
        {
               NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
                                                           
               RCPI = _mm512_mask_div_pd(RCPI,MASK,NP1,NLCCDYADNP);//(n+1)/i
               C_new = _mm512_fmsub_pd(RCPI,C_old,C_old);//(n+1)/i*C_old-C_old
               C_old = C_new;
               TRI = _mm512_mul_pd(C_new,PKNK);
               REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
               PKNK = _mm512_mul_pd(P1P,PKNK);               
               MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);      
               
        }
        #endif
        
        _mm512_store_pd(nLCC_dyad_np+i,NLCCDYADNP);
    
        __m512d REMAINDER2;         
        N1 = CONST1;
        N2 = _mm512_sub_pd(N1,NLCCDYADNP);
        //CD5 = _mm512_load_pd(cd5+i);

        #ifdef RandOrigin
        REMAINDER2 = _mm512_load_pd(l_Random+2*i+8);
        #else
        REMAINDER2 = _mm512_load_pd(l_Random+2*subi+8);
        #endif
        NLCCDYADP =  binomialVec(N2,CD5,REMAINDER2,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);      
        _mm512_store_pd(nLCC_dyad_p+i,NLCCDYADP);
	 
         	 
     }

}




//in:dyadInnerVars,Unit_cads[],nLCC_dyad_np[],nLCC_dyad_p[]
//out:jlca_dyad[](new)
//inout:dyadOuterVars(reduction vars),
#ifdef OPENMPONDYAD
__attribute__((always_inline)) inline void dyadloop3ICALCOMP(DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *nLCC_dyad_np,double *nLCC_dyad_p,double *jlca_dyad)
#endif
#ifdef OPENMPONCELL
__attribute__((always_inline)) inline void dyadloop3ICALCOMP(DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *nLCC_dyad_np,double *nLCC_dyad_p,double *jlca_dyad,double &ilca,double &ilcana,double &ilcak)
#endif
{
    int Ndyads = dyadOuterVars->Ndyads;
    double PhiCaLP2 = dyadInnerVars->PhiCaLP2;
    double caoM = dyadInnerVars->caoM;
    double PhiCaLP1 = dyadInnerVars->PhiCaLP1;
    int Ndhpr = dyadOuterVars->Ndhpr;
    double PCa = dyadOuterVars->PCa;
    double PCap = dyadOuterVars->PCap;
    double PhiCaNa = dyadInnerVars->PhiCaNa;
    double PCaNa = dyadOuterVars->PCaNa;
    double PCaNap = dyadOuterVars->PCaNap;
    double PCaKp = dyadOuterVars->PCaKp;
    double PhiCaK = dyadInnerVars->PhiCaK;
    double PCaK = dyadOuterVars->PCaK;
    double jlca_dyadP = dyadInnerVars->jlca_dyadP;    
    double RCNR = (double) 1.0/Ndhpr;

#ifdef AVEC
  #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
    #pragma ivdep
    #pragma vector aligned //nontemporal
    #pragma vector always
    for(int i=0;i<Ndyads;i++)     
    {
        double PhiCaL= (Unit_cads[i]*PhiCaLP2-caoM)*PhiCaLP1;
        double ilca_dyad = (PhiCaL*RCNR)*(PCa*nLCC_dyad_np[i] + PCap*nLCC_dyad_p[i]);
	double icana_dyad = PhiCaNa*(PCaNa*nLCC_dyad_np[i] + PCaNap*nLCC_dyad_p[i]);	
	double icak_dyad = PhiCaK*(PCaK*nLCC_dyad_np[i] + PCaKp*nLCC_dyad_p[i]);	

	ilca += ilca_dyad;	
        ilcana += icana_dyad;
        ilcak += icak_dyad;

	jlca_dyad[i] = jlca_dyadP*ilca_dyad;	
    }
#endif

#ifdef MVEC   
    
    __m512d PHICALP2,CAOM,PHICALP1,PCA,PCAP,PHICANA,PCANA,PCANAP,PCAKP,PHICAK,PCAK,JLCADYADP,RCNRR;
    __m512d UCADS,NLCCDYADNP,NLCCDYADP,PHICAL,ILCANA_DYAD,ICANA_DYAD,ICAK_DYAD,ILCA,ILCANA,ILCAK,JLCADYAD;
    __m512d RES1,RES2,RES3,RES4,RES5,RES6,RES7,RES8;
    
    PHICALP2 = _mm512_set1_pd(PhiCaLP2);
    CAOM = _mm512_set1_pd(caoM);
    PHICALP1 = _mm512_set1_pd(PhiCaLP1);
    PCA = _mm512_set1_pd(PCa);
    PCAP = _mm512_set1_pd(PCap);
    PHICANA = _mm512_set1_pd(PhiCaNa);
    PCANA = _mm512_set1_pd(PCaNa);
    PCANAP = _mm512_set1_pd(PCaNap);
    PCAKP = _mm512_set1_pd(PCaKp);
    PHICAK = _mm512_set1_pd(PhiCaK);
    PCAK = _mm512_set1_pd(PCaK);
    JLCADYADP = _mm512_set1_pd(jlca_dyadP);
    RCNRR = _mm512_set1_pd(RCNR);
    
    ILCA = _mm512_set1_pd(0.0);
    ILCANA = _mm512_set1_pd(0.0);
    ILCAK = _mm512_set1_pd(0.0);
  #ifdef OPENMPONDYAD  
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
    #pragma vector aligned nontemporal
    #pragma unroll_and_jam(4)
    for(int i=0;i<Ndyads;i+=8)   
    {
    	UCADS = _mm512_load_pd(Unit_cads+i);
    	NLCCDYADNP = _mm512_load_pd(nLCC_dyad_np+i);
    	NLCCDYADP = _mm512_load_pd(nLCC_dyad_p+i);
    	
    	RES1 = _mm512_fmsub_pd(UCADS,PHICALP2,CAOM);
	PHICAL = _mm512_mul_pd(RES1,PHICALP1);//compute PhiCaL
	

	RES2 = _mm512_mul_pd(PCA,NLCCDYADNP);
	RES3 = _mm512_fmadd_pd(PCAP,NLCCDYADP,RES2);
	RES4 = _mm512_mul_pd(PHICAL,RCNRR);
	ILCANA_DYAD = _mm512_mul_pd(RES4,RES3);//COMPUTE ilca_dyad
	
	RES5 = _mm512_mul_pd(PCANA,NLCCDYADNP);
	RES6 = _mm512_fmadd_pd(PCANAP,NLCCDYADP,RES5);
	ICANA_DYAD = _mm512_mul_pd(PHICANA,RES6);//compute icana_dyad
	
	RES7 = _mm512_mul_pd(PCAK,NLCCDYADNP);
	RES8 = _mm512_fmadd_pd(PCAKP,NLCCDYADP,RES7);
	ICAK_DYAD = _mm512_mul_pd(PHICAK,RES8);//compute icak_dyad
	
	ILCA = _mm512_add_pd(ILCA,ILCANA_DYAD);
	ILCANA = _mm512_add_pd(ILCANA,ICANA_DYAD);
	ILCAK = _mm512_add_pd(ILCAK,ICAK_DYAD);
	
	JLCADYAD = _mm512_mul_pd(JLCADYADP,ILCANA_DYAD);
	_mm512_store_pd(jlca_dyad+i,JLCADYAD);
    	
    }
    
    ilca += _mm512_reduce_add_pd(ILCA);
    ilcana += _mm512_reduce_add_pd(ILCANA);
    ilcak += _mm512_reduce_add_pd(ILCAK);
 #endif  
}

//reuse Unit_cads[],
#ifdef OPENMPONDYAD
__attribute__((always_inline)) inline void dyadloop123LtypeCOMP(cell &Cell,DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *Cell_nca,double *Cell_CaMKa,double *l_Random,double *T,double *ZT,double *jlca_dyad)
#endif
#ifdef OPENMPONCELL
__attribute__((always_inline)) inline void dyadloop123LtypeCOMP(cell &Cell,DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *Cell_nca,double *Cell_CaMKa,double *l_Random,double *T,double *ZT,double *jlca_dyad,double &ilca,double &ilcana,double &ilcak)
#endif
{
     //from dyadloop2
    int Ndyads = dyadOuterVars->Ndyads;
     double k2nP = dyadInnerVars->k2nP;
     double if2 = dyadInnerVars->if2;
     double if3 = dyadInnerVars->if3;
     double if4 = dyadInnerVars->if4;
     double if5 = dyadInnerVars->if5;
     double expkm2n = dyadInnerVars->expkm2n;
     int numThreads = dyadOuterVars->NumThreads;
     
     //from dyadloop3
    double PhiCaLP2 = dyadInnerVars->PhiCaLP2;
    double caoM = dyadInnerVars->caoM;
    double PhiCaLP1 = dyadInnerVars->PhiCaLP1;
    int Ndhpr = dyadOuterVars->Ndhpr;
    double PCa = dyadOuterVars->PCa;
    double PCap = dyadOuterVars->PCap;
    double PhiCaNa = dyadInnerVars->PhiCaNa;
    double PCaNa = dyadOuterVars->PCaNa;
    double PCaNap = dyadOuterVars->PCaNap;
    double PCaKp = dyadOuterVars->PCaKp;
    double PhiCaK = dyadInnerVars->PhiCaK;
    double PCaK = dyadOuterVars->PCaK;
    double jlca_dyadP = dyadInnerVars->jlca_dyadP;    
    double RCNR = (double) 1.0/Ndhpr;
     
     
     //from dyadloop2
     __m512d K2NP = _mm512_set1_pd(dyadInnerVars->k2nP);
     __m512d IF2 = _mm512_set1_pd(dyadInnerVars->if2);
     __m512d IF3 = _mm512_set1_pd(dyadInnerVars->if3);
     __m512d IF4 = _mm512_set1_pd(dyadInnerVars->if4);
     __m512d IF5 = _mm512_set1_pd(dyadInnerVars->if5);
     __m512d EXPKM2N = _mm512_set1_pd(dyadInnerVars->expkm2n);
     __m512d KMNMULT = _mm512_set1_pd(Kmnmult);
     __m512d CONST = _mm512_set1_pd(1.0);
     __m512d KMCAMK = _mm512_set1_pd(KmCaMK);  
     int randU = dyadOuterVars->randU;
      
     __m512d CONST1,CONST2,CONST3;
    __m512i CONST4,CONST5;    
    //CONST = _mm512_set1_pd(1.0);
    CONST1 = _mm512_set1_pd(15.0);
    CONST2 = _mm512_set1_pd(0.0);
    CONST3 = _mm512_set1_pd(101.0);
    CONST4 = _mm512_set1_epi64(4);
    CONST5 = _mm512_set1_epi64(1);
    
    //from dyadloop3
    __m512d PHICALP2,CAOM,PHICALP1,PCA,PCAP,PHICANA,PCANA,PCANAP,PCAKP,PHICAK,PCAK,JLCADYADP,RCNRR;
    __m512d PHICAL,ILCANA_DYAD,ICANA_DYAD,ICAK_DYAD,ILCA,ILCANA,ILCAK,JLCADYAD;
    //__m512d RES1,RES2,RES3,RES4,RES5,RES6,RES7,RES8;
    
    PHICALP2 = _mm512_set1_pd(PhiCaLP2);
    CAOM = _mm512_set1_pd(caoM);
    PHICALP1 = _mm512_set1_pd(PhiCaLP1);
    PCA = _mm512_set1_pd(PCa);
    PCAP = _mm512_set1_pd(PCap);
    PHICANA = _mm512_set1_pd(PhiCaNa);
    PCANA = _mm512_set1_pd(PCaNa);
    PCANAP = _mm512_set1_pd(PCaNap);
    PCAKP = _mm512_set1_pd(PCaKp);
    PHICAK = _mm512_set1_pd(PhiCaK);
    PCAK = _mm512_set1_pd(PCaK);
    JLCADYADP = _mm512_set1_pd(jlca_dyadP);
    RCNRR = _mm512_set1_pd(RCNR);
    
    ILCA = _mm512_set1_pd(0.0);
    ILCANA = _mm512_set1_pd(0.0);
    ILCAK = _mm512_set1_pd(0.0);
    
    #ifdef OPENMPONCELL
   #ifdef SAVERAND
     unsigned int randomUsed=0;
   #endif
   #endif 
    
    
   #ifdef OPENMPONDYAD
     #pragma omp for schedule(static,CHUNCKSIZE) nowait
   #endif
     #pragma vector aligned nontemporal
     #pragma unroll_and_jam(4)
     #ifdef SPLIT
    for(int nb=0;nb<NB;nb++)
     for(int i=nb*(Ndyads/NB);i<(nb+1)*(Ndyads/NB);i+=8)
     #else
     for(int i=0;i<Ndyads;i+=8) 
     #endif     
     {
         __m512d UCADS,KU,KU2,KU4,ANCA,CNCA,CCAMKA,CAMKATMP,RES1,RES2,RES3,RES4,RES5,RES6,RES7,RES8,AB5,CD5;
         UCADS = _mm512_load_pd(Unit_cads+i);
         RES1 = _mm512_div_pd(KMNMULT,UCADS);
         KU = _mm512_add_pd(CONST,RES1);
         KU2 = _mm512_mul_pd(KU,KU);
         KU4 = _mm512_mul_pd(KU2,KU2);
         RES2 = _mm512_add_pd(K2NP,KU4);
         ANCA = _mm512_div_pd(K2NP,RES2);
         
         CNCA = _mm512_load_pd(Cell_nca+i);
         RES3 = _mm512_sub_pd(ANCA,CNCA);
	 CNCA = _mm512_fnmadd_pd(RES3,EXPKM2N,ANCA);
	 _mm512_store_pd(Cell_nca+i,CNCA);
	 
	 CCAMKA = _mm512_load_pd(Cell_CaMKa+i);
	 RES4 = _mm512_add_pd(CCAMKA,KMCAMK);
	 CAMKATMP = _mm512_div_pd(CCAMKA,RES4);
	 
	 RES5 = _mm512_fmadd_pd(CNCA,IF3,IF2);
	 AB5 = _mm512_fnmadd_pd(RES5,CAMKATMP,RES5);
	 RES6 = _mm512_fmadd_pd(CNCA,IF5,IF4);
	 CD5 = _mm512_mul_pd(CAMKATMP,RES6);
	 CD5 = _mm512_div_pd(CD5,_mm512_sub_pd(CONST,AB5));//cd5[i]/(1-ab5[i])
	 
	 //_mm512_store_pd(ab5,AB5);
	 //_mm512_store_pd(cd5,CD5);
	 //_mm512_store_pd(cd5+i,CD5);
	 
	 
	 #ifndef RandOrigin
	int subi = i%(LRS/2);
	if(subi==0)
	    vdRngUniform(0, Cell.Randomstream, LRS, l_Random, 0.0, 1.0-(1.2e-12));
	#endif

        __m512d P,REMAINDER,PKNK,N1,P1P,N2,NLCCDYADNP,NLCCDYADP,TRI,C_old,C_new,RCPI,NP1;//,RES1,RES2,RES3;
        __mmask8 MASK;
        
        MASK = 255;
        bool flag1 =1;
        int j;
        
        //P = _mm512_load_pd(ab5+i);
        P = AB5;
        RES1 = _mm512_sub_pd(CONST,P);
        //PKNK = _mm512_pow_pd(RES1,CONST1);
        RES2 = _mm512_mul_pd(RES1,RES1);//(1-p)^2
        RES3 = _mm512_mul_pd(RES2,RES2);//(1-p)^4
        PKNK = _mm512_mul_pd(RES3,RES3);//(1-p)^8
        PKNK = _mm512_mul_pd(RES3,PKNK);//(1-p)^12
        PKNK = _mm512_mul_pd(RES2,PKNK);//(1-p)^14
        PKNK = _mm512_mul_pd(RES1,PKNK);//(1-p)^15
        P1P = _mm512_div_pd(P,RES1);
        
        
         #ifdef RandOrigin
        #ifdef SAVERAND
        REMAINDER = _mm512_load_pd(l_Random+randomUsed);
        randomUsed+=8;
        #else
        REMAINDER = _mm512_load_pd(l_Random+i*2);
        #endif  //endif saverand
        #else
        REMAINDER = _mm512_load_pd(l_Random+subi*2);
        #endif
        
                
        #ifdef CMPFB    
        NLCCDYADNP = _mm512_set1_pd(-1.0);
        #else            
        NLCCDYADNP = _mm512_set1_pd(0.0);
        C_old = CONST;//set to 1
        C_new = CONST;        
        TRI = _mm512_mul_pd(C_new,PKNK);
        REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
        PKNK = _mm512_mul_pd(P1P,PKNK);                                              
        RCPI = _mm512_set1_pd(1.0);  
        NP1 = _mm512_add_pd(CONST1,CONST);  //n+1=15+1        
        #endif
        
        
        #ifdef CMPFB   
        for( j=0;j<8;j++)
        {
                MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
                NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
        	TRI = _mm512_set1_pd((T+101*(dyadOuterVars->Ndhpr))[j]);
        	TRI = _mm512_mul_pd(TRI,PKNK);
        	REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        	PKNK = _mm512_mul_pd(P1P,PKNK);   	
        }
        
        
        
        for( ;(j<16)&&(flag1);j++)
        {
                MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);
                NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
        	TRI = _mm512_set1_pd((T+101*(dyadOuterVars->Ndhpr))[j]);
        	TRI = _mm512_mul_pd(TRI,PKNK);
        	REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
        	PKNK = _mm512_mul_pd(P1P,PKNK);
        	if(MASK==0)
        	    flag1=0;	
        }
        #else
        
        while(MASK) 
        {
               NLCCDYADNP = _mm512_mask_add_pd(NLCCDYADNP,MASK,NLCCDYADNP,CONST); 
                                                           
               RCPI = _mm512_mask_div_pd(RCPI,MASK,NP1,NLCCDYADNP);//(n+1)/i
               C_new = _mm512_fmsub_pd(RCPI,C_old,C_old);//(n+1)/i*C_old-C_old
               C_old = C_new;
               TRI = _mm512_mul_pd(C_new,PKNK);
               REMAINDER = _mm512_sub_pd(REMAINDER,TRI);
               PKNK = _mm512_mul_pd(P1P,PKNK);               
               MASK = _mm512_mask_cmp_pd_mask(MASK,REMAINDER,CONST2,_MM_CMPINT_GE);      
               
        }
        #endif
        
        //_mm512_store_pd(nLCC_dyad_np+i,NLCCDYADNP);
    
        __m512d REMAINDER2;         
        N1 = CONST1;
        N2 = _mm512_sub_pd(N1,NLCCDYADNP);
        //CD5 = _mm512_load_pd(cd5+i);
        
        
        
        NLCCDYADP = CONST2;//set to 0              
        MASK = _mm512_cmp_pd_mask(N2,CONST,_MM_CMPINT_LT);
        if(MASK!=255)
        {
        #ifdef RandOrigin
        #ifdef SAVERAND
        REMAINDER2 = _mm512_load_pd(l_Random+randomUsed);
        randomUsed+=8;
        #else
        REMAINDER2 = _mm512_load_pd(l_Random+2*i+8);
        #endif  //endif of saverand
        #else
        REMAINDER2 = _mm512_load_pd(l_Random+2*subi+8);
        #endif
        NLCCDYADP =  binomialVec(N2,CD5,REMAINDER2,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);  
        
        }     
        //_mm512_store_pd(nLCC_dyad_p+i,NLCCDYADP);
        
        //dyadloop3
        //UCADS = _mm512_load_pd(Unit_cads+i);
    	//NLCCDYADNP = _mm512_load_pd(nLCC_dyad_np+i);
    	//NLCCDYADP = _mm512_load_pd(nLCC_dyad_p+i);
    	
    	RES1 = _mm512_fmsub_pd(UCADS,PHICALP2,CAOM);
	PHICAL = _mm512_mul_pd(RES1,PHICALP1);//compute PhiCaL
	

	RES2 = _mm512_mul_pd(PCA,NLCCDYADNP);
	RES3 = _mm512_fmadd_pd(PCAP,NLCCDYADP,RES2);
	RES4 = _mm512_mul_pd(PHICAL,RCNRR);
	ILCANA_DYAD = _mm512_mul_pd(RES4,RES3);//COMPUTE ilca_dyad
	
	RES5 = _mm512_mul_pd(PCANA,NLCCDYADNP);
	RES6 = _mm512_fmadd_pd(PCANAP,NLCCDYADP,RES5);
	ICANA_DYAD = _mm512_mul_pd(PHICANA,RES6);//compute icana_dyad
	
	RES7 = _mm512_mul_pd(PCAK,NLCCDYADNP);
	RES8 = _mm512_fmadd_pd(PCAKP,NLCCDYADP,RES7);
	ICAK_DYAD = _mm512_mul_pd(PHICAK,RES8);//compute icak_dyad
	
	ILCA = _mm512_add_pd(ILCA,ILCANA_DYAD);
	ILCANA = _mm512_add_pd(ILCANA,ICANA_DYAD);
	ILCAK = _mm512_add_pd(ILCAK,ICAK_DYAD);
	
	JLCADYAD = _mm512_mul_pd(JLCADYADP,ILCANA_DYAD);
	_mm512_store_pd(jlca_dyad+i,JLCADYAD);
	 
         	 
     }
     
     //for dyadloop3
     ilca += _mm512_reduce_add_pd(ILCA);
     ilcana += _mm512_reduce_add_pd(ILCANA);
     ilcak += _mm512_reduce_add_pd(ILCAK);
     
     
      #ifdef OPENMPONCELL
      #ifdef SAVERAND
        Cell.RandUsedN+=randomUsed;
      #endif
      #endif


}




//in:Unit_NryrO1,dyadOuterVars
//out:Cell_nactive,tref[](new array)
//inout:Cell_trel,
__attribute__((always_inline)) inline void dyadloop4TrefCOMP(DyadOuterVars* dyadOuterVars,double *Unit_NryrO1,int *Cell_nactive,double *Cell_trel,double *tref)
{
    int Ndyads = dyadOuterVars->Ndyads;
    double t = dyadOuterVars->t;
    int beats = dyadOuterVars->beats;
    int bcl = dyadOuterVars->bcl;
    int limit=	dyadOuterVars->Nryr/5;
    double bcl98 = -0.98*dyadOuterVars->bcl;	

    int numThreads = dyadOuterVars->NumThreads;
    int RCT = (int)Ndyads/numThreads;

    if (t>(beats-8)*bcl && t<(beats-8)*bcl + 500)
{
  #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
    #pragma ivdep
    #pragma vector aligned nontemporal
    #pragma vector always
    for(int i=0;i<Ndyads;i++)  
    {
        int nryropen = Unit_NryrO1[i];
        if (nryropen>limit) 
	{
		Cell_trel[i] = t;

        //to demonstrate EAD unhealthy test
     		tref[i] = -325;
	}
	else
	{
		tref[i] =t-Cell_trel[i]-325;
	}
    }
}
	else
{
  #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
    #pragma ivdep
    #pragma vector aligned nontemporal
    #pragma vector always
    for(int i=0;i<Ndyads;i++)  
    {
        int nryropen = Unit_NryrO1[i];
        if (nryropen>limit) 
	{
		Cell_trel[i] = t;
        //to demonstrate EAD unhealthy test
     		tref[i] = bcl98;
	}
	else
	{
		tref[i] = t-Cell_trel[i]+bcl98;
	}
        
    }
}
}

//in:Unit_cads,dyadInnerVars,dyadOuterVars,Unit_cajsr,tref[]
//out:kC1O1[],kC1C2[],kC2C1[],kC2O2[](new array)
//inout:
__attribute__((always_inline)) inline void dyadloop5RyrPCOMP(DyadOuterVars* dyadOuterVars,DyadInnerVars *dyadInnerVars,double *Unit_cads,double *Unit_cajsr,double *tref,double *kC1O1,double *kC1C2,double *kC2C1,double *kC2O2)
{
     int Ndyads = dyadOuterVars->Ndyads;
     double dt = dyadOuterVars->dt;
     double kmcsqn8 = dyadInnerVars->kmcsqn8;
     double Kmcsqn4 = dyadInnerVars->Kmcsqn4;
     double Km4 = dyadInnerVars->Km4;
 
     double dt3 = dt*3;
     int numThreads = dyadOuterVars->NumThreads;
     
#ifdef AVEC
  #ifdef OPENMPONDYAD
     #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
     #pragma ivdep
     #pragma vector aligned //nontemporal
     #pragma vector always
    for(int i=0;i<Ndyads;i++)  
    {
         double U2 = Unit_cads[i]*Unit_cads[i];
         double pT = U2*U2;

	 double UR2 = Unit_cajsr[i]*Unit_cajsr[i];
	 double UR4 = UR2*UR2;
	 double UR8 = UR4*UR4;
	 double csqn1=kmcsqn8/(UR8+kmcsqn8); 
         double csqn = csqnbar*csqn1;
         double csqnca = csqnbar - csqn;        
         kC1O1[i]= dt3*pT/(pT+Km4);
         kC1C2[i]= dt*csqn1;
         kC2C1[i] = (dt-dt*csqn1)/(1+exp(-tref[i]*1000));

         kC2O2[i] = dt3*pT/(pT + Kmcsqn4);	
         
    }
 #endif
 
 #ifdef MVEC 
    __m512d DT,KMCSQN8,KMCSQN4,KM4,DT3;
    __m512d UCADS,U2,PT,UCAJSR,UR2,UR4,UR8,CSQN1,CSQN,CSQNCA,CSQNBAR,KC1O1,KC1C2,KC2C1,KC2O2,TREF;
    __m512d RES1,RES2,RES3,RES4,RES5,RES6,RES7,RES8,CONST,CONST1;
    DT = _mm512_set1_pd(dt);
    KMCSQN8 = _mm512_set1_pd(kmcsqn8);
    KMCSQN4 = _mm512_set1_pd(Kmcsqn4);
    CSQNBAR = _mm512_set1_pd(csqnbar);
    KM4 = _mm512_set1_pd(Km4);
    DT3 = _mm512_set1_pd(dt3);
    CONST = _mm512_set1_pd(-1000.0);
    CONST1 = _mm512_set1_pd(1.0);
    
 #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
 #endif
    #pragma vector aligned nontemporal
    #pragma unroll_and_jam(4)
    for(int i=0;i<Ndyads;i+=8)
    {  
    	TREF = _mm512_load_pd(tref+i);
    	UCADS = _mm512_load_pd(Unit_cads+i);
    	UCAJSR = _mm512_load_pd(Unit_cajsr+i);
    	U2 = _mm512_mul_pd(UCADS,UCADS);
    	PT = _mm512_mul_pd(U2,U2);
    	
    	UR2 = _mm512_mul_pd(UCAJSR,UCAJSR);
    	UR4 = _mm512_mul_pd(UR2,UR2);
    	UR8 = _mm512_mul_pd(UR4,UR4);
    	RES1 = _mm512_add_pd(UR8,KMCSQN8);
    	CSQN1 = _mm512_div_pd(KMCSQN8,RES1);
    	CSQN = _mm512_mul_pd(CSQNBAR,CSQN1);
    	RES2 = _mm512_mul_pd(DT3,PT);
    	RES3 = _mm512_add_pd(PT,KM4);
    	KC1O1 = _mm512_div_pd(RES2,RES3);
    	KC1C2 = _mm512_mul_pd(DT,CSQN1);
    	RES4 = _mm512_fnmadd_pd(DT,CSQN1,DT);
    	RES5 = _mm512_mul_pd(TREF,CONST);
    	RES6 = _mm512_exp_pd(RES5);
    	RES7 = _mm512_add_pd(CONST1,RES6);
    	KC2C1 = _mm512_div_pd(RES4,RES7);
    	
    	RES8 = _mm512_add_pd(PT,KMCSQN4);
	KC2O2 = _mm512_div_pd(RES2,RES8);
    	_mm512_store_pd(kC1O1+i,KC1O1);
    	_mm512_store_pd(kC1C2+i,KC1C2);
    	_mm512_store_pd(kC2C1+i,KC2C1);
    	_mm512_store_pd(kC2O2+i,KC2O2);
    	
        
        
    }
 #endif
}

#ifdef OPENMPONDYAD
__attribute__((always_inline)) inline void dyadloop345(DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *nLCC_dyad_np,double *nLCC_dyad_p,double *jlca_dyad,double *Unit_NryrO1,int *Cell_nactive,double *Cell_trel,double *tref1,double *Unit_cajsr,double *kC1O1,double *kC1C2,double *kC2C1,double *kC2O2)
#endif

#ifdef OPENMPONCELL
__attribute__((always_inline)) inline void dyadloop345(DyadInnerVars *dyadInnerVars,DyadOuterVars* dyadOuterVars,double *Unit_cads,double *nLCC_dyad_np,double *nLCC_dyad_p,double *jlca_dyad,double *Unit_NryrO1,int *Cell_nactive,double *Cell_trel,double *tref1,double *Unit_cajsr,double *kC1O1,double *kC1C2,double *kC2C1,double *kC2O2,double &ilca,double &ilcana,double &ilcak)
#endif

{
     int Ndyads = dyadOuterVars->Ndyads;
    /*double PhiCaLP2 = dyadInnerVars->PhiCaLP2;
    double caoM = dyadInnerVars->caoM;
    double PhiCaLP1 = dyadInnerVars->PhiCaLP1;
    int Ndhpr = dyadOuterVars->Ndhpr;
    double PCa = dyadOuterVars->PCa;
    double PCap = dyadOuterVars->PCap;
    double PhiCaNa = dyadInnerVars->PhiCaNa;
    double PCaNa = dyadOuterVars->PCaNa;
    double PCaNap = dyadOuterVars->PCaNap;
    double PCaKp = dyadOuterVars->PCaKp;
    double PhiCaK = dyadInnerVars->PhiCaK;*/
    double PCaK = dyadOuterVars->PCaK;
    double jlca_dyadP = dyadInnerVars->jlca_dyadP;
    //double ilca = 0.0,ilcana = 0.0,ilcak = 0.0;
    int numThreads = dyadOuterVars->NumThreads;
    int RCT = (int)Ndyads/numThreads;    
    

    double t = dyadOuterVars->t;
    int beats = dyadOuterVars->beats;
    int bcl = dyadOuterVars->bcl;
    int limit=	dyadOuterVars->Nryr/5;
    double bcl98 = -0.98*dyadOuterVars->bcl;
    double kmcsqn8 = dyadInnerVars->kmcsqn8;
     double Kmcsqn4 = dyadInnerVars->Kmcsqn4;
     double Km4 = dyadInnerVars->Km4;	

     double dt = dyadOuterVars->dt;
     double RCNR = (double) 1.0/(dyadOuterVars->Ndhpr);
     
 
     double dt3 = dt*3;
     
    
    if (t>(beats-8)*bcl && t<(beats-8)*bcl + 500)
    {
      #ifdef OPENMPONDYAD
        #pragma omp for schedule(static,CHUNCKSIZE) nowait
      #endif
        #pragma ivdep
        #pragma vector aligned //nontemporal
        #pragma vector always
        for(int i=0;i<Ndyads;i++)     
        {
                double PhiCaL= (Unit_cads[i]*dyadInnerVars->PhiCaLP2-dyadInnerVars->caoM)*dyadInnerVars->PhiCaLP1;
                double ilca_dyad = (PhiCaL*RCNR)*(dyadOuterVars->PCa*nLCC_dyad_np[i] + dyadOuterVars->PCap*nLCC_dyad_p[i]);
	        double icana_dyad = dyadInnerVars->PhiCaNa*(dyadOuterVars->PCaNa*nLCC_dyad_np[i] + dyadOuterVars->PCaNap*nLCC_dyad_p[i]);	
	        double icak_dyad = dyadInnerVars->PhiCaK*(PCaK*nLCC_dyad_np[i] + dyadOuterVars->PCaKp*nLCC_dyad_p[i]);	

	        ilca += ilca_dyad;	
                ilcana += icana_dyad;
                ilcak += icak_dyad;

	        jlca_dyad[i] = jlca_dyadP*ilca_dyad;
        
                double tref;
        
                double nryropen = Unit_NryrO1[i];
                if (nryropen>limit) 
	        {
		     Cell_trel[i] = t;
                     //to demonstrate EAD unhealthy test
     		     //tref[i] = -325;
     		     tref = -325;
	        }
	        else
	        {
		     //tref[i] =t-Cell_trel[i]-325;
		     tref =t-Cell_trel[i]-325;
	        }
	
	        double U2 = Unit_cads[i]*Unit_cads[i];
                double pT = U2*U2;

	        double UR2 = Unit_cajsr[i]*Unit_cajsr[i];
	        double UR4 = UR2*UR2;
	        double UR8 = UR4*UR4;
	        double csqn1=kmcsqn8/(UR8+kmcsqn8);                 
                double csqn = csqnbar*csqn1;              
                double csqnca = csqnbar - csqn;        
                kC1O1[i]= dt3*pT/(pT+Km4);
                kC1C2[i]= dt*csqn1;         
                kC2C1[i] = (dt-dt*csqn1)/(1+exp(-tref*1000));

                kC2O2[i] = dt3*pT/(pT + Kmcsqn4);
           
        }
    }
    else
    {
      #ifdef OPENMPONDYAD
        #pragma omp for schedule(static,CHUNCKSIZE) nowait
      #endif
        #pragma ivdep
        #pragma vector aligned //nontemporal
        #pragma vector always
        for(int i=0;i<Ndyads;i++)     
        {              
                 double PhiCaL= (Unit_cads[i]*dyadInnerVars->PhiCaLP2-dyadInnerVars->caoM)*dyadInnerVars->PhiCaLP1;                 
                 double ilca_dyad = (PhiCaL*RCNR)*(dyadOuterVars->PCa*nLCC_dyad_np[i] + dyadOuterVars->PCap*nLCC_dyad_p[i]);
		 double icana_dyad = dyadInnerVars->PhiCaNa*(dyadOuterVars->PCaNa*nLCC_dyad_np[i] + dyadOuterVars->PCaNap*nLCC_dyad_p[i]);	
		 double icak_dyad = dyadInnerVars->PhiCaK*(PCaK*nLCC_dyad_np[i] + dyadOuterVars->PCaKp*nLCC_dyad_p[i]);	

		 ilca += ilca_dyad;	
        	 ilcana += icana_dyad;
        	 ilcak += icak_dyad;

		 jlca_dyad[i] = jlca_dyadP*ilca_dyad;    
                 double tref;
    
        	 double nryropen = Unit_NryrO1[i];
        	 if (nryropen>limit) 
		 {
		      Cell_trel[i] = t;
        	      //to demonstrate EAD unhealthy test
     		      //tref[i] = bcl98;
     		      tref = bcl98;
		 }
		 else
		 {
		      //tref[i] = t-Cell_trel[i]+bcl98;
		      tref = t-Cell_trel[i]+bcl98;
		 }
        
        	 double U2 = Unit_cads[i]*Unit_cads[i];
         	 double pT = U2*U2;

	 	double UR2 = Unit_cajsr[i]*Unit_cajsr[i];
	  	double UR4 = UR2*UR2;
	  	double UR8 = UR4*UR4;
	  	double csqn1=kmcsqn8/(UR8+kmcsqn8);         	
         	double csqn = csqnbar*csqn1;         	
         	double csqnca = csqnbar - csqn;        
         	kC1O1[i]= dt3*pT/(pT+Km4);
         	kC1C2[i]= dt*csqn1;         	
         	kC2C1[i] = (dt-dt*csqn1)/(1+exp(-tref*1000));

         	kC2O2[i] = dt3*pT/(pT + Kmcsqn4);
    	    
       }
   }     
}



//in:dyadOuterVars,l_Random,ZT,B,kC1O1[],kC1C2[],kC2C1[],kC2O2[]
//out:
//inout:Unit_NryrO1[],Unit_NryrO2[],Unit_NryrC1[],Unit_NryrC2[]
__attribute__((always_inline)) inline void dyadloop6RyrOpenCOMP(DyadOuterVars* dyadOuterVars,double *l_Random,double *T,double *ZT,double *B,double *kC1O1,double *kC1C2,double *kC2C1,double *kC2O2,double *Unit_NryrO1,double *Unit_NryrO2,double *Unit_NryrC1,double *Unit_NryrC2)
{
    int Ndyads = dyadOuterVars->Ndyads;
    int randU = dyadOuterVars->randU;
    int Cell_LtypeRandN = dyadOuterVars->Cell_LtypeRandN;
    double dt = dyadOuterVars->dt;
    int Ndhpr = dyadOuterVars->Ndhpr;
 
 
    int numThreads = dyadOuterVars->NumThreads;
     int RCT = (int)Ndyads/numThreads;
 #ifdef RYRNew    
     __attribute__((aligned(64))) double cc[8] = {0.0};
    __attribute__((aligned(64))) double dd[8] = {0.0};
    __attribute__((aligned(64))) double nryrc1[8] = {0.0};
    __attribute__((aligned(64))) double nryrc2[8] = {0.0};
    __attribute__((aligned(64))) double nryro1[8] = {0.0};
    __attribute__((aligned(64))) double nryro2[8] = {0.0};
    double *rder = &l_Random[Cell_LtypeRandN*Ndyads];
 #endif
     
     
  #ifdef OPENMPONDYAD
     #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
  
  #ifdef RYROrigin
     for(int i=0;i<Ndyads;i++)     
     {
        double nryrc1,nryro1,nryrc2,nryro2,nryropen;
        nryrc1 = nryro1 = nryrc2 = nryro2 = nryropen = 0;
        double c;
        double d;

        double remainder1 = l_Random[Cell_LtypeRandN*Ndyads+i*8+0];
        double remainder2 = l_Random[Cell_LtypeRandN*Ndyads+i*8+1];
        double remainder3 = l_Random[Cell_LtypeRandN*Ndyads+i*8+2];
        double remainder4 = l_Random[Cell_LtypeRandN*Ndyads+i*8+3];
        double remainder5 = l_Random[Cell_LtypeRandN*Ndyads+i*8+4];
        double remainder6 = l_Random[Cell_LtypeRandN*Ndyads+i*8+5];
        double remainder7 = l_Random[Cell_LtypeRandN*Ndyads+i*8+6];
        double remainder8 = l_Random[Cell_LtypeRandN*Ndyads+i*8+7];


        if(Unit_NryrC1[i]>0)
        {

             c=binomial(Unit_NryrC1[i],kC1O1[i],remainder1,T+101*(int)(Unit_NryrC1[i]),ZT);
             d=binomial(Unit_NryrC1[i]-c,kC1C2[i]/(1-kC1O1[i]),remainder5,T+101*(int)(Unit_NryrC1[i]-c),ZT);
             nryrc1+=Unit_NryrC1[i]-(c+d);
             nryro1+=c;
             nryrc2+=d;
         }

         if(Unit_NryrO1[i]>0)
         {
    	     //kO1O2 = dt*csqn1;
	     double kO1O2 = kC1C2[i];
             double kO1C1 = 0.5*dt;
    	     c=constp_binomial(remainder2,B+101*(int)(Unit_NryrO1[i]));
    	     //c=binomial(Unit_NryrO1[i],kO1C1,remainder6,T+101*(int)(Unit_NryrO1[i]),ZT);

             d=binomial(Unit_NryrO1[i]-c,kO1O2/(1-kO1C1),remainder6,T+101*(int)(Unit_NryrO1[i]-c),ZT);
    	     nryro1+=Unit_NryrO1[i]-(c+d);
             nryrc1+=c;
             nryro2+=d;
         }

         //if(Unit_NryrC2[i]>0) 
         //{ 
         c=binomial(Unit_NryrC2[i],kC2C1[i],remainder3,T+101*(int)(Unit_NryrC2[i]),ZT);
         d=binomial(Unit_NryrC2[i]-c,kC2O2[i]/(1-kC2C1[i]),remainder7,T+101*(int)(Unit_NryrC2[i]-c),ZT);
         nryrc2+=Unit_NryrC2[i]-(c+d);
         nryrc1+=c;
         nryro2+=d;
         //}

         if(Unit_NryrO2[i]>0)
         {    
	     double kO2O1 = kC2C1[i];
             double kO2C2 = 0.5*dt;
             c=constp_binomial(remainder4,B+101*(int)(Unit_NryrO2[i]));
             //c=binomial(Unit_NryrO2[i],kO2C2,remainder8,T+101*(int)(Unit_NryrO2[i]),ZT);
             d=binomial(Unit_NryrO2[i]-c,kO2O1/(1-kO2C2),remainder8,T+101*(int)(Unit_NryrO2[i]-c),ZT);
             nryro2+=Unit_NryrO2[i]-(c+d);
             nryrc2+=c;
             nryro1+=d;
          }

          Unit_NryrC1[i]=(double)nryrc1;
          Unit_NryrO1[i]=(double)nryro1;
          Unit_NryrC2[i]=(double)nryrc2;
          Unit_NryrO2[i]=(double)nryro2;

    }
 #endif
    
    
 #ifdef RYRNew
    //for(int i=0;i<Ndyads;i+=8)     
    // {
        
     // for(int li=0;li<8;li++)
     // {
        /*double nryrc1,nryro1,nryrc2,nryro2,nryropen;
        nryrc1 = nryro1 = nryrc2 = nryro2 = nryropen = 0;
        double c;
        double d;

        double remainder1 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+0];
        double remainder2 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+1];
        double remainder3 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+2];
        double remainder4 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+3];
        double remainder5 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+4];
        double remainder6 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+5];
        double remainder7 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+6];
        double remainder8 = l_Random[Cell_LtypeRandN*Ndyads+(i+li)*8+7];
        
        if(Unit_NryrC1[i+li]>0)
        {

             c=binomial(Unit_NryrC1[i+li],kC1O1[i+li],remainder1,T+101*(int)(Unit_NryrC1[i+li]),ZT);
             d=binomial(Unit_NryrC1[i+li]-c,kC1C2[i+li]/(1-kC1O1[i+li]),remainder2,T+101*(int)(Unit_NryrC1[i+li]-c),ZT);
             nryrc1+=Unit_NryrC1[i+li]-(c+d);
             nryro1+=c;
             nryrc2+=d;
         }

         if(Unit_NryrO1[i+li]>0)
         {
    	     //kO1O2 = dt*csqn1;
	     double kO1O2 = kC1C2[i+li];
             double kO1C1 = 0.5*dt;
    	     c=constp_binomial(remainder3,B+101*(int)(Unit_NryrO1[i+li]));

             d=binomial(Unit_NryrO1[i+li]-c,kO1O2/(1-kO1C1),remainder4,T+101*(int)(Unit_NryrO1[i+li]-c),ZT);
    	     nryro1+=Unit_NryrO1[i+li]-(c+d);
             nryrc1+=c;
             nryro2+=d;
         }

         //if(Unit_NryrC2[i]>0) 
         //{ 
         c=binomial(Unit_NryrC2[i+li],kC2C1[i+li],remainder5,T+101*(int)(Unit_NryrC2[i+li]),ZT);
         d=binomial(Unit_NryrC2[i+li]-c,kC2O2[i+li]/(1-kC2C1[i+li]),remainder6,T+101*(int)(Unit_NryrC2[i+li]-c),ZT);
         nryrc2+=Unit_NryrC2[i+li]-(c+d);
         nryrc1+=c;
         nryro2+=d;
         //}

         if(Unit_NryrO2[i+li]>0)
         {    
	     double kO2O1 = kC2C1[i+li];
             double kO2C2 = 0.5*dt;
             c=constp_binomial(remainder7,B+101*(int)(Unit_NryrO2[i+li]));
             d=binomial(Unit_NryrO2[i+li]-c,kO2O1/(1-kO2C2),remainder8,T+101*(int)(Unit_NryrO2[i+li]-c),ZT);
             nryro2+=Unit_NryrO2[i+li]-(c+d);
             nryrc2+=c;
             nryro1+=d;
          }

          Unit_NryrC1[i+li]=(double)nryrc1;
          Unit_NryrO1[i+li]=(double)nryro1;
          Unit_NryrC2[i+li]=(double)nryrc2;
          Unit_NryrO2[i+li]=(double)nryro2;*/
        
       /* for(int li=0;li<8;li++)
        {
        nryrc1[li] = 0;
        nryro1[li] = 0;
        nryrc2[li] = 0;
        nryro2[li] = 0;
        
        if(Unit_NryrC1[i+li]>0)
        {

             
             cc[li]=binomial(Unit_NryrC1[i+li],kC1O1[i+li],rder[i*8+li],T+101*(int)(Unit_NryrC1[i+li]),ZT);
             dd[li]=binomial(Unit_NryrC1[i+li]-cc[li],kC1C2[i+li]/(1-kC1O1[i+li]),rder[(i+1)*8+li],T+101*(int)(Unit_NryrC1[i+li]-cc[li]),ZT);
             //cc[li]=binomial(Unit_NryrC1[i+li],kC1O1[i+li],rder[(i+li)*8+0],T+101*(int)(Unit_NryrC1[i+li]),ZT);
             //dd[li]=binomial(Unit_NryrC1[i+li]-cc[li],kC1C2[i+li]/(1-kC1O1[i+li]),rder[(i+li)*8+1],T+101*(int)(Unit_NryrC1[i+li]-cc[li]),ZT);
             nryrc1[li]+=Unit_NryrC1[i+li]-(cc[li]+dd[li]);
             nryro1[li]+=cc[li];
             nryrc2[li]+=dd[li];
         }
         
         }
       
         

	for(int li=0;li<8;li++)
        {
         if(Unit_NryrO1[i+li]>0)
         {
    	     //kO1O2 = dt*csqn1;
	     double kO1O2 = kC1C2[i+li];
             double kO1C1 = 0.5*dt;
    	   
             cc[li]=constp_binomial(rder[(i+2)*8+li],B+101*(int)(Unit_NryrO1[i+li]));
             dd[li]=binomial(Unit_NryrO1[i+li]-cc[li],kO1O2/(1-kO1C1),rder[(i+3)*8+li],T+101*(int)(Unit_NryrO1[i+li]-cc[li]),ZT);
             //cc[li]=constp_binomial(rder[(i+li)*8+2],B+101*(int)(Unit_NryrO1[i+li]));
             //dd[li]=binomial(Unit_NryrO1[i+li]-cc[li],kO1O2/(1-kO1C1),rder[(i+li)*8+3],T+101*(int)(Unit_NryrO1[i+li]-cc[li]),ZT);
    	     nryro1[li]+=Unit_NryrO1[i+li]-(cc[li]+dd[li]);
             nryrc1[li]+=cc[li];
             nryro2[li]+=dd[li];
         }
         }

	for(int li=0;li<8;li++)
        {
         //if(Unit_NryrC2[i]>0) 
         //{ 
         
         cc[li]=binomial(Unit_NryrC2[i+li],kC2C1[i+li],rder[(i+4)*8+li],T+101*(int)(Unit_NryrC2[i+li]),ZT);
         dd[li]=binomial(Unit_NryrC2[i+li]-cc[li],kC2O2[i+li]/(1-kC2C1[i+li]),rder[(i+5)*8+li],T+101*(int)(Unit_NryrC2[i+li]-cc[li]),ZT);
         //cc[li]=binomial(Unit_NryrC2[i+li],kC2C1[i+li],rder[(i+li)*8+4],T+101*(int)(Unit_NryrC2[i+li]),ZT);
         //dd[li]=binomial(Unit_NryrC2[i+li]-cc[li],kC2O2[i+li]/(1-kC2C1[i+li]),rder[(i+li)*8+5],T+101*(int)(Unit_NryrC2[i+li]-cc[li]),ZT);
         nryrc2[li]+=Unit_NryrC2[i+li]-(cc[li]+dd[li]);
         nryrc1[li]+=cc[li];
         nryro2[li]+=dd[li];
         //}
         }
         
         for(int li=0;li<8;li++)
        {

         if(Unit_NryrO2[i+li]>0)
         {    
	     double kO2O1 = kC2C1[i+li];
             double kO2C2 = 0.5*dt;
             
             cc[li]=constp_binomial(rder[(i+6)*8+li],B+101*(int)(Unit_NryrO2[i+li]));
             dd[li]=binomial(Unit_NryrO2[i+li]-cc[li],kO2O1/(1-kO2C2),rder[(i+7)*8+li],T+101*(int)(Unit_NryrO2[i+li]-cc[li]),ZT);
             //cc[li]=constp_binomial(rder[(i+li)*8+6],B+101*(int)(Unit_NryrO2[i+li]));
             //dd[li]=binomial(Unit_NryrO2[i+li]-cc[li],kO2O1/(1-kO2C2),rder[(i+li)*8+7],T+101*(int)(Unit_NryrO2[i+li]-cc[li]),ZT);
             nryro2[li]+=Unit_NryrO2[i+li]-(cc[li]+dd[li]);
             nryrc2[li]+=cc[li];
             nryro1[li]+=dd[li];
          }
          }

          
    	for(int li=0;li<8;li++)
        {
          Unit_NryrC1[i+li]=nryrc1[li];
          Unit_NryrO1[i+li]=nryro1[li];
          Unit_NryrC2[i+li]=nryrc2[li];
          Unit_NryrO2[i+li]=nryro2[li];
          }
         */
          
          
          
     //}//end of sub for loop

    //}
    
    //test 1 ,2(replace const_binomial with normal binomial)
    for(int i=0;i<Ndyads;i++)     
     {
        double nryrc1,nryro1,nryrc2,nryro2,nryropen;
        nryrc1 = nryro1 = nryrc2 = nryro2 = nryropen = 0;
        double c;
        double d;

        double remainder1 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+0*8];
        double remainder2 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+1*8];
        double remainder3 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+2*8];
        double remainder4 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+3*8];
        double remainder5 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+4*8];
        double remainder6 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+5*8];
        double remainder7 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+6*8];
        double remainder8 = l_Random[Cell_LtypeRandN*Ndyads+(i/8)*64+(i%8)+7*8];


        if(Unit_NryrC1[i]>0)
        {

             c=binomial(Unit_NryrC1[i],kC1O1[i],remainder1,T+101*(int)(Unit_NryrC1[i]),ZT);
             d=binomial(Unit_NryrC1[i]-c,kC1C2[i]/(1-kC1O1[i]),remainder2,T+101*(int)(Unit_NryrC1[i]-c),ZT);
             nryrc1+=Unit_NryrC1[i]-(c+d);
             nryro1+=c;
             nryrc2+=d;
         }

         if(Unit_NryrO1[i]>0)
         {
    	     //kO1O2 = dt*csqn1;
	     double kO1O2 = kC1C2[i];
             double kO1C1 = 0.5*dt;
    	     //c=constp_binomial(remainder3,B+101*(int)(Unit_NryrO1[i]));
    	     c=binomial(Unit_NryrO1[i],kO1C1,remainder3,T+101*(int)(Unit_NryrO1[i]),ZT);

             d=binomial(Unit_NryrO1[i]-c,kO1O2/(1-kO1C1),remainder4,T+101*(int)(Unit_NryrO1[i]-c),ZT);
    	     nryro1+=Unit_NryrO1[i]-(c+d);
             nryrc1+=c;
             nryro2+=d;
         }

         //if(Unit_NryrC2[i]>0) 
         //{ 
         c=binomial(Unit_NryrC2[i],kC2C1[i],remainder5,T+101*(int)(Unit_NryrC2[i]),ZT);
         d=binomial(Unit_NryrC2[i]-c,kC2O2[i]/(1-kC2C1[i]),remainder6,T+101*(int)(Unit_NryrC2[i]-c),ZT);
         nryrc2+=Unit_NryrC2[i]-(c+d);
         nryrc1+=c;
         nryro2+=d;
         //}

         if(Unit_NryrO2[i]>0)
         {    
	     double kO2O1 = kC2C1[i];
             double kO2C2 = 0.5*dt;
             //c=constp_binomial(remainder7,B+101*(int)(Unit_NryrO2[i]));
             c=binomial(Unit_NryrO2[i],kO2C2,remainder7,T+101*(int)(Unit_NryrO2[i]),ZT);
             d=binomial(Unit_NryrO2[i]-c,kO2O1/(1-kO2C2),remainder8,T+101*(int)(Unit_NryrO2[i]-c),ZT);
             nryro2+=Unit_NryrO2[i]-(c+d);
             nryrc2+=c;
             nryro1+=d;
          }

          Unit_NryrC1[i]=(double)nryrc1;
          Unit_NryrO1[i]=(double)nryro1;
          Unit_NryrC2[i]=(double)nryrc2;
          Unit_NryrO2[i]=(double)nryro2;

    }
    
    
    
    
  #endif
    
    
}


#ifdef RYR
__attribute__((always_inline)) inline void dyadloop6RyrOpenCOMPVec(cell &Cell,DyadOuterVars* dyadOuterVars,double *l_Random,double *T,double *ZT,double *B,double *kC1O1,double *kC1C2,double *kC2C1,double *kC2O2,double *Unit_NryrO1,double *Unit_NryrO2,double *Unit_NryrC1,double *Unit_NryrC2)
{
    int Ndyads = dyadOuterVars->Ndyads;
    int randU = dyadOuterVars->randU;
    int Cell_LtypeRandN = dyadOuterVars->Cell_LtypeRandN;
    double dt = dyadOuterVars->dt;
    int Ndhpr = dyadOuterVars->Ndhpr;
 
 
    int numThreads = dyadOuterVars->NumThreads;

    __m512d ONE = _mm512_set1_pd(1.0);
    
    __m512d CONST,CONST1,CONST2,CONST3;
    __m512i CONST4,CONST5;
    
    CONST = _mm512_set1_pd(1.0);
    CONST1 = _mm512_set1_pd(Ndhpr);
    //CONST1 = _mm512_set1_pd(15.0);
    CONST2 = _mm512_set1_pd(0.0);
    CONST3 = _mm512_set1_pd(101.0);
    CONST4 = _mm512_set1_epi64(4);
   
    CONST5 = _mm512_set1_epi32(1);
    
    __m512i I = _mm512_set1_epi32(0);
    
    
    
    
  #ifdef RandOrigin   
  #ifdef SAVERAND
     double *rder = l_Random+Cell.RandUsedN;////////////////////////////////////////////important
  #else
     double *rder = l_Random+Cell_LtypeRandN*Ndyads;
  #endif   //endif of saverand
  #else
     double *rder = l_Random;//if gnerate random many time during one timestep
  #endif
  
  #ifdef OPENMPONCELL
  #ifdef SAVERAND
  unsigned int randomUsed=0;
  #endif
  #endif
  

  #ifdef OPENMPONDYAD
     #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
     #ifdef SPLIT
    for(int nb=0;nb<NB;nb++)
     for(int i=nb*(Ndyads/NB);i<(nb+1)*(Ndyads/NB);i+=8)
     #else
     for(int i=0;i<Ndyads;i+=8) 
     #endif      
     {
        #ifndef RandOrigin
        int subi = i%(RRS/8);
        if(subi==0)
	    vdRngUniform(0, Cell.Randomstream, RRS, l_Random, 0.0, 1.0-(1.2e-12));
	#endif    
	    
        __m512d NRYRC1 = _mm512_set1_pd(0.0);
        __m512d NRYRO1 = _mm512_set1_pd(0.0);
        __m512d NRYRC2 = _mm512_set1_pd(0.0);
        __m512d NRYRO2 = _mm512_set1_pd(0.0);  
        
   
        __m512d NC1 = _mm512_load_pd(Unit_NryrC1+i);
        __mmask8 mask;
        mask = _mm512_cmp_pd_mask(NC1,ONE,_MM_CMPINT_LT);
        //if(_mm512_cmp_pd_mask(NC1,ONE,_CMP_LT_OQ))
        if(mask!=255)
        {                    
             //RES = NC1;  
             __m512d P = _mm512_load_pd(kC1O1+i);
             #ifdef RandOrigin
             #ifdef SAVERAND
             __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             __m512d REMAINDER = _mm512_load_pd(rder+i*8);
             #endif    //endif of saverand
             #else 
             __m512d REMAINDER = _mm512_load_pd(rder+subi*8);   
             #endif
             
             __m512d C = binomialVec(NC1,P,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             __m512d P2 = _mm512_load_pd(kC1C2+i);
             P2 = _mm512_div_pd(P2,_mm512_sub_pd(ONE,P)); 

             __m512d N = _mm512_sub_pd(NC1,C);  
             #ifdef RandOrigin 
             #ifdef SAVERAND
             REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             REMAINDER = _mm512_load_pd(rder+(i+1)*8); 
             #endif //endif of saverand
             #else   
             REMAINDER = _mm512_load_pd(rder+(subi+1)*8);  
             #endif  
             __m512d D = binomialVec(N,P2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             //NRYRC1 = _mm512_sub_pd(NRYRC1,C);   
             //NRYRC1 = _mm512_sub_pd(NRYRC1,D); 
             NC1 = _mm512_sub_pd(NC1,C);
             NC1 = _mm512_sub_pd(NC1,D); 
             NRYRC1 = _mm512_add_pd(NRYRC1,NC1);  
             NRYRO1 = _mm512_add_pd(NRYRO1,C); 
             NRYRC2 = _mm512_add_pd(NRYRC2,D); 
             
             
         
         }
          
         
       
         __m512d NO1 = _mm512_load_pd(Unit_NryrO1+i);
         mask = _mm512_cmp_pd_mask(NO1,ONE,_MM_CMPINT_LT);
         //if(_mm512_cmp_pd_mask(NO1,ONE,_CMP_LT_OQ))
         if(mask!=255)
         {

             
             //NRYRO1 = NO1;            
             __m512d P = _mm512_set1_pd(0.5*dt);  
             #ifdef RandOrigin   
             #ifdef SAVERAND
             __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             __m512d REMAINDER = _mm512_load_pd(rder+(i+2)*8);
             #endif //endif saverand
             #else    
             __m512d REMAINDER = _mm512_load_pd(rder+(subi+2)*8);  
             #endif 
             
             #ifdef ConstBinomial 
             __m512d C = constp_binomialVec(NO1,REMAINDER,B,I,CONST5,CONST3);
             #else
             __m512d C= binomialVec(NO1,P,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);
             #endif
             
             //load kO1O2 the same as kC1C2
             __m512d P2 = _mm512_load_pd(kC1C2+i);
             P2 = _mm512_div_pd(P2,_mm512_sub_pd(ONE,P));
             
             __m512d N = _mm512_sub_pd(NO1,C);   
             #ifdef RandOrigin
             #ifdef SAVERAND
             REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             REMAINDER = _mm512_load_pd(rder+(i+3)*8);  
             #endif //endif saverand
             #else
             REMAINDER = _mm512_load_pd(rder+(subi+3)*8);   
             #endif
             __m512d D= binomialVec(N,P2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             //NRYRO1 = _mm512_sub_pd(NRYRO1,C);   
             //NRYRO1 = _mm512_sub_pd(NRYRO1,D);
             NO1 = _mm512_sub_pd(NO1,C);
             NO1 = _mm512_sub_pd(NO1,D);
             NRYRO1 = _mm512_add_pd(NRYRO1,NO1);    
             NRYRC1 = _mm512_add_pd(NRYRC1,C); 
             NRYRO2 = _mm512_add_pd(NRYRO2,D); 
             
            
             
                                      
         }

	

        __m512d NC2 = _mm512_load_pd(Unit_NryrC2+i);
         //NRYRC2 = NC2;  
         __m512d PC2C1 = _mm512_load_pd(kC2C1+i);
         #ifdef RandOrigin
         #ifdef SAVERAND
         __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
         randomUsed+=8;
         #else
         __m512d REMAINDER = _mm512_load_pd(rder+(i+4)*8); 
         #endif  //endif save rand
         #else
         __m512d REMAINDER = _mm512_load_pd(rder+(subi+4)*8);  
         #endif 
         __m512d C= binomialVec(NC2,PC2C1,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

         __m512d PC2O2 = _mm512_load_pd(kC2O2+i);
         PC2O2 = _mm512_div_pd(PC2O2,_mm512_sub_pd(ONE,PC2C1)); 

         __m512d N = _mm512_sub_pd(NC2,C);   
         #ifdef RandOrigin
         #ifdef SAVERAND
         REMAINDER = _mm512_load_pd(rder+randomUsed);
         randomUsed+=8;
         #else
         REMAINDER = _mm512_load_pd(rder+(i+5)*8);
         #endif //endif save rand
         #else    
         REMAINDER = _mm512_load_pd(rder+(subi+5)*8);  
         #endif
         __m512d D= binomialVec(N,PC2O2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

         //NRYRC2 = _mm512_sub_pd(NRYRC2,C);   
         //NRYRC2 = _mm512_sub_pd(NRYRC2,D);  
         NC2 = _mm512_sub_pd(NC2,C);
         NC2 = _mm512_sub_pd(NC2,D);
         NRYRC2 = _mm512_add_pd(NRYRC2,NC2);  
         NRYRC1 = _mm512_add_pd(NRYRC1,C); 
         NRYRO2 = _mm512_add_pd(NRYRO2,D);
         
         


	

        __m512d NO2 = _mm512_load_pd(Unit_NryrO2+i);
        mask = _mm512_cmp_pd_mask(NO2,ONE,_MM_CMPINT_LT);
        //if(_mm512_cmp_pd_mask(NO2,ONE,_CMP_LT_OQ))
        if(mask!=255)
        {                    
                       
             __m512d P = _mm512_set1_pd(0.5*dt);  
             #ifdef RandOrigin   
             #ifdef SAVERAND
             __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             __m512d REMAINDER = _mm512_load_pd(rder+(i+6)*8); 
             #endif //endif save rand 
             #else 
             __m512d REMAINDER = _mm512_load_pd(rder+(subi+6)*8); 
             #endif 
             
             #ifdef ConstBinomial 
             __m512d C= constp_binomialVec(NO2,REMAINDER,B,I,CONST5,CONST3);
             #else
             __m512d C= binomialVec(NO2,P,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);
             #endif
             
             //load kO2O1 the same as kC2C1
             __m512d P2 = _mm512_load_pd(kC2C1+i);
             P2 = _mm512_div_pd(P2,_mm512_sub_pd(ONE,P));
             
             __m512d N = _mm512_sub_pd(NO2,C);   
             #ifdef RandOrigin
             #ifdef SAVERAND
             REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             REMAINDER = _mm512_load_pd(rder+(i+7)*8); 
             #endif  //endif saverand
             #else  
             REMAINDER = _mm512_load_pd(rder+(subi+7)*8); 
             #endif   
             __m512d D= binomialVec(N,P2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             NO2 = _mm512_sub_pd(NO2,C);   
             NO2 = _mm512_sub_pd(NO2,D); 
             NRYRO2 = _mm512_add_pd(NRYRO2,NO2); 
             NRYRC2 = _mm512_add_pd(NRYRC2,C); 
             NRYRO1 = _mm512_add_pd(NRYRO1,D);   
             
             
         }
         _mm512_store_pd(Unit_NryrC1+i,NRYRC1);
         _mm512_store_pd(Unit_NryrO1+i,NRYRO1);
         _mm512_store_pd(Unit_NryrC2+i,NRYRC2);
         _mm512_store_pd(Unit_NryrO2+i,NRYRO2);
         
        

    }
    
    #ifdef OPENMPONCELL
    #ifdef SAVERAND
        Cell.RandUsedN+=randomUsed;
    #endif
    #endif
}
#endif


__attribute__((always_inline)) inline void dyadloop56(DyadOuterVars* dyadOuterVars,DyadInnerVars *dyadInnerVars,double *Unit_cads,double *Unit_cajsr,double *tref,cell &Cell,double *l_Random,double *T,double *ZT,double *B,double *Unit_NryrO1,double *Unit_NryrO2,double *Unit_NryrC1,double *Unit_NryrC2)
{
    //from dyad5
    int Ndyads = dyadOuterVars->Ndyads;
     double dt = dyadOuterVars->dt;
     double kmcsqn8 = dyadInnerVars->kmcsqn8;
     double Kmcsqn4 = dyadInnerVars->Kmcsqn4;
     double Km4 = dyadInnerVars->Km4; 
     double dt3 = dt*3;
     int numThreads = dyadOuterVars->NumThreads;     

    __m512d DT,KMCSQN8,KMCSQN4,KM4,DT3;
    __m512d UCADS,U2,PT,UCAJSR,UR2,UR4,UR8,CSQN1,CSQN,CSQNCA,CSQNBAR,KC1O1,KC1C2,KC2C1,KC2O2,TREF;
    __m512d RES1,RES2,RES3,RES4,RES5,RES6,RES7,RES8,CONST6,CONST7;
    DT = _mm512_set1_pd(dt);
    KMCSQN8 = _mm512_set1_pd(kmcsqn8);
    KMCSQN4 = _mm512_set1_pd(Kmcsqn4);
    CSQNBAR = _mm512_set1_pd(csqnbar);
    KM4 = _mm512_set1_pd(Km4);
    DT3 = _mm512_set1_pd(dt3);
    CONST6 = _mm512_set1_pd(-1000.0);
    CONST7 = _mm512_set1_pd(1.0);
        
    //from dyad6
    int randU = dyadOuterVars->randU;
    int Cell_LtypeRandN = dyadOuterVars->Cell_LtypeRandN;   
    int Ndhpr = dyadOuterVars->Ndhpr;
 
    __m512d ONE = _mm512_set1_pd(1.0);    
    __m512d CONST,CONST1,CONST2,CONST3;
    __m512i CONST4,CONST5;    
    CONST = _mm512_set1_pd(1.0);
    CONST1 = _mm512_set1_pd(Ndhpr);
    CONST2 = _mm512_set1_pd(0.0);
    CONST3 = _mm512_set1_pd(101.0);
    CONST4 = _mm512_set1_epi64(4);   
    CONST5 = _mm512_set1_epi32(1);    
    __m512i I = _mm512_set1_epi32(0);
    
    
    
    #ifdef RandOrigin   
    #ifdef SAVERAND
     double *rder = l_Random+Cell.RandUsedN;////////////////////////////////////////////important
    #else
     double *rder = l_Random+Cell_LtypeRandN*Ndyads;
    #endif   //endif of saverand
    #else
     double *rder = l_Random;//if gnerate random many time during one timestep
    #endif
  
    #ifdef OPENMPONCELL
    #ifdef SAVERAND
    unsigned int randomUsed=0;
    #endif
    #endif
 

    
 #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
 #endif
    #pragma vector aligned nontemporal
    #pragma unroll_and_jam(4)
    #ifdef SPLIT
    for(int nb=0;nb<NB;nb++)
     for(int i=nb*(Ndyads/NB);i<(nb+1)*(Ndyads/NB);i+=8)
     #else
     for(int i=0;i<Ndyads;i+=8) 
     #endif    
    {  
    	TREF = _mm512_load_pd(tref+i);
    	UCADS = _mm512_load_pd(Unit_cads+i);
    	UCAJSR = _mm512_load_pd(Unit_cajsr+i);
    	U2 = _mm512_mul_pd(UCADS,UCADS);
    	PT = _mm512_mul_pd(U2,U2);
    	
    	UR2 = _mm512_mul_pd(UCAJSR,UCAJSR);
    	UR4 = _mm512_mul_pd(UR2,UR2);
    	UR8 = _mm512_mul_pd(UR4,UR4);
    	RES1 = _mm512_add_pd(UR8,KMCSQN8);
    	CSQN1 = _mm512_div_pd(KMCSQN8,RES1);
    	CSQN = _mm512_mul_pd(CSQNBAR,CSQN1);
    	RES2 = _mm512_mul_pd(DT3,PT);
    	RES3 = _mm512_add_pd(PT,KM4);
    	KC1O1 = _mm512_div_pd(RES2,RES3);
    	KC1C2 = _mm512_mul_pd(DT,CSQN1);
    	RES4 = _mm512_fnmadd_pd(DT,CSQN1,DT);
    	RES5 = _mm512_mul_pd(TREF,CONST6);
    	RES6 = _mm512_exp_pd(RES5);
    	RES7 = _mm512_add_pd(CONST7,RES6);
    	KC2C1 = _mm512_div_pd(RES4,RES7);
    	
    	RES8 = _mm512_add_pd(PT,KMCSQN4);
	KC2O2 = _mm512_div_pd(RES2,RES8);
    	//_mm512_store_pd(kC1O1+i,KC1O1);
    	//_mm512_store_pd(kC1C2+i,KC1C2);
    	//_mm512_store_pd(kC2C1+i,KC2C1);
    	//_mm512_store_pd(kC2O2+i,KC2O2);
    	
        #ifndef RandOrigin
        int subi = i%(RRS/8);
        if(subi==0)
	    vdRngUniform(0, Cell.Randomstream, RRS, l_Random, 0.0, 1.0-(1.2e-12));
	#endif    
	    
        __m512d NRYRC1 = _mm512_set1_pd(0.0);
        __m512d NRYRO1 = _mm512_set1_pd(0.0);
        __m512d NRYRC2 = _mm512_set1_pd(0.0);
        __m512d NRYRO2 = _mm512_set1_pd(0.0);  
        
   
        __m512d NC1 = _mm512_load_pd(Unit_NryrC1+i);
        __mmask8 mask;
        mask = _mm512_cmp_pd_mask(NC1,ONE,_MM_CMPINT_LT);
        //if(_mm512_cmp_pd_mask(NC1,ONE,_CMP_LT_OQ))
        if(mask!=255)
        {                    
             //RES = NC1;  
             //__m512d P = _mm512_load_pd(kC1O1+i);
             __m512d P = KC1O1;
             
             
             #ifdef RandOrigin
             #ifdef SAVERAND
             __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             __m512d REMAINDER = _mm512_load_pd(rder+i*8);
             #endif    //endif of saverand
             #else 
             __m512d REMAINDER = _mm512_load_pd(rder+subi*8);   
             #endif
             
             
             __m512d C = binomialVec(NC1,P,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             //__m512d P2 = _mm512_load_pd(kC1C2+i);
             __m512d P2 = KC1C2;
             
             P2 = _mm512_div_pd(P2,_mm512_sub_pd(ONE,P)); 

             __m512d N = _mm512_sub_pd(NC1,C);  
             #ifdef RandOrigin 
             #ifdef SAVERAND
             REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             REMAINDER = _mm512_load_pd(rder+(i+1)*8); 
             #endif //endif of saverand
             #else   
             REMAINDER = _mm512_load_pd(rder+(subi+1)*8);  
             #endif  
             __m512d D = binomialVec(N,P2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             //NRYRC1 = _mm512_sub_pd(NRYRC1,C);   
             //NRYRC1 = _mm512_sub_pd(NRYRC1,D); 
             NC1 = _mm512_sub_pd(NC1,C);
             NC1 = _mm512_sub_pd(NC1,D); 
             NRYRC1 = _mm512_add_pd(NRYRC1,NC1);  
             NRYRO1 = _mm512_add_pd(NRYRO1,C); 
             NRYRC2 = _mm512_add_pd(NRYRC2,D); 
         
         }
          
         
       
         __m512d NO1 = _mm512_load_pd(Unit_NryrO1+i);
         mask = _mm512_cmp_pd_mask(NO1,ONE,_MM_CMPINT_LT);
         //if(_mm512_cmp_pd_mask(NO1,ONE,_CMP_LT_OQ))
         if(mask!=255)
         {

             
             //NRYRO1 = NO1;            
             __m512d P = _mm512_set1_pd(0.5*dt);  
             #ifdef RandOrigin   
             #ifdef SAVERAND
             __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             __m512d REMAINDER = _mm512_load_pd(rder+(i+2)*8);
             #endif //endif saverand
             #else    
             __m512d REMAINDER = _mm512_load_pd(rder+(subi+2)*8);  
             #endif 
             
             #ifdef ConstBinomial 
             __m512d C = constp_binomialVec(NO1,REMAINDER,B,I,CONST5,CONST3);
             #else
             __m512d C= binomialVec(NO1,P,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);
             #endif
             
             //load kO1O2 the same as kC1C2
             //__m512d P2 = _mm512_load_pd(kC1C2+i);
             __m512d P2 = KC1C2;
             
             P2 = _mm512_div_pd(P2,_mm512_sub_pd(ONE,P));
             
             __m512d N = _mm512_sub_pd(NO1,C);   
             #ifdef RandOrigin
             #ifdef SAVERAND
             REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             REMAINDER = _mm512_load_pd(rder+(i+3)*8);  
             #endif //endif saverand
             #else
             REMAINDER = _mm512_load_pd(rder+(subi+3)*8);   
             #endif
             __m512d D= binomialVec(N,P2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             //NRYRO1 = _mm512_sub_pd(NRYRO1,C);   
             //NRYRO1 = _mm512_sub_pd(NRYRO1,D);
             NO1 = _mm512_sub_pd(NO1,C);
             NO1 = _mm512_sub_pd(NO1,D);
             NRYRO1 = _mm512_add_pd(NRYRO1,NO1);    
             NRYRC1 = _mm512_add_pd(NRYRC1,C); 
             NRYRO2 = _mm512_add_pd(NRYRO2,D);  
             
                                      
         }

	

        __m512d NC2 = _mm512_load_pd(Unit_NryrC2+i);
         //NRYRC2 = NC2;  
         //__m512d PC2C1 = _mm512_load_pd(kC2C1+i);
         __m512d PC2C1 = KC2C1;
         
         #ifdef RandOrigin
         #ifdef SAVERAND
         __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
         randomUsed+=8;
         #else
         __m512d REMAINDER = _mm512_load_pd(rder+(i+4)*8); 
         #endif  //endif save rand
         #else
         __m512d REMAINDER = _mm512_load_pd(rder+(subi+4)*8);  
         #endif 
         __m512d C= binomialVec(NC2,PC2C1,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

         //__m512d PC2O2 = _mm512_load_pd(kC2O2+i);
         __m512d PC2O2 = KC2O2;
         
         PC2O2 = _mm512_div_pd(PC2O2,_mm512_sub_pd(ONE,PC2C1)); 

         __m512d N = _mm512_sub_pd(NC2,C);   
         #ifdef RandOrigin
         #ifdef SAVERAND
         REMAINDER = _mm512_load_pd(rder+randomUsed);
         randomUsed+=8;
         #else
         REMAINDER = _mm512_load_pd(rder+(i+5)*8);
         #endif //endif save rand
         #else    
         REMAINDER = _mm512_load_pd(rder+(subi+5)*8);  
         #endif
         __m512d D= binomialVec(N,PC2O2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

         //NRYRC2 = _mm512_sub_pd(NRYRC2,C);   
         //NRYRC2 = _mm512_sub_pd(NRYRC2,D);  
         NC2 = _mm512_sub_pd(NC2,C);
         NC2 = _mm512_sub_pd(NC2,D);
         NRYRC2 = _mm512_add_pd(NRYRC2,NC2);  
         NRYRC1 = _mm512_add_pd(NRYRC1,C); 
         NRYRO2 = _mm512_add_pd(NRYRO2,D);
         
         


	

        __m512d NO2 = _mm512_load_pd(Unit_NryrO2+i);
        mask = _mm512_cmp_pd_mask(NO2,ONE,_MM_CMPINT_LT);
        //if(_mm512_cmp_pd_mask(NO2,ONE,_CMP_LT_OQ))
        if(mask!=255)
        {                    
                       
             __m512d P = _mm512_set1_pd(0.5*dt);  
             #ifdef RandOrigin   
             #ifdef SAVERAND
             __m512d REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             __m512d REMAINDER = _mm512_load_pd(rder+(i+6)*8); 
             #endif //endif save rand 
             #else 
             __m512d REMAINDER = _mm512_load_pd(rder+(subi+6)*8); 
             #endif 
             
             #ifdef ConstBinomial 
             __m512d C= constp_binomialVec(NO2,REMAINDER,B,I,CONST5,CONST3);
             #else
             __m512d C= binomialVec(NO2,P,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);
             #endif
             
             //load kO2O1 the same as kC2C1
             //__m512d P2 = _mm512_load_pd(kC2C1+i);
             __m512d P2 = KC2C1;
             
             P2 = _mm512_div_pd(P2,_mm512_sub_pd(ONE,P));
             
             __m512d N = _mm512_sub_pd(NO2,C);   
             #ifdef RandOrigin
             #ifdef SAVERAND
             REMAINDER = _mm512_load_pd(rder+randomUsed);
             randomUsed+=8;
             #else
             REMAINDER = _mm512_load_pd(rder+(i+7)*8); 
             #endif  //endif saverand
             #else  
             REMAINDER = _mm512_load_pd(rder+(subi+7)*8); 
             #endif   
             __m512d D= binomialVec(N,P2,REMAINDER,T,ZT,CONST,CONST1,CONST2,CONST3,CONST4,CONST5);

             NO2 = _mm512_sub_pd(NO2,C);   
             NO2 = _mm512_sub_pd(NO2,D); 
             NRYRO2 = _mm512_add_pd(NRYRO2,NO2); 
             NRYRC2 = _mm512_add_pd(NRYRC2,C); 
             NRYRO1 = _mm512_add_pd(NRYRO1,D);   
         }
         _mm512_store_pd(Unit_NryrC1+i,NRYRC1);
         _mm512_store_pd(Unit_NryrO1+i,NRYRO1);
         _mm512_store_pd(Unit_NryrC2+i,NRYRC2);
         _mm512_store_pd(Unit_NryrO2+i,NRYRO2);
        
    }
    #ifdef OPENMPONCELL
    #ifdef SAVERAND
        Cell.RandUsedN+=randomUsed;
    #endif
    #endif

}






//in:dyadInnerVars,Unit_NryrO1[],jlca_dyad[],
//out:Cell_Poryr,
//inout:dyadOuterVars(reduction vars),Unit_cajsr,Unit_cads,Unit_cass,Unit_cacyt,Unit_cansr,Cell_CaMKb,Cell_CaMKt,Cell_CaMKa,
#ifdef OPENMPONDYAD
__attribute__((always_inline)) inline void dyadloop7ConcCOMP(DyadOuterVars* dyadOuterVars,DyadInnerVars *dyadInnerVars,double *Unit_NryrO1,double *jlca_dyad,double *Cell_Poryr,double *Unit_cajsr,double *Unit_cads,double *Unit_cass,double *Unit_cacyt,double *Unit_cansr,double *Cell_CaMKb,double *Cell_CaMKt,double *Cell_CaMKa)
#endif
#ifdef OPENMPONCELL
__attribute__((always_inline)) inline void dyadloop7ConcCOMP(DyadOuterVars* dyadOuterVars,DyadInnerVars *dyadInnerVars,double *Unit_NryrO1,double *jlca_dyad,double *Cell_Poryr,double *Unit_cajsr,double *Unit_cads,double *Unit_cass,double *Unit_cacyt,double *Unit_cansr,double *Cell_CaMKb,double *Cell_CaMKt,double *Cell_CaMKa,double &jrel,double &inaca,double &inacass,double &ipca,double &icab)
#endif
{
    int Ndyads = dyadOuterVars->Ndyads;
    int Nryr = dyadOuterVars->Nryr;
    double jrelP = dyadInnerVars->jrelP;
    double dt = dyadOuterVars->dt;
    double vsjrM = dyadInnerVars->vsjrM;
    double vjsr_dyad = dyadOuterVars->vjsr_dyad;
    double vnsr_dyad = dyadOuterVars->vnsr_dyad;
    double h6 = dyadInnerVars->h6;
    double x1P = dyadInnerVars->x1P;
    double k2 = dyadInnerVars->k2;
    double k4 = dyadInnerVars->k4;
    
    double x2P = dyadInnerVars->x2P;
    double x3P = dyadInnerVars->x3P;
    double x2P2 = dyadInnerVars->x2P2;
    double x3P2 = dyadInnerVars->x3P2;
    double k1 = dyadInnerVars->k1;
    double k3 = dyadInnerVars->k3;
    double EP1 = dyadInnerVars->EP1;
    double EP2 = dyadInnerVars->EP2;
    double X4 = dyadInnerVars->X4;
    double k7 = dyadInnerVars->k7;
    double k8 = dyadInnerVars->k8;
    double k4pp = dyadInnerVars->k4pp;
    double k3pp = dyadInnerVars->k3pp;
    double cassP1 = dyadInnerVars->cassP1;
    double cassP2 = dyadInnerVars->cassP2;
    double cacytP = dyadInnerVars->cacytP;
 
    double PCabM2 = dyadInnerVars->PCabM2;
    double PCabM1 = dyadInnerVars->PCabM1;

    double kmtrpn = dyadOuterVars->kmtrpn;
    double vcyt_dyad = dyadOuterVars->vcyt_dyad;
    double vss_dyad = dyadOuterVars->vss_dyad;

    double t = dyadOuterVars->t;
    int beats = dyadOuterVars->beats;
    int bcl = dyadOuterVars->bcl;

    //double jrel = 0.0,inaca=0.0,inacass=0.0,ipca=0.0,icab=0.0;

    double Gncx=0.0008;
    if (t>(beats-8)*bcl && t<(beats-8)*bcl + 500) Gncx = 3*Gncx;

    double const1,const2,const3,const4;
    if (iso == 1)
    {
        const1 = 0.004375;        const2 = 0.54*0.00092;
        const3 = 2.75*0.004375;   const4 = 0.54*0.00075;
    }
    else
    {
        const1 = 0.004375;        const2 = 0.00092;
        const3 = 0.01203125;      const4 = 0.00075;
    }


    int numThreads = dyadOuterVars->NumThreads;
    int RCT = (int)Ndyads/numThreads;
  #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE) nowait
  #endif
    #pragma ivdep
    #pragma vector aligned //nontemporal
    #pragma vector always
     for(int i=0;i<Ndyads;i++)     
     {
        double nryropen = Unit_NryrO1[i];

        //if (t> (beats-3)*bcl && t<(beats-3)*bcl+20) nryropen = Nryr;
        //if (t>(beats-5)*bcl) Dx = 0;

        Cell_Poryr[i] =  1.0*nryropen/Nryr;
        double jrel_dyad = dryr*nryropen*(Unit_cajsr[i]*1e3-Unit_cads[i]); //uM/ms      
	jrel += jrel_dyad*jrelP;         
        double jcadiff_ds_ss = (Unit_cads[i]-Unit_cass[i]*1e3)/tauefflux;
	Unit_cads[i] =  (jrel_dyad + jlca_dyad[i])*tauefflux + Unit_cass[i]*1e3;

        double	bsubspace = 1/(1 + ((bsrbar*kmbsr)/(pow((kmbsr+Unit_cass[i]),2))) + ((bslbar*kmbsl)/(pow((kmbsl+Unit_cass[i]),2))));
        double jcadiff_ss_cyt = (Unit_cass[i]-Unit_cacyt[i])/taudiff;
        double jrefill = (Unit_cansr[i] - Unit_cajsr[i])/taurefill;

        double	bjsr = 1/( 1 + (csqnbar*kmcsqn)/pow((kmcsqn + Unit_cajsr[i]),2));
	Unit_cajsr[i] = Unit_cajsr[i] + bjsr*dt*(jrefill-jrel_dyad*vsjrM);//////////////////////////////////////////////////////555555555555:

        double Jupnp;
        double Jupp;      
        Jupnp=const1*Unit_cacyt[i]/(Unit_cacyt[i]+const2);
        Jupp=const3*Unit_cacyt[i]/(Unit_cacyt[i]+const4);
        
        double fJupp=(1.0/(1.0+KmCaMK/Cell_CaMKa[i]));
        double ileak=0.0039375*Unit_cansr[i]/15.0;
        double iup=((1.0-fJupp)*Jupnp+fJupp*Jupp);

        Unit_cansr[i] = Unit_cansr[i] + (iup - ileak - jrefill*(vjsr_dyad/vnsr_dyad))*dt;

        // local CaMK bound, active and trapped concentration
        Cell_CaMKb[i] = CaMKo*(1.0-Cell_CaMKt[i] )/(1.0+KmCaM/Unit_cass[i]);
        Cell_CaMKa[i] = Cell_CaMKb[i] + Cell_CaMKt[i];
        Cell_CaMKt[i] += dt*(aCaMK*Cell_CaMKb[i]*(Cell_CaMKb[i]+Cell_CaMKt[i])-bCaMK*Cell_CaMKt[i]);//move 

        double icab_dyad=0,ipca_dyad=0,inaca_dyad=0,inacass_dyad;//move to top

        /////////////////////////////%%%%%%%%%%%%%%%%%%8.3.1comp_ica_dyad()%%%%%%%%%%%%%%%%%/////////////
       // calcium currents 
        double k6=h6*Unit_cacyt[i]*kcaon;

	double x1=x1P + k2*k4*k6;
        double x2=x2P + x2P2*k6;
        double x3=x3P2+k1*k3*k6+ x3P*k6;

        double Edenom=k6*EP1+EP2;
        double allo=1.0/(1.0+pow(KmCaAct/Unit_cacyt[i],2));

	double JncxNa=(3.0*(X4*k7-x1*k8)+x3*k4pp-x2*k3pp)/Edenom;
        double JncxCa=(x2*k2-x1*k1)/Edenom;

        
        
        inaca_dyad=0.8*Gncx*allo*(zna*JncxNa+zca*JncxCa);
        k6=h6*Unit_cass[i]*kcaon;

	x1=x1P + k2*k4*k6;
        x2=x2P + x2P2*k6;
        x3=x3P2+k1*k3*k6+ x3P*k6;

	Edenom=k6*EP1+EP2;

        allo=1.0/(1.0+pow(KmCaAct/Unit_cass[i],2));
 
	JncxNa=(3.0*(X4*k7-x1*k8)+x3*k4pp-x2*k3pp)/Edenom;
        JncxCa=(x2*k2-x1*k1)/Edenom;

        inacass_dyad=0.2*Gncx*allo*(zna*JncxNa+zca*JncxCa);
 
        Unit_cass[i] = Unit_cass[i] + (-bsubspace*(((-2*inacass_dyad)*cassP1) + jcadiff_ss_cyt - jcadiff_ds_ss*cassP2 ))*dt;//moved here

        icab_dyad=  Unit_cacyt[i]*PCabM2-PCabM1;//////////////////////////////////////////////////////////////////////////66666666666:from 64.54 to 63.48

        double GpCa = 0.0005;
        ipca_dyad=GpCa*Unit_cacyt[i]/(0.0005+Unit_cacyt[i]);

        inaca += inaca_dyad;
        inacass += inacass_dyad;
        ipca += ipca_dyad;
        icab += icab_dyad;

        /////////////////////////////%%%%%%%%%%%%%%%%%%8.3.1comp_ica_dyad()%%%%%%%%%%%%%%%%%/////////////
    	double bmyo;
        bmyo = 1/(1 + ((trpnbar*kmtrpn)/(pow((kmtrpn+Unit_cacyt[i]),2))) + ((cmdnbar*kmcmdn)/(pow((kmcmdn+Unit_cacyt[i]),2))));
        // local myoplasmic Ca concentration     
        Unit_cacyt[i] = Unit_cacyt[i] + (-bmyo*(((icab_dyad+ipca_dyad-2*inaca_dyad)*cacytP) + (iup-ileak)*(vnsr_dyad/vcyt_dyad) - jcadiff_ss_cyt*(vss_dyad/vcyt_dyad) ))*dt;
    }
   
}


#ifdef CPU
void comp_cell(struct cell &Cell, int ix, int iy, int iz,int offset_x, int offset_z,double t, double ist, int bcl, double ***v,double dtstep,int Ndhpr , int Ntotdyads,int Nryr,double **T,double **B,double *ZT,int icounter,int steps,double TT)
#endif
#ifdef PHI
void comp_cell(struct cell &Cell, int I,int ix, int iy, int iz,int offset_x, int offset_z,double t, double ist, int bcl, double *v,double dtstep,int Ndhpr , int Ntotdyads,int Nryr,double **T,double **B,double *ZT,int icounter,int steps)
#endif

{


#ifdef TIME
    double T_Start = timing();
#endif
    

    double gna = 75;
    double mtau,hftau,hstau,hsptau,jtau,jptau,Ahf,Ahs;
    double mss,hss,jss,hssp;
    double h,hp,finap;

    double mlss,mltau,hlss,hlssp,hltau,hlptau;

    double gnal,finalp;
    double naiont=0,caiont,kiont=0,it,istim;
    double ina,inal,INab;
    double INaK,Ito,IKr,IKs,IK1,IKb;
    double jrel1,ilca1,ilcana1,ilcak1,inaca1,icab1,ipca1,inacass1;
    //double CaMKb_cell,CaMKt_cell;
    double nsr, jsr, cadyad=0.1, casub,cai;
    double ena;

    double kmtrpn = 0.0005;///

    //added 
    if (iso == 1) kmtrpn = 1.6*kmtrpn;


    double	ageo = 2*pi*ar*ar + 2*pi*ar*al;///
    double	acap = 2*ageo;///
    double vcell = 1000*pi*ar*ar*al;///
    double	vmyo = 0.68*vcell;////
    

    double	vnsr = 0.0552*vcell;///
    double	vjsr = 0.0048*vcell;///
    double	vss = 0.01*vcell;///
    double	vjsr_dyad = vjsr/Ntotdyads;///
    double	vnsr_dyad = vnsr/Ntotdyads;///
    double	vcyt_dyad = vmyo/Ntotdyads;///
    double	vss_dyad = vss/Ntotdyads;///


    double PCa=0.0001;
    if (iso == 1) PCa = 2.5*PCa;
    double PCap=1.1*PCa;
    double PCaNa=0.00125*PCa;
    double PCaK=3.574e-4*PCa;
    double PCaNap=0.00125*PCap;
    double PCaKp=3.574e-4*PCap;

    #ifdef PHI 
    double voltage = v[I];
    #endif
    
    #ifdef CPU
    double voltage = v[ix][iy][iz];
    #endif

    double vffrt = voltage*frdy*frdy/(R*temp);
    double vfrt = voltage*frdy/(R*temp);

    double dt = dtstep;

    ic = icounter;
#ifdef TIME
    double T_start,T_end;
#endif

#ifdef TIME
    T_start = timing();
#endif
//########################1.comp_ina() below########################//
//comp_ina( iso, gna,mtau,hftau,hstau,hsptau,jtau,jptau,Ahf,Ahs,mss,hss,hssp,jss,h,hp,finap,mlss,mltau,hlss,hlssp,hltau,hlptau,gnal,finalp,ena,ina,inal,v,ix,iy,iz,dt,Cell);
    if (iso == 1) gna = 2.7*gna;
    mtau = 1.0/(6.765*exp((voltage+11.64)/34.77)+8.552*exp(-(voltage+77.42)/5.955));
    hftau=1.0/(1.432e-5*exp(-(voltage+1.196)/6.285)+6.149*exp((voltage+0.5096)/20.27));
    hstau=1.0/(0.009794*exp(-(voltage+17.95)/28.05)+0.3343*exp((voltage+5.730)/56.66));
    hsptau=3.0*hstau;
    jtau=2.038+1.0/(0.02136*exp(-(voltage+100.6)/8.281)+0.3052*exp((voltage+0.9941)/38.45));
    jptau=1.46*jtau;
    Ahf=0.99;
    Ahs=1.0-Ahf;
    mss = 1.0/(1.0+exp((-(voltage+44.57))/9.871));


    hss =1.0/(1+exp((voltage+82.90)/6.086));
    hssp=1.0/(1+exp((voltage+89.1)/6.086));
    if (iso == 1)
    {
        hss =1.0/(1+exp((voltage+82.90+5)/6.086));
        hssp=1.0/(1+exp((voltage+89.1+5)/6.086));
    }
    jss = hss;

    Cell.m = mss-(mss-Cell.m)*exp(-dt/mtau);
    Cell.hf=hss-(hss-Cell.hf)*exp(-dt/hftau);
    Cell.hs=hss-(hss-Cell.hs)*exp(-dt/hstau);
    Cell.hsp=hssp-(hssp-Cell.hsp)*exp(-dt/hsptau);

    h = Ahf*Cell.hf+Ahs*Cell.hs;
    hp=Ahf*Cell.hf+Ahs*Cell.hsp;
    Cell.j = jss-(jss-Cell.j)*exp(-dt/jtau);
    Cell.jp=jss-(jss-Cell.jp)*exp(-dt/jptau);

    finap=(1.0/(1.0+KmCaMK/Cell.CaMKa_cell));

    mlss=1.0/(1.0+exp((-(voltage+42.85))/5.264));
    mltau = mtau;
    Cell.ml=mlss-(mlss-Cell.ml)*exp(-dt/mltau);

    hlss=1.0/(1.0+exp((voltage+87.61)/7.488));
    hlssp=1.0/(1.0+exp((voltage+93.81)/7.488));
    hltau=200.0;
    hlptau=3.0*hltau;
    Cell.hl=hlss-(hlss-Cell.hl)*exp(-dt/hltau);
    Cell.hlp=hlssp-(hlssp-Cell.hlp)*exp(-dt/hlptau);

    gnal = 0.0075;
    finalp = finap;

    ena = ((R*temp)/frdy)*log(nao/Cell.nai);

    ina=gna*(voltage-ena)*Cell.m*Cell.m*Cell.m*((1.0-finap)*h*Cell.j+finap*hp*Cell.jp);
    inal=gnal*(voltage-ena)*Cell.ml*((1.0-finalp)*Cell.hl+finalp*Cell.hlp);
    //########################1.comp_ina() below########################//
#ifdef TIME
    T_end = timing();
    Cell.T_ina += T_end-T_start;
#endif


#ifdef TIME
    T_start = timing();
#endif
    //######################2.comp_ito() below########################//
    double EK;
    EK=(R*temp/frdy)*log(ko/Cell.ki);//move above because of EK

    double ass;
    ass=1.0/(1.0+exp((-(voltage-14.34))/14.82));
    double atau;
    atau=1.0515/(1.0/(1.2089*(1.0+exp(-(voltage-18.4099)/29.3814)))+3.5/(1.0+exp((voltage+100.0)/29.3814)));

    Cell.a=ass-(ass-Cell.a)*exp(-dt/atau);

    double iss;
    iss=1.0/(1.0+exp((voltage+43.94)/5.711));
    double delta_epi=1.0;
    double iftau,istau;

    iftau=4.562+1/(0.3933*exp((-(voltage+100.0))/100.0)+0.08004*exp((voltage+50.0)/16.59));
    istau=23.62+1/(0.001416*exp((-(voltage+96.52))/59.05)+1.780e-8*exp((voltage+114.1)/8.079));

    iftau*=delta_epi;
    istau*=delta_epi;

    double AiF,AiS;
    AiF=1.0/(1.0+exp((voltage-213.6)/151.2));
    AiS=1.0-AiF;

    Cell.iF=iss-(iss-Cell.iF)*exp(-dt/iftau);
    Cell.iS=iss-(iss-Cell.iS)*exp(-dt/istau);

    double i_ito;
    i_ito=AiF*Cell.iF+AiS*Cell.iS;

    double assp;
    assp=1.0/(1.0+exp((-(voltage-24.34))/14.82));
    Cell.ap=assp-(assp-Cell.ap)*exp(-dt/atau);

    double dti_develop,dti_recover;
    dti_develop=1.354+1.0e-4/(exp((voltage-167.4)/15.89)+exp(-(voltage-12.23)/0.2154));
    dti_recover=1.0-0.5/(1.0+exp((voltage+70.0)/20.0));

    double tiFp,tiSp;
    tiFp=dti_develop*dti_recover*iftau;
    tiSp=dti_develop*dti_recover*istau;

    Cell.iFp=iss-(iss-Cell.iFp)*exp(-dt/tiFp);
    Cell.iSp=iss-(iss-Cell.iSp)*exp(-dt/tiSp);

    double ip;
    ip=AiF*Cell.iFp+AiS*Cell.iSp;

    double Gto = 0.02;
    double fItop;
    fItop=(1.0/(1.0+KmCaMK/Cell.CaMKa_cell));

    Ito=Gto*(voltage-EK)*((1.0-fItop)*Cell.a*i_ito+fItop*Cell.ap*ip);
    //######################2.comp_ito() below########################//
#ifdef TIME
    T_end = timing();
    Cell.T_ito += T_end-T_start;
#endif

#ifdef TIME
    T_start = timing();
#endif
 //############################3.comp_ikr()#########################//
    double xrss=1.0/(1.0+exp((-(voltage+8.337))/6.789));
    double txrf=12.98+1.0/(0.3652*exp((voltage-31.66)/3.869)+4.123e-5*exp((-(voltage-47.78))/20.38));
    double txrs=1.865+1.0/(0.06629*exp((voltage-34.70)/7.355)+1.128e-5*exp((-(voltage-29.74))/25.94));
    double Axrf=1.0/(1.0+exp((voltage+54.81)/38.21));
    double Axrs=1.0-Axrf;
    Cell.xrf=xrss-(xrss-Cell.xrf)*exp(-dt/txrf);
    Cell.xrs=xrss-(xrss-Cell.xrs)*exp(-dt/txrs);
    double xr=Axrf*Cell.xrf+Axrs*Cell.xrs;
    double rkr=1.0/(1.0+exp((voltage+55.0)/75.0))*1.0/(1.0+exp((voltage-10.0)/30.0));
    double GKr=0.046; GKr = 1*GKr;

    IKr=GKr*sqrt(ko/5.4)*xr*rkr*(voltage-EK);
    //###############################3.comp_ikr()#######################//
#ifdef TIME
    T_end = timing();
    Cell.T_ikr+=T_end-T_start;
#endif

#ifdef TIME
    T_start = timing();
#endif
//#########################4.comp_iks()##########################//
    double EKs=(R*temp/frdy)*log((ko+0.01833*nao)/(Cell.ki+0.01833*Cell.nai));
    double xs1ss=1.0/(1.0+exp((-(voltage+11.60))/8.932));
    double txs1=817.3+1.0/(2.326e-4*exp((voltage+48.28)/17.80)+0.001292*exp((-(voltage+210.0))/230.0));
    Cell.xs1=xs1ss-(xs1ss-Cell.xs1)*exp(-dt/txs1);
    double xs2ss=xs1ss;
    double txs2=1.0/(0.01*exp((voltage-50.0)/20.0)+0.0193*exp((-(voltage+66.54))/31.0));
    Cell.xs2=xs2ss-(xs2ss-Cell.xs2)*exp(-dt/txs2);
    double KsCa=1.0+0.6/(1.0+pow(3.8e-5/3.8e-5,1.4));
    double GKs=0.0034;
    if (iso == 1) GKs = 3.2*GKs;

    IKs=GKs*KsCa*Cell.xs1*Cell.xs2*(voltage-EKs);
    //#########################4.comp_iks()##########################//
#ifdef TIME
    T_end = timing();
    Cell.T_iks+=T_end-T_start;
#endif

#ifdef TIME
    T_start = timing();
#endif
//###############################5.comp_ik1()#######################//
    double xk1ss=1.0/(1.0+exp(-(voltage+2.5538*ko+144.59)/(1.5692*ko+3.8115)));
    double txk1=122.2/(exp((-(voltage+127.2))/20.36)+exp((voltage+236.8)/69.33));
    Cell.xk1=xk1ss-(xk1ss-Cell.xk1)*exp(-dt/txk1);
    double rk1=1.0/(1.0+exp((voltage+105.8-2.6*ko)/9.493));
    double GK1=0.1908;
//if (iso == 1)
 GK1 = 0.4*0.1908;
    IK1=GK1*sqrt(ko)*rk1*Cell.xk1*(voltage-EK);
    //###############################5.comp_ik1()#######################//
#ifdef TIME
    T_end = timing();
    Cell.T_ik1+=T_end-T_start;
#endif

    
#ifdef TIME
    T_start = timing();
#endif
//################################6.comp_inak()####################//

    double Knai=Knai0*exp((delta*voltage*frdy)/(3.0*R*temp));
    if (iso == 1) Knai = 0.7*Knai;
    double Knao=Knao0*exp(((1.0-delta)*voltage*frdy)/(3.0*R*temp));

    double P=eP/(1.0+H/Khp+Cell.nai/Knap+Cell.ki/Kxkur);
    double a1=(k1p*pow(Cell.nai/Knai,3.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
    double b1=k1m*MgADP;
    double a2=k2p;
    double b2=(k2m*pow(nao/Knao,3.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
    double a3=(k3p*pow(ko/Kko,2.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
    double b3=(k3m*P*H)/(1.0+MgATP/Kmgatp);
    double a4=(k4p*MgATP/Kmgatp)/(1.0+MgATP/Kmgatp);
    double b4=(k4m*pow(Cell.ki/Kki,2.0))/(pow(1.0+Cell.nai/Knai,3.0)+pow(1.0+Cell.ki/Kki,2.0)-1.0);
    double x1=a4*a1*a2+b2*b4*b3+a2*b4*b3+b3*a1*a2;
    double x2=b2*b1*b4+a1*a2*a3+a3*b1*b4+a2*a3*b4;
    double x3=a2*a3*a4+b3*b2*b1+b2*b1*a4+a3*a4*b1;
    double x4=b4*b3*b2+a3*a4*a1+b2*a4*a1+b3*b2*a1;
    double E1=x1/(x1+x2+x3+x4);
    double E2=x2/(x1+x2+x3+x4);
    double E3=x3/(x1+x2+x3+x4);
    double E4=x4/(x1+x2+x3+x4);
    //double zk=1.0;
    double JnakNa=3.0*(E1*a3-E2*b3);
    double JnakK=2.0*(E4*b1-E3*a1);
    //double Pnak=30;

    INaK=Pnak*(zna*JnakNa+zk*JnakK);
    //################################6.comp_inak()####################//
#ifdef TIME
    T_end = timing();
    Cell.T_inak += T_end-T_start;
#endif


#ifdef TIME
    T_start = timing();
#endif
    //#################################7.comp_inab()######################//
    double xkb=1.0/(1.0+exp(-(voltage-14.48)/18.34));
    double GKb=0.003;
  /*  if (iso == 1)///?
         GKb = 2.5*GKb;////?*/

    IKb=GKb*xkb*(voltage-EK);

    double PNab=3.75e-10;
    INab=PNab*vffrt*(Cell.nai*exp(vfrt)-nao)/(exp(vfrt)-1.0);
    //#################################7.comp_inab()######################//
#ifdef TIME
    T_end = timing();
    Cell.T_inab += T_end-T_start;
#endif


#ifdef TIME
    T_start = timing();
#endif
    //############################8.comp_calc_dyad()#####################################///
    //comp_calc_dyad(jrel1,ilca1,ilcana1,ilcak1,inaca1,icab1,ipca1,inacass1,cai,casub,cadyad,jsr,nsr,voltage, ix, iy, iz, dt, PCa, PCap, PCaNa, PCaK, PCaNap, PCaKp, vffrt, vfrt, acap, Ntotdyads, vss_dyad, vcyt_dyad, Ndhpr, T, ZT, B, Nryr, t, bcl, vmyo, vjsr_dyad, vnsr_dyad, kmtrpn,Cell, icounter, steps);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    int Nx = Cell.unitNum_x;
    int Ny = Cell.unitNum_y;
    int Nz = Cell.unitNum_z;
    int Ndyads = Cell.Ndyads;

    int Nx_diff = (Nx-1)*Nvox + 1;
    int Ny_diff = (Ny-1)*Nvox + 1;
    int Nz_diff = (Nz-1)*Nvox + 1;
    
    double *casstemp = Cell.casstemp;
    double *cansrtemp = Cell.cansrtemp;
    double *cacyttemp = Cell.cacyttemp;
    double *cassdiff;
    double *cansrdiff;
    double *cacytdiff;

    

    int Cell_LtypeRandN = Cell.LtypeRandN;
    double *Cell_nca = Cell.nca;
    double *Cell_CaMKa = Cell.CaMKa;
    int *Cell_nactive = Cell.nactive;//can be deleted
    double *Cell_trel = Cell.trel;
    double *Cell_Poryr = Cell.Poryr;//can be deleted
    double *Cell_CaMKb = Cell.CaMKb;
    double *Cell_CaMKt = Cell.CaMKt;
    
    unit *unitS = Cell.unitsInCell;
    double *Unit_cacyt = Cell.cacyt;
    double *Unit_cass = Cell.cass;
    double *Unit_cansr = Cell.cansr;
    double *Unit_cajsr = Cell.cajsr;
    double *Unit_cads = Cell.cads;

    double *Unit_NryrO1 = Cell.NryrO1;
    double *Unit_NryrO2 = Cell.NryrO2;
    double *Unit_NryrC1 = Cell.NryrC1;
    double *Unit_NryrC2 = Cell.NryrC2;

    double *Tt = T[0];
    double *Bb = B[0];

    
    double nodhpr,noryr,nmodeca_total,nivf_total,nivs_total;
    double factivedyad;

#ifdef OPENMPONCELL
    double  jrel,ilca,ilcana,ilcak,inaca,icab,ipca,inacass;
#endif
    
    ilca = nodhpr = jrel = noryr = nmodeca_total = nivf_total = nivs_total = 0;
    ilcana=ilcak=0;
    inaca = icab = ipca = inacass = 0;
    //double cai,casub,cadyad,jsr,nsr;
    double CCaMKa_cell;
    cai = casub = cadyad = jsr = nsr = CCaMKa_cell = 0;
    //Cell.CaMKa_cell = CaMKb_cell = CaMKt_cell = 0;
    Cell.CaMKa_cell =  0;
    factivedyad  = 0;
    //added by qianglan to generate random data using intel MKL lib
    double *l_Random = Cell.l_Random;
    unsigned long rand_n = Cell.rand_n;

    ////////////////////////////move from inside of dyad loop
    PCa=0.0001;
    if (iso == 1) PCa = 2.5*PCa;
    PCap=1.1*PCa;
    PCaNa=0.00125*PCa;
    PCaK=3.574e-4*PCa;
    PCaNap=0.00125*PCap;
    PCaKp=3.574e-4*PCap;//step 1:this part no improvment

//#ifdef TIME
    double T_OStart,T_IStart,T_Oend,T_Iend;
//#endif

#ifdef TIME
    T_OStart = timing();
#endif




#ifdef OPENMPONDYAD
  #ifdef parallerandom
      //omp_set_num_threads(NUMTHREADS);
      #pragma omp parallel 
      {
          int tid = omp_get_thread_num();
          int rand_n_size = rand_n/omp_get_num_threads()+1;
          int rand_n_mod = rand_n_size;
          if (tid==omp_get_num_threads()-1)
              rand_n_mod=rand_n-(omp_get_num_threads()-1)*rand_n_size;
          #ifdef SAVERAND
          vdRngUniform(0, Cell.RandomMultistream[tid], rand_n_mod-Cell.RandUsedN[tid], l_Random+(tid*rand_n_size), 0.0, 1.0-(1.2e-12));  //////Make sure access l_Random data aligned by threads
          Cell.RandUsedN[tid] = 0;//this seem not correct  
          #else
          vdRngUniform(0, Cell.RandomMultistream[tid], rand_n_mod, l_Random+(tid*rand_n_size), 0.0, 1.0-(1.2e-12));
          #endif
        
      }
   #else
       vdRngUniform(0, Cell.RandomMultistream[0], rand_n, l_Random, 0.0, 1.0-(1.2e-12));
   #endif

#else
       #ifdef RandOrigin
       #ifdef SAVERAND
          vdRngUniform(0, Cell.Randomstream, Cell.RandUsedN, l_Random, 0.0, 1.0-(1.2e-12));
          Cell.RandUsedN = 0;
       #else
          vdRngUniform(0, Cell.Randomstream, rand_n, l_Random, 0.0, 1.0-(1.2e-12));
       #endif //endif of saverand
       #endif
#endif

#ifdef TIME
    T_Oend = timing();
    Cell.T_randomG += T_Oend-T_OStart;
#endif

//#ifdef TIME
//   T_OStart = timing();
//#endif	

    //DyadOuterVars *dyadOuterVars = (DyadOuterVars *)_mm_malloc(sizeof(struct DyadOuterVars),64);
    DyadOuterVars *dyadOuterVars = Cell.dyadOuterVPtr;
    dyadOuterVars->Ndyads = Ndyads;
    dyadOuterVars->randU = Cell.randU;
    dyadOuterVars->Ndhpr = Ndhpr;
    dyadOuterVars->PCa = PCa;
    dyadOuterVars->PCaNa = PCaNa;
    dyadOuterVars->PCaNap = PCaNap;
    dyadOuterVars->PCaK = PCaK;
    dyadOuterVars->PCaKp = PCaKp;
    dyadOuterVars->ilca = ilca;
    dyadOuterVars->ilcana = ilcana;
    dyadOuterVars->ilcak = ilcak; 
    dyadOuterVars->Nryr = Nryr;
    dyadOuterVars->t = t;
    dyadOuterVars->beats = Cell.beats;
    dyadOuterVars->bcl = bcl;
     
    dyadOuterVars->Cell_LtypeRandN = Cell_LtypeRandN;
   
    dyadOuterVars->dt = dt;
    dyadOuterVars->vjsr_dyad = vjsr_dyad;
    dyadOuterVars->vnsr_dyad = vnsr_dyad;
    dyadOuterVars->kmtrpn = kmtrpn;
    dyadOuterVars->vcyt_dyad = vcyt_dyad;
    dyadOuterVars->jrel = jrel;
    dyadOuterVars->inaca = inaca;
    dyadOuterVars->inacass = inacass;
    dyadOuterVars->ipca = ipca;
    dyadOuterVars->icab = icab;
    dyadOuterVars->voltage = voltage;
    dyadOuterVars->vmyo = vmyo;
    dyadOuterVars->vss_dyad = vss_dyad;
    dyadOuterVars->Ntotdyads = Ntotdyads;
    dyadOuterVars->acap = acap;
    dyadOuterVars->vfrt = vfrt;
    dyadOuterVars->vffrt = vffrt;
    dyadOuterVars->PCap = PCap;

    
    //dyadCOMP(dyadOuterVars,Tt,ZT,Bb,Cell,Cell_nca,Cell_CaMKa,Cell_nactive,Cell_trel,Cell_Poryr,Cell_CaMKb,Cell_CaMKt,unitS,Unit_cacyt,Unit_cass,Unit_cansr,Unit_cajsr, Unit_cads, l_Random,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #ifdef ITIME
    double T_OStart,T_IStart,T_Oend,T_Iend;
#endif
    //double  noryr,jrel,ilca,ilcana,ilcak,inaca,icab,ipca,inacass;
    //ilca  = jrel = noryr = 0;
    //ilcana=ilcak=0;
    //inaca = icab = ipca = inacass = 0;


    int beats = Cell.beats;
    int randU = Cell.randU;
    
    double hca=exp((qca*(dyadOuterVars->voltage)*frdy)/(R*temp));
    double hna=exp((qna*(dyadOuterVars->voltage)*frdy)/(R*temp));
    double h1=1+Cell.nai/kna3*(1+hna);
    double h2=(Cell.nai*hna)/(kna3*h1);
    double h3=1.0/h1;
    double h4=1.0+Cell.nai/kna1*(1+Cell.nai/kna2);
    double h5=Cell.nai*Cell.nai/(h4*kna1*kna2);
    double h6=1.0/h4;
    double h7=1.0+nao/kna3*(1.0+1.0/hna);
    double h8=nao/(kna3*hna*h7);
    double h9=1.0/h7;
    double h10=kasymm+1.0+nao/kna1*(1.0+nao/kna2);
    double h11=nao*nao/(h10*kna1*kna2);
    double h12=1.0/h10;
    double k1=h12*cao*kcaon;
    double k2=kcaoff;
    double K3p=h9*wca;//rename k3p to K3p
    double k3pp=h8*wnaca;
    double k3=K3p+k3pp;//replace k3p to K3p
    double K4p=h3*wca/hca;//rename k4p to K4p
    double k4pp=h2*wnaca;
    double k4=K4p+k4pp;//replace k4p to K4p
    double k5=kcaoff;
    double k7=h5*h2*wna;
    double k8=h8*h11*wna;
    double X4=k2*k8*(k4+k5)+k3*k5*(k1+k8);//rename x4 to X4
    double km2n=Cell.jca;
    double KKnmn = k2n/km2n;
    double Kmndt = km2n*(dyadOuterVars->dt);
    double if1,if2,if3,if4,if5,if6,if7,if8;
    if2 = Cell.d*Cell.f;
    if3 = Cell.d*Cell.jca*Cell.fca-Cell.d*Cell.f;
    if4 = Cell.d*Cell.fp;
    if5 = Cell.d*Cell.jca*Cell.fcap-Cell.d*Cell.fp;

    double PhiCaNa=1.0*(dyadOuterVars->vffrt)*(0.75*Cell.nai*exp(1.0*(dyadOuterVars->vfrt))-0.75*nao)/(exp(1.0*(dyadOuterVars->vfrt))-1.0)/(dyadOuterVars->Ndhpr);
    double PhiCaK=1.0*(dyadOuterVars->vffrt)*(0.75*Cell.ki*exp(1.0*(dyadOuterVars->vfrt))-0.75*ko)/(exp(1.0*(dyadOuterVars->vfrt))-1.0)/(dyadOuterVars->Ndhpr);

   double caoM = 0.341*cao;
   double PhiCaLP2 = 1e-3*0.01*exp(2.0*(dyadOuterVars->vfrt));
   double PhiCaLP1 = 4.0*(dyadOuterVars->vffrt)/(exp(2.0*(dyadOuterVars->vfrt))-1.0);//step3: from 84s to 83s

    double jlca_dyadP = -(dyadOuterVars->acap)*1e3/(2*vds*frdy*(dyadOuterVars->Ntotdyads));

    double cassP1 = (dyadOuterVars->acap)/((dyadOuterVars->Ntotdyads)*zca*frdy*(dyadOuterVars->vss_dyad));
    double cacytP = (dyadOuterVars->acap)/((dyadOuterVars->Ntotdyads)*zca*frdy*(dyadOuterVars->vcyt_dyad)); 
    double cassP2 = 1e-3*(vds/(dyadOuterVars->vss_dyad));

    double x1P=k2*k4*k7+k5*k7*(k2+k3);
    double x2P=k1*k7*(k4+k5); 
    double x2P2=k4*(k1+k8);
    double x3P= k8*(k2+k3);
    double x3P2= k1*k3*k7;

    double EP1= k2*k4+x2P2+k1*k3+x3P;
    double EP2= x1P+x2P+x3P2+X4;


    double expkm2n = exp(-km2n*(dyadOuterVars->dt));
    double k2nP = k2n/km2n;

    double jrelP=(vds/(dyadOuterVars->vmyo))*((dyadOuterVars->Ntotdyads)/(dyadOuterVars->Ndyads));
    double vsjrM=(vds/(dyadOuterVars->vjsr_dyad))/1000.;

    double expvfrt=exp(2.0*(dyadOuterVars->vfrt));
    double PCabM=PCab*4.0*(dyadOuterVars->vffrt)/(expvfrt-1.0);
    double PCabM1=0.341*cao*PCabM;
    double PCabM2=PCabM*expvfrt;

    double Km4=pow(Km,4);
    double kmcsqn8=pow(kmcsqn,8);
    double Kmcsqn4=pow(Kmcsqn,4);

    
    //int Ndyads = dyadOuterVars->Ndyads;

    //DyadInnerVars *dyadInnerVars = (DyadInnerVars *)_mm_malloc(sizeof(struct DyadInnerVars),64);
    DyadInnerVars *dyadInnerVars = Cell.dyadInnerVPtr;
    dyadInnerVars->k2nP = k2nP;
    dyadInnerVars->expkm2n = expkm2n;
    dyadInnerVars->if2 = if2;
    dyadInnerVars->if3 = if3;
    dyadInnerVars->if4 = if4;
    dyadInnerVars->if5 = if5;
    dyadInnerVars->PhiCaLP2 = PhiCaLP2;
    dyadInnerVars->caoM = caoM;
    dyadInnerVars->PhiCaLP1 = PhiCaLP1;
    dyadInnerVars->PhiCaNa = PhiCaNa;
    dyadInnerVars->PhiCaK = PhiCaK;  
    dyadInnerVars->jlca_dyadP = jlca_dyadP; 
    dyadInnerVars->kmcsqn8 = kmcsqn8; 
    dyadInnerVars->Kmcsqn4 = Kmcsqn4;
    dyadInnerVars->jrelP = jrelP;
    dyadInnerVars->vsjrM = vsjrM;
    dyadInnerVars->h6 = h6;
    dyadInnerVars->x1P = x1P;
    dyadInnerVars->k2 = k2;
    dyadInnerVars->k4 = k4;
    
    dyadInnerVars->x2P = x2P;
    dyadInnerVars->x3P = x3P;
    dyadInnerVars->x2P2 = x2P2;
    dyadInnerVars->x3P2 = x3P2;
    dyadInnerVars->k1 = k1;
    dyadInnerVars->k3 = k3;
    dyadInnerVars->EP1 = EP1;
    dyadInnerVars->EP2 = EP2;
    dyadInnerVars->X4 = X4;
    dyadInnerVars->k7 = k7;
    dyadInnerVars->k8 = k8;
    dyadInnerVars->k4pp = k4pp;
    dyadInnerVars->k3pp = k3pp;
    dyadInnerVars->cassP1 = cassP1;
    dyadInnerVars->cassP2 = cassP2;
    dyadInnerVars->cacytP = cacytP;  
    dyadInnerVars->Km4 = Km4;  

    dyadInnerVars->PCabM2 = PCabM2;
    dyadInnerVars->PCabM1 = PCabM1;

    
    double *ab5 = Cell.ab5;
    double *cd5 = Cell.cd5;
    double *nLCC_dyad_np = Cell.nLCC_dyad_np;
    double *nLCC_dyad_p  = Cell.nLCC_dyad_p;//memset(nLCC_dyad_p,0.0,sizeof(double)*Ndyads);
    double *jlca_dyad = Cell.jlca_dyad;
    double *tref = Cell.tref;
    double *kC1O1 = Cell.kC1O1;
    double *kC1C2 = Cell.kC1C2;
    double *kC2C1 = Cell.kC2C1;
    double *kC2O2 = Cell.kC2O2;


#ifdef OPENMPONDYAD
//omp_set_num_threads(NUMTHREADS);
#pragma omp parallel reduction(+:jrel,ilca,ilcana,ilcak,inaca,icab,ipca,inacass)
{
#endif

#ifdef OPENMPONDYAD
    double T_OStart,T_IStart,T_Oend,T_Iend;
#endif

#ifdef TIME
    double T_dyad1=0.0,T_dyad2=0.0,T_dyad3=0.0,T_dyad4=0.0,T_dyad5=0.0,T_dyad6=0.0,T_dyad7=0.0;
#endif

#ifdef OPENMPONDYAD
    #pragma omp single
    {
	dyadOuterVars->NumThreads = omp_get_num_threads();
    }
#endif
    

///////////////////////////////////////////////////////////split 1 2 3 4 5 6 7 

#ifdef NOMERGE

#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop1LtypePCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,Cell_nca,Cell_CaMKa,ab5,cd5);
#ifdef TIME
    T_Oend = timing();
    //Cell.T_ical_dyad1 += T_Oend-T_OStart;
    T_dyad1 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop2LtypeOpenCOMP(Cell,dyadOuterVars,l_Random,Tt,ZT,ab5,cd5,nLCC_dyad_np,nLCC_dyad_p);  
    
#ifdef TIME
    T_Oend = timing();
    //Cell.T_L_Channel += T_Oend-T_OStart;
    T_dyad2 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif

#ifdef OPENMPONDYAD
    dyadloop3ICALCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad);
#endif
#ifdef OPENMPONCELL
    dyadloop3ICALCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad,ilca,ilcana,ilcak);
#endif

#ifdef TIME
    T_Oend = timing();
    //Cell.T_ical_dyad += T_Oend-T_OStart;
    T_dyad3 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop4TrefCOMP(dyadOuterVars,Unit_NryrO1,Cell_nactive,Cell_trel,tref);
#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel1 += T_Oend-T_OStart;
    T_dyad4 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop5RyrPCOMP(dyadOuterVars,dyadInnerVars,Unit_cads,Unit_cajsr,tref,kC1O1,kC1C2,kC2C1,kC2O2);


#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel2 += T_Oend-T_OStart;
    T_dyad5 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
  #ifdef AVEC
    dyadloop6RyrOpenCOMP(dyadOuterVars,l_Random,Tt,ZT,Bb,kC1O1,kC1C2,kC2C1,kC2O2,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
  #endif
  #ifdef MVEC
    dyadloop6RyrOpenCOMPVec(Cell,dyadOuterVars,l_Random,Tt,ZT,Bb,kC1O1,kC1C2,kC2C1,kC2O2,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
  #endif
#ifdef TIME
    T_Oend = timing();
    //Cell.T_RyrOpen += T_Oend-T_OStart;
    T_dyad6 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif

#ifdef OPENMPONDYAD
    dyadloop7ConcCOMP(dyadOuterVars,dyadInnerVars,Unit_NryrO1,jlca_dyad,Cell_Poryr,Unit_cajsr,Unit_cads,Unit_cass,Unit_cacyt,Unit_cansr,Cell_CaMKb,Cell_CaMKt,Cell_CaMKa);
#endif
    #ifdef OPENMPONCELL
    dyadloop7ConcCOMP(dyadOuterVars,dyadInnerVars,Unit_NryrO1,jlca_dyad,Cell_Poryr,Unit_cajsr,Unit_cads,Unit_cass,Unit_cacyt,Unit_cansr,Cell_CaMKb,Cell_CaMKt,Cell_CaMKa,jrel,inaca,inacass,ipca,icab);
#endif
#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel3 += T_Oend-T_OStart;
    T_dyad7 += T_Oend-T_OStart;
#endif

#endif //endif of NOMERGE
///////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////Merge 1 2 and merge 3 4 5

#ifdef MERGE12AND345

#ifdef TIME
    T_OStart = timing();
#endif

#ifdef MERGE12
    dyadloop12LtypeCOMP(Cell,dyadInnerVars, dyadOuterVars,Unit_cads,Cell_nca,Cell_CaMKa,l_Random,T[0],ZT,nLCC_dyad_np,nLCC_dyad_p);
#else
    dyadloop1LtypePCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,Cell_nca,Cell_CaMKa,ab5,cd5);
#ifdef TIME
    T_Oend = timing();
    //Cell.T_ical_dyad1 += T_Oend-T_OStart;
    T_dyad1 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop2LtypeOpenCOMP(Cell,dyadOuterVars,l_Random,Tt,ZT,ab5,cd5,nLCC_dyad_np,nLCC_dyad_p);
#endif //endif of merge12    
    
#ifdef TIME
    T_Oend = timing();
    //Cell.T_L_Channel += T_Oend-T_OStart;
    T_dyad2 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif

#ifdef MERGE345
#ifdef OPENMPONDYAD
    dyadloop345(dyadInnerVars,dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad,Unit_NryrO1,Cell_nactive,Cell_trel,tref,Unit_cajsr,kC1O1,kC1C2,kC2C1,kC2O2);
#endif
#ifdef OPENMPONCELL
    dyadloop345(dyadInnerVars,dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad,Unit_NryrO1,Cell_nactive,Cell_trel,tref,Unit_cajsr,kC1O1,kC1C2,kC2C1,kC2O2,ilca,ilcana,ilcak);
#endif
#else
#ifdef OPENMPONDYAD
    dyadloop3ICALCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad);
#endif
#ifdef OPENMPONCELL
    dyadloop3ICALCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad,ilca,ilcana,ilcak);
#endif

#ifdef TIME
    T_Oend = timing();
    //Cell.T_ical_dyad += T_Oend-T_OStart;
    T_dyad3 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop4TrefCOMP(dyadOuterVars,Unit_NryrO1,Cell_nactive,Cell_trel,tref);
#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel1 += T_Oend-T_OStart;
    T_dyad4 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop5RyrPCOMP(dyadOuterVars,dyadInnerVars,Unit_cads,Unit_cajsr,tref,kC1O1,kC1C2,kC2C1,kC2O2);

#endif

#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel2 += T_Oend-T_OStart;
    T_dyad5 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
  #ifdef AVEC
    dyadloop6RyrOpenCOMP(dyadOuterVars,l_Random,Tt,ZT,Bb,kC1O1,kC1C2,kC2C1,kC2O2,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
  #endif
  #ifdef MVEC
    dyadloop6RyrOpenCOMPVec(Cell,dyadOuterVars,l_Random,Tt,ZT,Bb,kC1O1,kC1C2,kC2C1,kC2O2,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
  #endif
#ifdef TIME
    T_Oend = timing();
    //Cell.T_RyrOpen += T_Oend-T_OStart;
    T_dyad6 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif

#ifdef OPENMPONDYAD
    dyadloop7ConcCOMP(dyadOuterVars,dyadInnerVars,Unit_NryrO1,jlca_dyad,Cell_Poryr,Unit_cajsr,Unit_cads,Unit_cass,Unit_cacyt,Unit_cansr,Cell_CaMKb,Cell_CaMKt,Cell_CaMKa);
#endif
    #ifdef OPENMPONCELL
    dyadloop7ConcCOMP(dyadOuterVars,dyadInnerVars,Unit_NryrO1,jlca_dyad,Cell_Poryr,Unit_cajsr,Unit_cads,Unit_cass,Unit_cacyt,Unit_cansr,Cell_CaMKb,Cell_CaMKt,Cell_CaMKa,jrel,inaca,inacass,ipca,icab);
#endif
#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel3 += T_Oend-T_OStart;
    T_dyad7 += T_Oend-T_OStart;
#endif

#endif  //end if of MERGE12AND345
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////Merge 1 2 3 and merge 5 6

#ifdef MERGE123AND56




#ifdef TIME
    T_OStart = timing();
#endif

#ifdef MERGE123
#ifdef OPENMPONDYAD
dyadloop123LtypeCOMP(Cell,dyadInnerVars,dyadOuterVars,Unit_cads,Cell_nca,Cell_CaMKa,l_Random,Tt,ZT,jlca_dyad);
#endif
#ifdef OPENMPONCELL
dyadloop123LtypeCOMP(Cell,dyadInnerVars,dyadOuterVars,Unit_cads,Cell_nca,Cell_CaMKa,l_Random,Tt,ZT,jlca_dyad,ilca,ilcana,ilcak);
#endif
#else

    dyadloop1LtypePCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,Cell_nca,Cell_CaMKa,ab5,cd5);
#ifdef TIME
    T_Oend = timing();
    //Cell.T_ical_dyad1 += T_Oend-T_OStart;
    T_dyad1 += T_Oend-T_OStart;
#endif
#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop2LtypeOpenCOMP(Cell,dyadOuterVars,l_Random,Tt,ZT,ab5,cd5,nLCC_dyad_np,nLCC_dyad_p); 
#ifdef TIME
    T_Oend = timing();
    //Cell.T_L_Channel += T_Oend-T_OStart;
    T_dyad2 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif

#ifdef OPENMPONDYAD
    dyadloop3ICALCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad);
#endif
#ifdef OPENMPONCELL
    dyadloop3ICALCOMP(dyadInnerVars, dyadOuterVars,Unit_cads,nLCC_dyad_np,nLCC_dyad_p,jlca_dyad,ilca,ilcana,ilcak);
#endif

#endif  //end if of MERGE123

#ifdef TIME
    T_Oend = timing();
    //Cell.T_ical_dyad += T_Oend-T_OStart;
    T_dyad3 += T_Oend-T_OStart;
#endif



#ifdef TIME
    T_OStart = timing();
#endif
    dyadloop4TrefCOMP(dyadOuterVars,Unit_NryrO1,Cell_nactive,Cell_trel,tref);
#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel1 += T_Oend-T_OStart;
    T_dyad4 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif

#ifdef MERGE56
dyadloop56(dyadOuterVars,dyadInnerVars,Unit_cads,Unit_cajsr,tref,Cell,l_Random,Tt,ZT,Bb,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
#else

    dyadloop5RyrPCOMP(dyadOuterVars,dyadInnerVars,Unit_cads,Unit_cajsr,tref,kC1O1,kC1C2,kC2C1,kC2O2);
#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel2 += T_Oend-T_OStart;
    T_dyad5 += T_Oend-T_OStart;
#endif

#ifdef TIME
    T_OStart = timing();
#endif
  #ifdef AVEC
    dyadloop6RyrOpenCOMP(dyadOuterVars,l_Random,Tt,ZT,Bb,kC1O1,kC1C2,kC2C1,kC2O2,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
  #endif
  #ifdef MVEC
    dyadloop6RyrOpenCOMPVec(Cell,dyadOuterVars,l_Random,Tt,ZT,Bb,kC1O1,kC1C2,kC2C1,kC2O2,Unit_NryrO1,Unit_NryrO2,Unit_NryrC1,Unit_NryrC2);
  #endif
#endif//end if of MERGE56    
    
#ifdef TIME
    T_Oend = timing();
    //Cell.T_RyrOpen += T_Oend-T_OStart;
    T_dyad6 += T_Oend-T_OStart;
#endif
#ifdef TIME
    T_OStart = timing();
#endif

#ifdef OPENMPONDYAD
    dyadloop7ConcCOMP(dyadOuterVars,dyadInnerVars,Unit_NryrO1,jlca_dyad,Cell_Poryr,Unit_cajsr,Unit_cads,Unit_cass,Unit_cacyt,Unit_cansr,Cell_CaMKb,Cell_CaMKt,Cell_CaMKa);
#endif
#ifdef OPENMPONCELL
    dyadloop7ConcCOMP(dyadOuterVars,dyadInnerVars,Unit_NryrO1,jlca_dyad,Cell_Poryr,Unit_cajsr,Unit_cads,Unit_cass,Unit_cacyt,Unit_cansr,Cell_CaMKb,Cell_CaMKt,Cell_CaMKa,jrel,inaca,inacass,ipca,icab);
#endif
#ifdef TIME
    T_Oend = timing();
    //Cell.T_irel3 += T_Oend-T_OStart;
    T_dyad7 += T_Oend-T_OStart;
#endif

#endif //endif of MERGE123AND56
/////////////////////////////////////////////////////////////////////////



#ifdef OPENMPONDYAD
    #pragma omp master
    {
#endif
        
	Cell.T_ical_dyad1 += T_dyad1;
        Cell.T_L_Channel += T_dyad2;
        Cell.T_ical_dyad += T_dyad3;
        Cell.T_irel1 += T_dyad4;
        Cell.T_irel2 +=  T_dyad5;
	Cell.T_RyrOpen += T_dyad6;
	Cell.T_irel3 +=  T_dyad7;
#ifdef OPENMPONDYAD
    }
    #pragma omp barrier
#endif

#ifdef OPENMPONDYAD
}//end of parallel
#endif



    
	dyadOuterVars->ilca = ilca;
        dyadOuterVars->ilcana = ilcana;
        dyadOuterVars->ilcak = ilcak; 
        dyadOuterVars->jrel = jrel;
        dyadOuterVars->inaca = inaca;
        dyadOuterVars->inacass = inacass;
        dyadOuterVars->ipca = ipca;
        dyadOuterVars->icab = icab;

    //_mm_free(dyadInnerVars);
    
    
    
    
    
////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
//#ifdef TIME
//    T_Oend = timing();
//    Cell.T_irel_dyad += T_Oend-T_OStart;
//#endif

	dyadOuterVars->ilca  = dyadOuterVars->ilca/Ndyads;   //uA/uF
        dyadOuterVars->ilcana = dyadOuterVars->ilcana/Ndyads;
        dyadOuterVars->ilcak = dyadOuterVars->ilcak/Ndyads;

	dyadOuterVars->inaca = dyadOuterVars->inaca/Ndyads;
        dyadOuterVars->inacass = dyadOuterVars->inacass/Ndyads;
        dyadOuterVars->ipca = dyadOuterVars->ipca/Ndyads;
        dyadOuterVars->icab = dyadOuterVars->icab/Ndyads;



#ifdef TIME
     T_OStart = timing();
#endif
    /////////////////////////////%%%%%%%%%%%%%%%%%%8.4ca_conc_diff_update()%%%%%%%%%%%%%%%%%/////////////
#ifdef OPENMPONDYAD
//omp_set_num_threads(NUMTHREADS);
#pragma omp parallel reduction(+:cai,casub,cadyad,jsr,nsr,factivedyad,CCaMKa_cell)
#endif
{    

    

    cacytdiff = Unit_cacyt;
    cansrdiff = Unit_cansr;
    cassdiff = Unit_cass;


double bsubspace;
double bmyo;

double bsrkm = bsrbar*kmbsr;
double bslkm = bslbar*kmbsl;
double trpkm = trpnbar*kmtrpn;
double cmdkm = cmdnbar*kmcmdn;


double r1 = dt/(dx*dx);
double r2 = dt/(dy*dy);
double r3 = dt/(dz*dz);


#ifdef OPENMPONDYAD
#pragma omp for
#endif
for(int i=0;i<Nz_diff*Ny_diff;i++)
{
    int z = i/Ny_diff;
    int y = i%Ny_diff;
//for(int z=0;z<Nz_diff;z++)
//{
//    for(int y=0;y<Ny_diff;y++)
//    {
	int x,c,n,s,b,t;
	x=0;
	c=x+y*Nx_diff+z*Nx_diff*Ny_diff;
	
	
	n=c-Nx_diff;
	s=c+Nx_diff;
	if(Ny_diff==1)
	     n=s=c;
	else if(y==0)
	     n = c+Nx_diff;
	     else if(y==Ny_diff-1)
	        s=c-Nx_diff;


	b=c-Nx_diff*Ny_diff;
	t=c+Nx_diff*Ny_diff;
	if(Nz_diff==1)
	     b=t=c;
	else if(z==0)
	     b=c+Nx_diff*Ny_diff;
	     else if(z==Nz_diff-1)
	        t=c-Nx_diff*Ny_diff;
	
	
	bsubspace = 1/(1 + (bsrkm/(pow((kmbsr+cassdiff[c]),2))) + (bslkm/(pow((kmbsl+cassdiff[c]),2))));
        bmyo = 1/(1 + (trpkm/(pow((kmtrpn+cacytdiff[c]),2))) + (cmdkm/(pow((kmcmdn+cacytdiff[c]),2))));	
	
	cacyttemp[c] = cacytdiff[c] + Dcyt*bmyo*( 2*cacytdiff[c+1] -2*cacytdiff[c])*r1\
			+ Dcyt*bmyo*(cacytdiff[n] + cacytdiff[s] -2*cacytdiff[c])*r2\
			+ 2*Dcyt*bmyo*(cacytdiff[b] + cacytdiff[t] -2*cacytdiff[c])*r3;

	casstemp[c] = cassdiff[c] + Dcyt*bsubspace*( 2*cassdiff[c+1] - 2*cassdiff[c])*r1\
		 	+ Dcyt*bsubspace*(cassdiff[n] + cassdiff[s] - 2*cassdiff[c])*r2 \
			+ 2*Dcyt*bsubspace*(cassdiff[b] + cassdiff[t] - 2*cassdiff[c])*r3;

	cansrtemp[c] = cansrdiff[c] + Dsr*( 2*cansrdiff[c+1] -2*cansrdiff[c])*r1 \
			+ Dsr*(cansrdiff[n] + cansrdiff[s] -2*cansrdiff[c])*r2 \
			+ Dsr*(cansrdiff[b] + cansrdiff[t] -2*cansrdiff[c])*r3;


	//#pragma simd
	#pragma ivdep
	for(x=1;x<(Nx_diff-1);x++)
	{
	     ++c;
	     ++n;
	     ++s;
      	     ++b;
    	     ++t;

	     bsubspace = 1/(1 + (bsrkm/(pow((kmbsr+cassdiff[c]),2))) + (bslkm/(pow((kmbsl+cassdiff[c]),2))));
             bmyo = 1/(1 + (trpkm/(pow((kmtrpn+cacytdiff[c]),2))) + (cmdkm/(pow((kmcmdn+cacytdiff[c]),2))));
	     cacyttemp[c] = cacytdiff[c] + Dcyt*bmyo*( cacytdiff[c-1]+cacytdiff[c+1] -2*cacytdiff[c])*r1\
			+ Dcyt*bmyo*(cacytdiff[n] + cacytdiff[s] -2*cacytdiff[c])*r2\
			+ 2*Dcyt*bmyo*(cacytdiff[b] + cacytdiff[t] -2*cacytdiff[c])*r3;

	     casstemp[c] = cassdiff[c] + Dcyt*bsubspace*( cassdiff[c-1]+cassdiff[c+1] - 2*cassdiff[c])*r1\
		 	+ Dcyt*bsubspace*(cassdiff[n] + cassdiff[s] - 2*cassdiff[c])*r2 \
			+ 2*Dcyt*bsubspace*(cassdiff[b] + cassdiff[t] - 2*cassdiff[c])*r3;

	    cansrtemp[c] = cansrdiff[c] + Dsr*( cansrdiff[c-1]+cansrdiff[c+1] -2*cansrdiff[c])*r1 \
			+ Dsr*(cansrdiff[n] + cansrdiff[s] -2*cansrdiff[c])*r2 \
			+ Dsr*(cansrdiff[b] + cansrdiff[t] -2*cansrdiff[c])*r3;
	}
	++c;
	++n;
	++s;
	++b;
	++t;
	bsubspace = 1/(1 + (bsrkm/(pow((kmbsr+cassdiff[c]),2))) + (bslkm/(pow((kmbsl+cassdiff[c]),2))));
        bmyo = 1/(1 + (trpkm/(pow((kmtrpn+cacytdiff[c]),2))) + (cmdkm/(pow((kmcmdn+cacytdiff[c]),2))));
	cacyttemp[c] = cacytdiff[c] + Dcyt*bmyo*( 2*cacytdiff[c-1] -2*cacytdiff[c])*r1\
			+ Dcyt*bmyo*(cacytdiff[n] + cacytdiff[s] -2*cacytdiff[c])*r2\
			+ 2*Dcyt*bmyo*(cacytdiff[b] + cacytdiff[t] -2*cacytdiff[c])*r3;

        casstemp[c] = cassdiff[c] + Dcyt*bsubspace*( 2*cassdiff[c-1] - 2*cassdiff[c])*r1\
		 	+ Dcyt*bsubspace*(cassdiff[n] + cassdiff[s] - 2*cassdiff[c])*r2 \
			+ 2*Dcyt*bsubspace*(cassdiff[b] + cassdiff[t] - 2*cassdiff[c])*r3;

	cansrtemp[c] = cansrdiff[c] + Dsr*( 2*cansrdiff[c-1] -2*cansrdiff[c])*r1 \
			+ Dsr*(cansrdiff[n] + cansrdiff[s] -2*cansrdiff[c])*r2 \
			+ Dsr*(cansrdiff[b] + cansrdiff[t] -2*cansrdiff[c])*r3;
		
    }	

//}
#ifdef OPENMPONDYAD
#pragma omp single
#endif
{
    double *tempPtr;
    tempPtr = cacyttemp;
    Cell.cacyttemp = cacytdiff;
    Cell.cacyt = tempPtr;
    Unit_cacyt = tempPtr;
    
    tempPtr = casstemp;
    Cell.casstemp = cassdiff;
    Cell.cass = tempPtr;
    Unit_cass = tempPtr;
    
    tempPtr = cansrtemp;
    Cell.cansrtemp = cansrdiff;
    Cell.cansr = tempPtr;
    Unit_cansr = tempPtr;
}

    /////////////////////////////%%%%%%%%%%%%%%%%%%8.4ca_conc_diff_update()%%%%%%%%%%%%%%%%%/////////////
    int numThreads = dyadOuterVars->NumThreads;
    int RCT = (int)Ndyads/numThreads;
    #ifdef OPENMPONDYAD
    #pragma omp for schedule(static,CHUNCKSIZE)
    #endif
     #pragma ivdep
    //#pragma vector aligned
    #pragma vector always
     for(int i=0;i<Ndyads;i++)     
     {
        cai += Unit_cacyt[i];
        casub += Unit_cass[i];
        cadyad += Unit_cads[i];
        jsr += Unit_cajsr[i];
        nsr += Unit_cansr[i];
        factivedyad += Cell_nactive[i];
        CCaMKa_cell += Cell_CaMKa[i];
        //Cell.CaMKa_cell += Cell_CaMKa[i];
        //CaMKb_cell += Cell_CaMKb[i];
        //CaMKt_cell += Cell_CaMKt[i];
    }

}
    cai = cai/Ndyads;
    casub = casub/Ndyads;
    cadyad = cadyad/Ndyads;
    jsr = jsr/Ndyads;
    nsr = nsr/Ndyads;
    factivedyad = factivedyad/Ndyads;
    Cell.CaMKa_cell = CCaMKa_cell/Ndyads;
    //Cell.CaMKa_cell = Cell_CaMKa_cell/Ndyads;

#ifdef TIME
     T_Oend = timing();
    Cell.T_conc_diff += T_Oend-T_OStart;
#endif



        Cell.cadyad = cadyad;
        Cell.casub = casub;
        Cell.cai = cai;
        Cell.nsr = nsr;
        Cell.jsr = jsr;

    ilca1 = dyadOuterVars->ilca; 
    ilcana1 =dyadOuterVars->ilcana;
    ilcak1 = dyadOuterVars->ilcak;
    inaca1 = dyadOuterVars->inaca;
    icab1 = dyadOuterVars->icab;
    ipca1 = dyadOuterVars->ipca;
    inacass1 = dyadOuterVars->inacass;
    jrel1 = dyadOuterVars->jrel;
    
    //_mm_free(dyadOuterVars);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    jrel = jrel1;
    ilca = ilca1;
    ilcana = ilcana1;
    ilcak = ilcak1;
    inaca = inaca1;
    icab = icab1;
    ipca = ipca1;
    ipca = ipca1;
    inacass = inacass1;


    //############################8.comp_calc_dyad()#####################################///
#ifdef TIME
    T_end = timing();
    Cell.T_calc_dyad += T_end-T_start;
#endif


#ifdef TIME
    T_start=timing();
#endif
    //################################### part of 9.comp_ical()##############################//
     ////////////////
    double dss=1.0/(1.0+exp((-(voltage+3.940))/4.230));
    if (iso == 1) dss=1.0/(1.0+exp((-(voltage+3.940+14))/4.230));

    double td=0.6+1.0/(exp(-0.05*(voltage+6.0))+exp(0.09*(voltage+14.0)));
    Cell.d=dss-(dss-Cell.d)*exp(-dt/td);
     ////////////////


    double fss=1.0/(1.0+exp((voltage+19.58)/3.696));
    if (iso == 1) fss=1.0/(1.0+exp((voltage+19.58+8)/3.696));

    double tff=7.0+1.0/(0.0045*exp(-(voltage+20.0)/10.0)+0.0045*exp((voltage+20.0)/10.0));
    double tfs=1000.0+1.0/(0.000035*exp(-(voltage+5.0)/4.0)+0.000035*exp((voltage+5.0)/6.0));
    double Aff=0.6;
    double Afs=1.0-Aff;
    Cell.ff=fss-(fss-Cell.ff)*exp(-dt/tff);
    Cell.fs=fss-(fss-Cell.fs)*exp(-dt/tfs);
     Cell.f=Aff*Cell.ff+Afs*Cell.fs;
    double fcass=fss;
    double tfcaf=7.0+1.0/(0.04*exp(-(voltage-4.0)/7.0)+0.04*exp((voltage-4.0)/7.0));
    double tfcas=100.0+1.0/(0.00012*exp(-voltage/3.0)+0.00012*exp(voltage/7.0));
    double Afcaf=0.3+0.6/(1.0+exp((voltage-10.0)/10.0));
    double Afcas=1.0-Afcaf;
    Cell.fcaf=fcass-(fcass-Cell.fcaf)*exp(-dt/tfcaf);
    Cell.fcas=fcass-(fcass-Cell.fcas)*exp(-dt/tfcas);
    Cell.fca=Afcaf*Cell.fcaf+Afcas*Cell.fcas;
    double tjca = 75.0;
    Cell.jca=fcass-(fcass-Cell.jca)*exp(-dt/tjca);
    double tffp=2.5*tff;
    Cell.ffp=fss-(fss-Cell.ffp)*exp(-dt/tffp);
    Cell.fp=Aff*Cell.ffp+Afs*Cell.fs;
    double tfcafp=2.5*tfcaf;
    Cell.fcafp=fcass-(fcass-Cell.fcafp)*exp(-dt/tfcafp);
    Cell.fcap=Afcaf*Cell.fcafp+Afcas*Cell.fcas;
    //###################################part of 9.comp_ical()##############################//

//###################################part of 9.comp_ical()##############################//  
   //for plotting ICaL only//
    //double km2n=Cell.jca*1.0;
     km2n=Cell.jca*1.0;
    double anca=1.0/(k2n/km2n+pow(1.0+Kmn/cadyad,4.0));
    Cell.nca_print = anca*k2n/km2n-(anca*k2n/km2n-Cell.nca_print)*exp(-km2n*dt);
    double fICaLp=(1.0/(1.0+KmCaMK/Cell.CaMKa_cell));
    double PhiCaL=4.0*vffrt*(1e-3*cadyad*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
    //double PhiCaNa=1.0*vffrt*(0.75*Cell.nai*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
    PhiCaNa=1.0*vffrt*(0.75*Cell.nai*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
    //double  PhiCaK=1.0*vffrt*(0.75*Cell.ki*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);
     PhiCaK=1.0*vffrt*(0.75*Cell.ki*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);

    double ICaL_print=(1.0-fICaLp)*PCa*PhiCaL*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCap*PhiCaL*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
    double ICaNa_print=(1.0-fICaLp)*PCaNa*PhiCaNa*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCaNap*PhiCaNa*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
    double ICaK_print=(1.0-fICaLp)*PCaK*PhiCaK*Cell.d*(Cell.f*(1.0-Cell.nca_print)+Cell.jca*Cell.fca*Cell.nca_print)+fICaLp*PCaKp*PhiCaK*Cell.d*(Cell.fp*(1.0-Cell.nca_print)+Cell.jca*Cell.fcap*Cell.nca_print);
    //###################################part of 9.comp_ical()##############################//
#ifdef TIME
    T_end = timing();
    Cell.T_ical+=T_end-T_start;
#endif


#ifdef TIME
    T_start = timing();
#endif
    //############################10. comp_it()#####################################///
    //if ( (ix>3*N_x/8 && ix<5*N_x/8) && (iy>3*N_y/8 && iy<5*N_y/8) ) istim = ist; else istim = 0;
    //if ( iz == 1 and offset_z == 0 ) istim = ist; else istim = 0;
    //if ( iz == 1 and iy == 1 and ix==1 ) istim = ist; else istim = 0;
    //istim = ist;
    
    #ifdef CPU
    if((((ix==1)&&(offset_x==0))&&(t<TT))||((t>TT)&&(t<=TT+10*dt)))
        istim = ist;
    else
        istim = 0;
    #else 
    istim = ist;
    #endif
    

    naiont = ina+inal+INab+ilcana+3*INaK+3*inaca+3*inacass;
    kiont = Ito + IKr + IKs + IK1 + ilcak + IKb - 2*INaK + istim;
    caiont = ilca+icab+ipca-2*inaca-2*inacass;
    it = naiont + kiont + caiont;
    //############################10. comp_it()#####################################///



    //#############################11.calc_nai()###################################////
    double dnai = -dt*(naiont*acap)/(vmyo*zna*frdy);
    Cell.nai = dnai + Cell.nai;
    //#############################11.calc_nai()###################################////


    //#############################12.calc_ki()###################################////
    double dki = -dt*((kiont)*acap)/(vmyo*zk*frdy);
    Cell.ki = dki + Cell.ki;
    //#############################12.calc_ki()###################################////
    
    #ifdef PHI
    v[I] += -it*dt;
    #endif
    #ifdef CPU
    v[ix][iy][iz] += -it*dt;
    #endif

        //out put info
        Cell.ina = ina;
        Cell.ICaL_print=ICaL_print;
        Cell.Ito=Ito;
        Cell.IKs=IKs;
        Cell.IKr=IKr;
        Cell.IK1=IK1;
        Cell.INaK=INaK;
        Cell.inaca=inaca;
        Cell.inacass=inacass;
        Cell.jrel=jrel;
#ifdef TIME
    T_end = timing();
 //   Cell.T_rest += T_end-T_start;
#endif

#ifdef TIME
    Cell.T_cell += T_end-T_Start;
#endif



}

