#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include <scif.h>
#include <source/COIProcess_source.h>
#include <source/COIEngine_source.h>


///////////////////////////////////////////////////Configuration Example:CPU+2PHI mode , OpenMP on cell level on both CPU and PHI

//#define TEST  //if define TEST, then bcl=200 which means there is only 4000 steps for one heart beat, if disable this then bcl=500ms which means there are 10000 steps for one beats, and the number of beats is set in tissue.cpp 

//#define ONLYPHI
#define ONLYCPU

#ifndef ONLYCPU
#define REG  //enabled when ONLYCPU disabled
//#define SCIFT  //using simple SCIF to transfer data between CPU and PHI
#endif

//############################################useless in ONLY CPU mode############################/////////////
#define OptiPhiThreads 236   //
#ifdef ONLYPHI
#define PhiNUM 1   //set this to 1 if only use 1 PHI
#else
#define PhiNUM 2   ///if want to test CPU+1PHI set this to 1,and if test CPU+2PHI,set this to 2
#endif
#define PHISpeedFactor 1 //estimated PHI vs CPU,  openmp on dyad set this to 0.48,opemp on cell level set it to 1 
//###############################################################################################///////////////


#define hh     //For the barrier() define, this can be enabled or disabled, it does not make difference

#define TIME   //enable time profiling

//#define APPROXIMATE    //make approximation in binomial function. when p<0.00000001,return 0 if enabled


//////###########################This region is for CPU code########################################///////
#ifdef CPU
#define PRINT   //enable print the time information 

#define OPENMPONCELL      //enable one of them under ONLY PHI mode
//#define OPENMPONDYAD  

#ifdef OPENMPONDYAD
#define CHUNCKSIZE 128    //This only works when openmp on dyad loop enabled
#endif

#ifdef ONLYPHI
#define NUMTHREADS 1    //if only use phi, then set this value to 1, because use only one thread on CPU to control the data transfer is enough.
#else
#define NUMTHREADS 32  //set this to 1~32 as you want to ,for OpenMP on cell level the opti value is 32, and for openmp on dyad, the opti is 16
#endif
 
#define AVEC            //this is always defined, means autovectorization for CPU, if we want to disable auto-vec, just add option -no-vec in Makefile.host but still define this .

//#define NOMERGE       //if defined then there is no merge between the 7 dyad functions
#ifndef NOMERGE
#define MERGE12AND345   //do not disable this any time
#define MERGE345        //merge 3 4 5 only under MERGE12AND345 defined(actually 1 2 is not merged)
#endif      

#define RandOrigin    //do not disable this


#define RYR
#define RYRNew  

#ifdef OPENMPONDYAD
#define parallerandom
#endif

#endif   //end of ifdef CPU
///////#################################end of CPU define region###############################/////////////////


//////##################################this region is for PHI code############################////////////////
#ifdef PHI

#define PRINT      //define this to enable time information


#define OPENMPONCELL
//#define OPENMPONDYAD

#define MVEC     //manual vectorization
//#define AVEC   //auto vectorization

#ifdef MVEC
#define CHUNCKSIZE 16   //maunal vec set this to 16, and auto vec set this to 88 when OpenMP on dyad loop
#else
#define CHUNCKSIZE 88
#endif

#define NUMTHREADS 236   //set this to 236 if OpenMP on cell level, and set it to 118 if OpenMP on dyad loop



//three way, no merge, merge 12 and merge 345, merge 123 and 56
//#define NOMERGE
//#define MERGE12AND345    //can only merged under MVEC defined
#define MERGE123AND56    //can only merged under MVEC defined

#ifdef MERGE12AND345
#define MERGE345
#define MERGE12
#endif

#ifdef MERGE123AND56
#define MERGE123
#define MERGE56
#endif

#define RandOrigin  //do not disable this 

#ifdef OPENMPONCELL
#define SAVERAND
#endif

#define RYR
#define RYRNew

#ifdef OPENMPONDYAD
#define parallerandom
#endif

#endif






struct PHItype_t{
    scif_epd_t dst0;
    int N_x;
    int N_y;
    int N_z;
    int Ndyads;
    int bcl;
    double dt;
    int offset_x;
    int offset_y;
    int offset_z;
    int total_x;
    int total_y;
    int total_z;
    double Dx;
    double d_x;
    int beats;
    int Unit_X;
    int Unit_Y;
    int Unit_Z;
    int rank;
    int steps;
    int start;
    int end;
};

typedef  PHItype_t PHItype;

#endif
